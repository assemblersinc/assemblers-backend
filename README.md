# README: Assemblers Inc. Backend #

### Overview ###

* Rest API and database setup for Assemblers Inc app using ([node.js](https://nodejs.org) + [MySQL](https://www.mysql.com/)) 
* v1.0

### Required software ###

* Install [node.js](https://nodejs.org/en/download/) 
* Install [MySQL Database Server](https://dev.mysql.com/downloads/mysql/)
* To manipulate db data, install a database management tool: ([Sequel Pro](https://sequelpro.com/download) or [MySQL Workbench](https://dev.mysql.com/downloads/workbench/))

### Local Database Setup ###

The API will point at Assemblers MySQL on Microsoft Azure by default.  
To create a local db:  

* Ensure MySQL Database Server is installed and running locally   
* In your database management tool, create a database with configuration parameters found in `<app>/config/env/`  
* Sequelize will handle db table migration based on `<app>/models/`  

### Local API Setup ###

In terminal:

```
#!python
$ cd <projects_directory>
$ git clone <bitbucket.git.address>
$ cd <app_directory>
$ npm install 
$ npm run <environment_variable> (default = dev)
```

Upon success, a verification endpoint is available via: [http://localhost:3000/api/v1/](http://localhost:3000/api/v1/)

The [Assemblers Paw Project](https://paw.cloud/account/teams/17640/projects/12028) contains documentation for all API endpoints.

### Helpful links ###

* [JIRA Dashboard](https://bluefletch.atlassian.net/projects/ASI/summary) (Issue tracking)
* [Paw Dashboard](https://paw.cloud/account/teams/17640/projects/12028) (API calls testing)
* [Dropbox](https://www.dropbox.com/sh/38d66brl2znhsap/AACznLBOzlMhRDwQnL8aMKzZa?dl=0) (Assets, Resources)
* [Sequelize Documentation](http://docs.sequelizejs.com/) (Node.js docs)

### TODO ###
* Unit testing
* Data verification
* Finish security
* Update endpoints docs, with expected inputs and outputs

### Contact ###
Nathaniel Compton  
Andres Avendano  
Tony Gutierrez