// ==============================
// System or Server configuration 
// ==============================
import fs from 'fs';

const config = {
  secret: 'assemblersincsupersecretkey',
  environment: process.env.NODE_ENV || 'dev',
  server: {
    port: process.env.PORT || 3000
  },
  api: {
    path: '/api/v1',
    securePath: '/secure-api',
    version: 1
  }, 

  // Main db for data
  databases: {
    apiDb: {
      user: 'assemblersdb@assemblers-db',
      password: 'Chattanooga@610',
      database: 'assemblers-store-dev-db', 
      options: {
        host: 'assemblers-db.mysql.database.azure.com',
        port: 3306,
        dialect: 'mysql',
        charset: 'utf8',
        collation: 'utf8_unicode_ci',
        dialectOptions: {
          insecureAuth: true,
          encrypt:true, 
          ssl:{ ca: fs.readFileSync('./certs/ca-assemblerinc.pem') }
        }
      }
    },
    // db for invoices
    exactdb: {
      host: 'http://74.205.128.196/',
      endpoint:'ws_oe_import/service.svc/importXml', 
      headers: {'Content-Type': 'application/soap+xml'},
      xml:'<?xml version="1.0"?>'
          + '<Envelope>'
            + '<EnvelopeHeader>'
             + '<Mode>Testing</Mode>'
             + '<PartnershipIdentifier>8103960483</PartnershipIdentifier>'
             +'</EnvelopeHeader>'
             +'<OrderData>'
             + '<OrderHeader>'
             +  '<ItemFilter>E</ItemFilter>'
              +  '<OrderType>Invoice</OrderType>'
              +  '<CustomerNumber>{{invoice.client.code}}{{lpad4 invoice.location.storeNumber}}</CustomerNumber>'//client code + store #
              +  '<Salesperson>{{invoice.user.techId}}</Salesperson>'//user.techid
              +  '<Keyrec>{{invoice.keyRecNumber}}</Keyrec>'
              +  '<CustomerPO>{{invoice.poNumber}}</CustomerPO>'
              +  '<OrderDate>{{invoice.invoiceCompletedTime}}</OrderDate>'//invoice.invoiceCompletedTime
              +  '<BatchID>{{invoice.invoiceId}}</BatchID>'//invoiceId
              +  '<OrderTotal>{{invoice.total}}</OrderTotal>'//invoice.total
              +  '<ShipToName>{{invoice.location.Location}}</ShipToName>'//location.Location
              +  '<ShipAddress1>{{invoice.location.address}}</ShipAddress1>'//location address
              +  '<ShipAddress2/>'
              +  '<ShipAddress3/>'
              +  '<ShipCity>{{invoice.location.city}}</ShipCity>'//loc
              +  '<ShipState>{{invoice.location.state}}</ShipState>'
              +  '<ShipZip>{{invoice.location.zip}}</ShipZip>'
              +  '<ShipCountry>US</ShipCountry>'
              +  '<UUID></UUID>'
             + '</OrderHeader>'
             + '<OrderLines>'//invoice product
               + '{{#each invoice.invoiceproduct}}<OrderLine>'
                  + '<Qty>{{qty}}</Qty>'
                  + '<ItemNo>{{ai_code}}</ItemNo>'
                  + '<UnitPrice>{{pricePerUnit}}</UnitPrice>'//price per unit
               + '</OrderLine>{{/each}}'
                
             + '</OrderLines>'
             + '<OrderComments>'
               + '{{#if invoice.signatureImageUrl}}<OrderComment>Signature: {{invoice.signatureImageUrl}}</OrderComment>{{/if}}'//url for images invoice sigurl and stamp.
               + '{{#if invoice.stamp}}<OrderComment>Stamp: {{invoice.stamp}}</OrderComment>{{/if}}'//url for images invoice sigurl and stamp.
               + '{{#if invoice.invoiceId}}<OrderComment>InvoiceId: {{invoice.invoiceId}}</OrderComment>{{/if}}'//url for images invoice sigurl and stamp.
               + '</OrderComments>'
           + '</OrderData>'
          + '</Envelope>'//response goes into invoice #
    },
    // db for time tracking / work hour logging
    optimumdb: {
      host: '74.205.128.196',
      port: 80,
      user: 'OsiAdministrator',
      password: 'OptimumSolutions',
      database: 'Suite', // revert back to Test_Suite
      charset: 'utf8',
      collation: 'utf8_unicode_ci'
    }
  },
//14
  // Azure provides us with 2 sets of Account Name / Access Key if we ever want to invalidate or refresh access
  AZURE_STORAGE_ACCOUNT: 'assemblersincstorage',
  AZURE_STORAGE_ACCESS_KEY: 'utU5xnB3/JAAKJfXq5/ri6DFeXerTnSAceVRRTVxfAo/TihVmkVr7oeb3M8lghScnZ1FbgLihYEBkTy9dAA+NA==',
  AZURE_STORAGE_CONNECTION_STRING: '',

  azuread: {
    // Required
    authorityHostUrl: 'https://login.windows.net/common',

    // Required, the client ID of your app in AAD  
    clientId: 'e9d0d500-9a9c-4567-9473-16570ecd76ac',// application id
    audience: 'e9d0d500-9a9c-4567-9473-16570ecd76ac', // same as clientid for now.
    tenant:'10eee59d-2fdc-47ae-a793-57636e703746',
    // Required if `responseType` is 'code', 'id_token code' or 'code id_token'.
    // If app key contains '\', replace it with '\\'.
    clientSecret: 'NIbyhv1S38Lwix5w9X4/zZOM+rjhdommdJyZyYWnuw4=', // if you are doing code or id_token code
  }, 

  // Notification Hub
  azurenotification:{
    hubName:'assemblersincnotificationbhub',
    connectionString: 'Endpoint=sb://assemblersincnotificationshub.servicebus.windows.net/;SharedAccessKeyName=DefaultFullSharedAccessSignature;SharedAccessKey=agIge8+5lNzNzXue/AR2YWHhWcfPuJgoUM5GsqcIfuE='
  },

	azureRedis: {
		host: 'assemblers.redis.cache.windows.net',
		port: 6379,
		auth: '47lfP33XbOj6qDq+cFnXwRy1ZAHkb1wRo9sWQPrrHk8='
	},

  // Azure Blob Storage Containers
  azurestorage: {
    // signature:'?sv=2016-05-31&ss=b&srt=sco&sp=rwdlac&se=2117-07-20T22:54:01Z&st=2017-07-20T14:54:01Z&spr=https&sig=%2F9iKx0oS%2BySUx3fi%2Fo7E7CWJz6UK3oA646OEs3T4b0Y%3D',
    //signature:'?st=2017-08-19T03%3A56%3A00Z&se=2017-08-20T03%3A56%3A00Z&sp=rl&sv=2016-05-31&sr=c&sig=kF%2BCVOTfJNTNtJTZVT3gdv9hkH4DU10bs02u61LK3zU%3D',
    signature:'?sv=2017-04-17&ss=b&srt=sco&sp=rwc&se=2117-08-21T02:29:59Z&st=2017-08-20T18:29:59Z&spr=https&sig=zul6%2BzAZspNjea9FxBYwomtouyjJ2xOH8HFUw7cAcPc%3D',
    azureStorageConnectionString: 'https://assemblersincstorage.blob.core.windows.net/',
    azureStorageAccessKey: 'utU5xnB3/JAAKJfXq5/ri6DFeXerTnSAceVRRTVxfAo/TihVmkVr7oeb3M8lghScnZ1FbgLihYEBkTy9dAA+NA==',
    azureStorageAccount: 'assemblersincstorage',
    containerNameRepairs: 'repairs',
    containerNameStamps: 'stamps',
    containerNameStorewalks: 'storewalks',
    containerNameSignatures: 'signatures',
    containerSecurity: 'private'
  },
  checkAuthLevelSwitch:true
}  

export {config}
