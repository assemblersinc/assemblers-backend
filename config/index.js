import { config as development} from  './env/development';
import { config as production} from  './env/production';

let configuration = {};
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development'; 
  configuration = development;

}else if (process.env.NODE_ENV == 'production') {
  configuration = production;
}
else{
  configuration = development;
}

export {configuration as config};