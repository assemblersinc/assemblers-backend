import moment               from 'moment';
import * as notificationHub from '../util/notificationHub';
import logger               from '../util/logger';
import {db}                 from '../models';
import BaseController       from './baseController';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const announcement = db.models.announcement;
const user = db.models.user;

let includes = [{model:user, required: true,as:'author'}];

class AnnouncementController {

  constructor(){ }

  /**
   * Find all announcements
   */
  findAll(req, res, next) {
    // checks security
    if(!checkAuthLevel(req.currentUser.roleId, announcement.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAll Announcements announcement');
    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    return announcement.findAndCountAll({
      offset: offset ? offset : 0,
      limit: limit ? limit : 30,
      order: [['createdAt', 'DESC']],
      include:includes
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

   /**
   * Find all announcements by announcement id
   */
  findById(req, res, next) {

    // checks security
    if(!checkAuthLevel(req.currentUser.roleId, announcement.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findById announcement');
    if (!req.params.announcementId || !req.params.announcementId) {
      logger.error('announcement Id is required');
      return res.status(400).send('announcement Id is required');
    }

    announcement.findOne({
      where: { announcementId: req.params.announcementId }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

   /**
   * Find all announcements by user id 
   */
  findByUserId(req, res, next) {

    // checks security
    if(!checkAuthLevel(req.currentUser.roleId, announcement.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('find all created by userId announcement');
    if (!req.params.userId) {
      logger.error('userId is required');
      return res.status(400).send('user Id is required');
    }
    announcement.findAll({
      where: {
        userId: req.params.userId
      }
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

   /**
   * Create announcement  
   */
  create(req, res, next) {

    // checks security
    if(!checkAuthLevel(req.currentUser.roleId, announcement.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('create announcement');
    const body = req.body;
    if (!body.text || !body.userId) {
      logger.error('announcement text & userId needed');
      return res.status(400).send('announcement name, userId needed');
    }
    return announcement.findOrCreate({
      where: {
        text: req.body.text, 
        tag:req.body.tag,
        userId:req.body.userId, 
        url: req.body.url
      }
    })
    .then(result => {
      logger.info('new announcement created');
      let tags = result.tag ? result.tag.split(",").map(String) : null;

      this.sendNotification(tags, result.message, {type:'announcement'})
      .then((data) =>{
        return res.status(200).json(result);
      }).catch((err) => {
         return res.status(400).send("Problem sending notification, Announcement has been created! " + err);
      });
    })
    .catch(next);

  }

   /**
   * Remove notifications. 
   */
  remove(req, res, next) {

    // checks security
    if(!checkAuthLevel(req.currentUser.roleId, announcement.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('remove announcement');
    announcement.destroy({
      where: {
        announcementId: req.params.announcementId
      }
    })
    .then(announcement => { return res.status(200).send('Deleted') })
    .catch(next);
    
  }

  //Helper function for notification 
  sendNotification(tag,message, payload)
  { 
    return notificationHub.sendPush(null, tag, message, payload);
  }

};

export default new AnnouncementController();