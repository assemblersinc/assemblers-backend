import logger          from '../util/logger';
import {ROLE_TYPE, ADMIN, GENERAL}     from '../util/roleType'; 
import {db}            from '../models';
import BaseController  from './baseController';
import * as notificationHub from '../util/notificationHub';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const clientproductsku = db.models.clientproductsku;
const conversation = db.models.conversation;
const geoarea = db.models.geoarea; 
const invoices = db.models.invoice;
const location = db.models.location; 
const managerclaimedtech = db.models.managerclaimedtech; 
const pricing = db.models.pricing; 
const product = db.models.product; 
const productcategory = db.models.productcategory;
const repair = db.models.repair;
const repairCategory = db.models.repairecategory; 
const request = db.models.request;
const requestcreatedby = db.models.requestcreatedby;
const requestProduct = db.models.requestproduct;
const requestType = db.models.requesttype; 
const service = db.models.service;
const storerepair = db.models.storerepair;
const storerepairticket = db.models.storerepairticket;
const storeservice = db.models.storeservice;
const timesheet = db.models.timesheet;
const user = db.models.user; 
const invoiceProduct = db.models.invoiceproduct; 
const invoiceType = db.models.invoicetype; 
const client      = db.models.client; 

let includes = [
    {model: user, required: true, as:'manager'}, 
    {model: user, required: true, as:'tech'}]; 

let requestIncludes = [
  { model: requestcreatedby, required: false, as: 'requestcreatedby' },
  { model: storeservice, required: false, as: 'requestservice' , include: [{model:service,require:true, as:'service' }, { model: user, required: false, as: 'user' }] },
  { model: requestProduct, required: false, as: 'requestproduct',include: {model:product,require:true, as:'product' },  
    include: [
      { model: user, required: false, as: 'user' },
      { model: product, required: false, as: 'product',
      include:[{ model: clientproductsku, required: true, as: 'sku' }, 
               { model: pricing, required: true, as:'pricing' }]
    }]
  },
  { model: requestType, required: true, as:'requesttype' }
];

let repairsIncludes = [
  { model: storerepair, required:true, as:'repairs', where : {completed: 0 },include:[
      { model: user, required: true, as:'user', otherKey:'enteredBy'},
      { model:repair, required: true, as:'repair', otherKey:'repairId', includes:[{
          model: repairCategory, required: true, as:'repairCategory'
      }]},
  ]},
  { model: user, required: true, as:'user', otherKey:'enteredBy'}
];

class AreaManagerController {

  constructor () { }
  /**********
   *  DASHBOARD. 
   * 
   */



  findInvoicePerClient(req, res, next){

    // checks security
    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }
    
    let param = req.params; 

    let currentUser = req.currentUser; 

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    // Remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"];
    let whereClientClause = param.clientId != 'all' ? {clientId : param.clientId } : {};

    let whereAreaClause = ROLE_TYPE.OPS_MANAGER == currentUser.roleId && req.query.areaId ? {areaId:req.query.areaId } :
                          {areaId: currentUser.areaId};

    return invoices.findAll({
      where: whereClientClause,
      include:  [
        {
          model: user,
          required: true, 
          as: 'user'
        },
        {
          model: client,
          required: true, 
          as: 'client'
        },
        {
          model: location, 
          required: true, 
          as: 'location', 
          where: whereAreaClause
        },
        {
          model: invoiceType,
          required: true, 
          as: 'invoicetype'
        },
        {
          model: invoiceProduct, 
          as: 'invoiceproduct'
        }
      ]
    })
    .then((result) =>{

      if(!result){ return res.status(204).send("There is no data to return")}
      
      return res.status(200).json(result);

    })
    .catch(next); 
  }

  findStoreRequestsPerClient(req, res, next){
    
        // checks security
    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    let param = req.params; 

    let user = req.currentUser; 

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    // Remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"];
    let whereClientClause = param.clientId != 'all' ? {clientId : param.clientId } : {};

    let whereAreaClause = ROLE_TYPE.OPS_MANAGER == user.roleId && req.query.areaId ? {areaId:req.query.areaId } :
                          {areaId: user.areaId};
    console.log(whereAreaClause); 

    location.findAll({
      where: whereClientClause,
      include: [
        { model: geoarea, required: true, as:'geoarea', where: whereAreaClause},
        { model: request, required: true, as:'requests', include: requestIncludes }
      ]
    })
    .then((result) =>{

      if(!result){ return res.status(204).send("There is no data to return")}
      
      return res.status(200).json(result);

    })
    .catch(next); 
  }

  findStoreRepairsPerClient(req, res, next){
    
      // checks security
    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }
    let param = req.params; 

    let user = req.currentUser; 

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    // Remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"];

    let whereClientClause = param.clientId != 'all' ? {clientId : param.clientId } : {}

    let whereAreaClause = ROLE_TYPE.OPS_MANAGER == user.roleId && req.query.areaId ? {areaId:req.query.areaId } :
                          {areaId: user.areaId};
    
    console.log(whereAreaClause); 

    location.findAll({
      where: whereClientClause,
      include: [
        { model: geoarea, required: true, as:'geoarea', where: whereAreaClause },
        { model: storerepairticket, required: true, as:'repairs', include:repairsIncludes }
      ]
    })
    .then((result) => {
      if(!result){return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next); 
    
  }



  findAllMyTechs(req, res, next){
    
    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    let param = req.params; 
    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    // Remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"];
    
    if(!param.managerId){
      return res.status(400).send("Invalid request");
    }

     return user.findOne({
       where: { userId: param.managerId, 
         $or:[{roleId:ROLE_TYPE.AREA_MANAGER}, {roleId: ROLE_TYPE.OPS_MANAGER}]
       },
        include:[ 
            {model: managerclaimedtech, required: false, as: 'clockedin',
            include:[
                {
                  model: user, required: true, as:'tech', where: {clockStatus: 'clock-in'} 
                  ,
                  include: [
                    {
                      model:managerclaimedtech, required: false, as:'claimedby', 
                      on: {
                       $and:[
                         db.sequelize.where(db.sequelize.col("clockedin->tech.userId"), "=", db.sequelize.col("clockedin->tech->claimedby.techId")),
                         db.sequelize.where(db.sequelize.col("clockedin->tech.managerId"), "=", db.sequelize.col("clockedin->tech->claimedby.managerId"))]
                      },
                     attributes: ['managerId', 'techId', 'chatName', 'createdAt', 'updatedAt'], 
                     include:[{model:user, as: 'manager' }]
                    },
                    {
                      model:invoices, required: false, as: 'invoices',otherKey:'userId',
                      include:[
                        { 
                          model: invoiceProduct, required: false, as: 'products', where : {entryType: {$in:['P', 'S']}}
                        }
                    ,{
                       model: invoiceProduct, required: false, as: 'repairs', where : {entryType: {$in:['R']}}
                     }
                  ]
                  },
                    {model:timesheet, required: false, as: 'timesheet',otherKey:'timeId',
                      include: 
                      [
                        {model: location, required: true, as: 'location', otherKey: 'locationId'}
                      ]
                    } 
                  ]
                }
            ]
          }, 
          {model: managerclaimedtech, required: false, as: 'clockedout',
            include:[
                {
                  model: user, required: true, as:'tech', where: {$or:[{clockStatus: 'clock-out'},{clockStatus:null}] 
                  },
                  include: [
                    {
                      model:managerclaimedtech, required: false, as:'claimedby', 
                      on: {
                       $and:[
                         db.sequelize.where(db.sequelize.col("clockedout->tech.userId"), "=", db.sequelize.col("clockedout->tech->claimedby.techId")),
                         db.sequelize.where(db.sequelize.col("clockedout->tech.managerId"), "=", db.sequelize.col("clockedout->tech->claimedby.managerId"))]
                       },
                       attributes: ['managerId', 'techId', 'chatName', 'createdAt', 'updatedAt'], 
                       include:[{model:user, as: 'manager' }]
                    },
                    {
                       model:invoices, required: false, as: 'invoices',otherKey:'userId'
                    },
                    {  model:timesheet, required: false, as: 'timesheet',otherKey:'timeId', 
                      include: 
                        [
                          {model: location, required: true, as: 'location', otherKey: 'locationId'}
                        ]
                    }]
                }
            ]
          }
       ],
        offset: offset ? offset : 0,
        limit: limit ? limit : 100 ,
        subQuery: false
      })
     	.then(usr => {
        if(!usr) {
          return res.status(204).send('No data to return!')
        } else {
          return res.status(200).json(usr);
        }
       })
       .catch(next);

  }

  /*
   * This section handles manager claiming techs. 
   */


   /**
   *  finds all claimed techs by managerId 
   * @param {*} req 
   * @param {*} res 
   * @param {*} next 
   */
  findAllClaimedTechs(req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    // Remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 
  
    if(req.currentUser.userId != req.params.managerId && 
      (req.currentUser.roleId != ROLE_TYPE.AREA_MANAGER ||
       req.currentUser.roleId != ROLE_TYPE.OPS_MANAGER )) {
        
        return res.status(400).send('Invalid request'); 
    }

    let managerId = req.params.managerId;

      return user.findOne({
       where: { userId: managerId, 
         $or:[{roleId:ROLE_TYPE.AREA_MANAGER}, {roleId: ROLE_TYPE.OPS_MANAGER}]
       },
       include: [{model: managerclaimedtech, required: true, as:'claimtechs',
        include: {model: user, required: true, as:'tech'}
      }],
        offset: offset ? offset : 0,
        limit: limit ? limit : 30 ,
        subQuery: false
      })
     	.then(usr => {
         console.log('WTF', usr); 
        if(!usr) {
          return res.status(204).send('No data to return!')
        } else {
          return res.status(200).json(usr);
        }
       })
       .catch(next);

  }

  /**
   * Finds all unclaimed techs by managerId
   * @param {*} req 
   * @param {*} res 
   * @param {*} next 
   */
  findAllUnClaimedTech(req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    // Remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 

    let managerId = req.params.managerId;


    return user.findOne({
        where: { userId: managerId, 
          $or:[{roleId:ROLE_TYPE.AREA_MANAGER}] //, {roleId: ROLE_TYPE.OPS_MANAGER}
        },
        //include: [{model: managerclaimedtech, required: true, as:'unclaimtechs',
        include:[{model: user, required: true, as:'unclaimtechs', otherKey:'areaId',foreignKey:'areaId',
          where:{ roleId : ROLE_TYPE.TECH, 
              $and: { userId:db.sequelize.literal('`unclaimtechs`.`userId` NOT IN (select techId from managerclaimedtech)')} 
          }
         }],
        offset: offset ? offset : 0,
        limit: limit ? limit : 5000 , // change this to handle lazy loading. 
        subQuery: false
      })
        .then(usr => {
        if(!usr) {
          return res.status(204).send('No data to return!')
        } else {
          return res.status(200).json(usr);
        }
        })
        .catch(err => {
        logger.error('find All claimed techs invalid query', err);
        next('INVALID_QUERY');
      }
    );



  }

  /**
   * Find one claimed tech
   * @param {*} req 
   * @param {*} res 
   * @param {*} next 
   */
  findOneClaimTech(req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    let managerId = req.params.managerId; 
    let techId = req.params.techId; 

    user.findOne({
      where: { userId: managerId}
    })
	  .then(usr => {
			if (!usr || (usr.roleId != ROLE_TYPE.AREA_MANAGER && usr.roleId != ROLE_TYPE.OPS_MANAGER )) {
        return res.status(400).send('Invalid request!');
      } else {
        managerclaimedtech.findAll({ 
          where: {
            managerId: managerId, techId:techId
          },
          include:includes
        })
        .then(result => { 
          if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
          return res.status(200).json(result);
        })
        .catch(err => {
          logger.error('find one claimed tech invalid query', err);
          next('INVALID_QUERY');
        });  
      }		
    });  

  }

/**
 * Create claim tech
 */
  createClaimTech(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    let managerId = req.params.managerId; 
    let managerBodyId = req.body.managerId; 
    let techId = req.body.techId; 
    let chatName = 'am-tech' + managerId + techId; 

    if(managerId != managerBodyId) {
      return res.status(400).send('Invalid request');
    }

   return user.findOne({ 
      where: { userId: managerId }
    })
	.then(usr => {

	    if (!usr || (usr.roleId != ROLE_TYPE.AREA_MANAGER && usr.roleId != ROLE_TYPE.OPS_MANAGER )) {
        return res.status(400).send('Invalid request!');
      } 
      else 
      { 
        
        if(usr.roleId == ROLE_TYPE.OPS_MANAGER)
        {
          chatName = 'ops-tech' + managerId + techId; 

          return managerclaimedtech.findOrCreate({
            where:{ managerId: managerId, techId:techId, chatName:chatName}
          })
          .spread((createdUser, created) => {
            
            if(!created) {
                
              return res.status(400).send('Ops Manager already claimed this tech')
            }
              //**sends message to mention that you tech has been claimed */
              return conversation.create(
                {
                  "message": "Ops manager wants to chat!",
                  "senderId": managerId,
                  "receiverId": techId,
                  "priority":1, 
                  "locationId": 0
                }
              )
              .then(message => {
  
                let payload = {
                  type: 'chat',
                  senderId: message.senderId, 
                  receiverId: message.receiverId
                }
  
                let tags = "user_tag_" + message.receiverId;
                
                return this.sendNotification(tags, message.message, payload )
                .then((note) => {
                  return  res.status(200).json(message);
                })
                .catch((err) => {
                  //even thou is error -> we still send the message object. 
                  return res.status(200).json(message);
                });
                     
              });

            })
            .catch(next);  
        }
        else
        {
          return managerclaimedtech.findOrCreate({
            where:{ managerId: managerId, techId:techId, chatName:chatName}
          })
          .spread((createdUser, created) => {
            
            if(!created) {
                
              return res.status(400).send('Manager already claimed this tech')
            }
  
            return user.update(
              {managerId: managerId},
              {where: {userId: techId}}
            )
            .then(result =>{

              console.log(result)
  
              //**sends message to mention that you tech has been claimed */
              return conversation.create(
                {
                  "message": "You have been claimed by your area manager",
                  "senderId": managerId,
                  "receiverId": techId,
                  "priority":1, 
                  "locationId": 0
                }
              )
              .then(message => {
  
                let payload = {
                  type: 'chat',
                  senderId: message.senderId, 
                  receiverId: message.receiverId
                }
  
                let tags = "user_tag_" + message.receiverId;
                
                return this.sendNotification(tags, message.message, payload )
                .then((note) => {
                  return  res.status(200).json(message);
                })
                .catch((err) => {
                  //even thou is error -> we still send the message object. 
                  return res.status(200).json(message);
                });
                     
              });
            });
          })
          .catch(next);  
        }
      }
    });
    // 
  }


  //Helper function for notification 
  sendNotification( tag,message, payload)
  { 
    return notificationHub.sendPush(null, tag, message, payload);

  }

 /**
 * Updates claimed tech by techId and managerId. 
 */
  updateTechClaim(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    if (!req.body) {
      logger.error('update tech claim update error');
      return res.status(400).send('Update parameters required');
    }
    let managerId = req.params.managerId; 
    let managerBodyId = req.body.managerId; 

    if(managerId != managerBodyId){
        res.status(400).send('A manager can\'t claim for another manager');
    }
    user.findOne({
      where: { userId: managerId }
    })
    .then(usr => {
      if (!usr || (usr.roleId != ROLE_TYPE.AREA_MANAGER && usr.roleId != ROLE_TYPE.OPS_MANAGER )) {
        return res.status(400).send('Invalid request!');
      } else { 
        const conditions = { managerId: req.params.managerId, techId:req.param.techId}; 
        managerclaimedtech.update(
          req.body,
          { where:conditions
        })
        .then(doc => {
          if (!doc) { return res.status(204).end(); }
            return res.status(200).json(doc);
        })
        .catch(next);
      }
    });
  }

  /**
   * Removes the claimed tech association. 
   */
  deleteTechClaim(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
      return res.status(403).send("Invlid user request");
    }
    
      let managerId = req.params.managerId; 
      let techId = req.params.techId; 
      if(!managerId  || !techId){
        res.status(400).send('invalid params');
      }
      user.findOne({
        where: { userId: managerId }
      })
      .then(usr => {
        if (!usr || (usr.roleId != ROLE_TYPE.AREA_MANAGER && usr.roleId != ROLE_TYPE.OPS_MANAGER )) {
          return res.status(400).send('Invalid request!');
        } else { 
          const conditions = {managerId: req.params.managerId, techId:req.params.techId}; 
            
            // transaction after destroy. 
            // remove nothing. 
            managerclaimedtech.destroy({
              where: conditions 
            })
            .then(doc => {
              return res.status(200).send('Item deleted');
            })
            .catch(next);
          }
      });
  }
}

export default new AreaManagerController();