import rp                 from 'request-promise';
import adal               from 'adal-node';
import logger             from '../util/logger';
import {config}           from '../config';
import userController     from './user'; 
import azureControllerApi from './azuregraph'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

let AuthenticationContext = adal.AuthenticationContext;

class AuthtController {
  
  /*
   * Authenticate user against Azure Active Directory
   */
  login(req, res, next){
    const body = req.body;
      if (!body.username || !body.password) {
        return res.status(400).send('Invalid credentials')
      }
    logger.info('Authorizing local user.');

    let parameters = {
      clientId : config.azuread.clientId,
      clientSecret: config.azuread.clientSecret,
      username : body.username,
      password : body.password, 
    };

    let authorityUrl = config.azuread.authorityHostUrl;
    let resource = config.azuread.audience;
    let context = new AuthenticationContext(authorityUrl);

    context.acquireTokenWithUsernamePassword(
      resource,
      parameters.username,
      parameters.password,
      parameters.clientId,
      parameters.clientSecret,

      (err, tokenResponse) => {
        if (err) {
          logger.info('user:' + parameters.username);
          logger.error(err);
          return res.status(400).send('Token error');
        } else {
          req.param.username = tokenResponse.userId; ;
          req.param.accessToken = tokenResponse.accessToken;
          req.param.refreshToken = tokenResponse.refreshToken;
          req.param.expiresOn = tokenResponse.expiresOn;
          return userController.getAuthenticatedUser(req, res,next);                     
        }
      });
    }
  
  /**
   * Clear any session values from the database. 
   */
  logout(req, res, next){
    return userController.update(req,res,next);
  }
}

export default new AuthtController(); 
