import GraphAPI from 'azure-graphapi-2';
import {config} from '../config';
import {db}     from '../models';
import logger   from '../util/logger';
import {checkAuthLevel}     from '../util/security/accessCheck'; 
import * as ROLES  from '../util/roleType'; 

const userTable = db.models.user;

let graph = new GraphAPI(
  config.azuread.tenant,
  config.azuread.clientId,
  config.azuread.clientSecret
);

class AzureGraphController { 
  
  
  getUser(req,res,next){
    
    if(!checkAuthLevel(req.currentUser.roleId, ROLES.ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('getUser');
    let username = req.params.username;
    graph.get('users/' + encodeURI(username), function(err, user) {
      if (!err) {
         return res.status(200).json(user);
      } else {
        return res.status(400).send(err);
      }
    });
  }

  createAzureUser(req,res,next){

    if(!checkAuthLevel(req.currentUser.roleId, ROLES.ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('createAzureUser');
    graph.post('users/', req.body,'application/json', function(err, user) {
      if (!err) {
        console.log('success');
        console.log(user);
         return res.status(200).json(user);
      } else {
         console.log('err', err);
        return res.status(400).send(err);
      }
    })
  }

  createAzureAndAssemblersUser(req,res,next){

    if(!checkAuthLevel(req.currentUser.roleId, ROLES.ADMIN)){
      return res.status(403).send("Invlid user request");
    }

    logger.debug('createAzureAndAssemblersUser');
    let azureInfo = req.body.azureInfo;
    let assemblersInfo = req.body.assemblersInfo;
    let splitName = azureInfo.displayName.split(' ');

    graph.post('users/', azureInfo,'application/json', function(err, user) {
      if (!err) {
        logger.info('Created Azure user');
        return userTable.findOrCreate({
          where: {
            username: azureInfo.userPrincipalName
          },
          defaults: {
            areaId: assemblersInfo.areaId,
            firstName: splitName[0],
            lastName: splitName[splitName.length - 1],
            username: azureInfo.userPrincipalName,
            email: azureInfo.otherMails && azureInfo.otherMails.length > 0 ? azureInfo.otherMails[0]: null ,
            roleId: assemblersInfo.roleId,
            techId: assemblersInfo.techId,
            phone: assemblersInfo.phone, 
            active: assemblersInfo.active
          }
        })
        .then(azureCreated => {
          return userTable.findOne({
            where: {
              username: azureInfo.userPrincipalName
            }
          })
        })
        .then(result => {
          if (!result) {
            logger.error('Azure User Created, Assemblers User Failed');
            return res.status(400).json(result);
          } else {
            logger.info('Azure AD user and Assemblers DB user both created successfully, userId:', result.userId);
            return res.status(201).json(result);
          }
        })
        .catch(next);
      } else {
        logger.error(err);
        return res.status(400).send(err);
      }
    })
  }

  invalidatedTokens(req,res,next){

    if(!checkAuthLevel(req.currentUser.roleId, ROLES.GENERAL)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('invalidatedTokens');
    graph.get('users/' + encodeURI(username) + '/invalidateAllRefreshTokens', (err,response)=> {
      if (!err) {
        console.log('error ' + username, response);
        return res.status(200).json(response);
      } else {
        console.log('get ' + username , err);
        return res.status(400).send(err);
      }
    });
  }
  
   /**
   * Change Azure user password. 
   */
  changeUserPassword(username){
    
    if(!checkAuthLevel(req.currentUser.roleId, ROLES.GENERAL)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('changeUserPassword');
    graph.get('users/', function(err, user) {
      if (!err) {
        console.log(user);
      }
      // TODO: Finish
    });
  }
}

export default new AzureGraphController();
