import logger from '../util/logger';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

class BaseController {

  constructor (model) { this.model = model }

  findAll (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, this.model.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    return this.model.findAll({ where: req.query, include: this.model.includes })
    .then((result) => { 
      if(!result || !result.length > 0) {return res.status(204).send("There is no data to return");}
      return res.status(200).json(result) })
    .catch(err => {
      logger.error('BaseController findAll invalid query');
      next('INVALID_QUERY');
    });
  }

  findOne (req, res,next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, this.model.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    this.model.findOne({ where: req.params || req.query, include: this.model.includes  })
    .then((result) => { 
    return res.status(200).json(result) })
    .catch(err => {
      logger.error('BaseController findOne invalid query');
      next('Invalid Query');
    });
  }

  create(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, this.model.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    this.model.create(req.body)
    .then(doc => res.status(201).json(doc))
    .catch(err => next(err));
  }

  update(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, this.model.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    if (!req.body) {
      logger.error('baseController update error');
      return res.status(400).send('Update parameters required');  
    }
    const conditions = req.params ;
    this.model.update(req.body, { where:conditions })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(200).json(doc);
    })
    .catch(err => next(err));
  }

  destroy(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, this.model.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    this.model.destroy({ where: req.params })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(204).end();
    })
    .catch(err => next(err));
  }

};

export default BaseController;