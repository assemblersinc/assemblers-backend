import logger             from '../util/logger';
import {db}               from '../models';
import {config}           from '../config';
import BaseController     from './BaseController';
import locationController from './location'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const client = db.models.client; 
const clientattributes = db.models.clientattributes;

let includes = [
  { model: clientattributes, required: false, as:'clientattributes'}
];

class ClientController extends BaseController {

  constructor(){ super(client) }

  /**
   * Find all clients. 
   */
  findAll(req, res, next){
    
    if(!checkAuthLevel(req.currentUser.roleId, client.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    let whereClause = req.query ? req.query: {}; 
    whereClause.active = true; 
    
    client.findAll({where:whereClause, include:includes })
    .then((name) => {
      if(name && !name.length > 0){
         return res.status(204).send("There is no data to return")
      }
      res.status(200).json(name)})
    .catch(next);
  }

  /**
   * Find one clients by clientId or client code. 
   */
  findOne(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, client.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOne Client');
    client.findOne({
      where: {
        $or : [{code: req.params.clientId},{clientId :req.params.clientId }], 
        active:true
      }, 
      include:includes
    })
    .then(result => { 
      if(!result ) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

   /**
   * Find all client locations by client id.
   * - references location table model.  
   */
  findClientLocationsWithClientId(...args) {
    
    if(!checkAuthLevel(req.currentUser.roleId, client.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    locationController.findAll(...args);
  }

/**
 * Find one location by client location with client and location id
 * - references location model
 */
  findClientLocationsWithClientAndLocationId(req, res, next) {
    if(!checkAuthLevel(req.currentUser.roleId, client.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    locationController.findOne(...args);
  }

  /**
   * Find client by client name. 
   */
  findClientByName(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, client.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    client.findAll({
      where: {
        name: {
          $like: '%' + req.params.clientName + '%'
        }
      },
      include:includes,
      active:true
    })
    .then((result) => { 
      if(!result  && !result.length > 0) {return res.status(204).send("There is no data to return");}
      return res.status(200).json(result) })
    .catch(next);
  }
}

export default new ClientController();