import logger               from '../util/logger';
import {db}                 from '../models';
import BaseController       from './baseController'; 
import * as notificationHub from '../util/notificationHub';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const conversation = db.models.conversation; 
const user = db.models.user; 
const location = db.models.location; 

let includes = [
  { model: user, required: true, as:'sender'}, 
  { model: user, required: true, as:'receiver'},
  { model: location, required: false, as:'location'},
];


class ConversationController{

  constructor(){
  }
   /**
   * Find one message of a conversation. 
   */
  findAll(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAll ConversationController');
    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);
    // Remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 

    return conversation.findAndCountAll({
      where: req.query,
      include:includes,
      offset: offset ? offset : 0,
      limit: limit ? limit : 30,
      order: [['createdAt', 'DESC']]
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  
  }

   /**
   * Find one message of a conversation. 
   */
  findMyMessages(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.select)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('findMyMessages ConversationController');

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);
    let body = req.body; 

    if(!req.params && !req.body ){
      if(!result) {return res.status(400).send("Invalid request")}
    }
    //remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 
  
    return conversation.findAndCountAll({
      where: { $or:[{ 
                $or:[{senderId:req.params.senderId, receiverId:req.params.receiverId},
                     {senderId:req.params.receiverId, receiverId:req.params.senderId}]
      }]},
      include:includes,
      offset: offset ? offset : 0,
      limit: limit ? limit : 30,
      order: [['createdAt', 'DESC']]
    })
    .then(result => { 
      if(!result ) {return res.status(204).send("There is no data to return")}
      // updates conversations to mark them as read. and resets priority of msg. 
      if(body.userId == req.params.receiverId ){
        conversation.update({ read:1, priority:0 },
          { where: {conversationId: body.conversationId}
        })
        .then(update => { 
          return res.status(200).json(result);
        })
        .catch(next);
      }
      else {
        return res.status(200).json(result);
      }
    })
    .catch(next);
  }

  updateMyMessage(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('updateMyMessage ConversationController');
    return conversation.update({ read:req.body.read },
      { where: {senderId:req.params.senderId, receiverId:req.params.receiverId }
    })
    .then((result) => { 
      if(!result) {return res.status(204).send("There is no data to return");}
      return res.status(200).json(result);
    })
    .catch(next);
  }

   /**
   * Find one message of a conversation. 
   */
  findAllConversationLastMessage(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAllConversationLastMessage ConversationController');

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);
   //remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 

    // Calls store procedure to get last user message. 
    return db.sequelize.query("CALL sp_getlastchatconversationbyreceiverid(:userId, :offset, :limit)", {
        replacements: {
          userId: req.params.receiverId,
          offset: offset ? offset : 0,
          limit: limit ? limit : 30
        }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next); 
  }

  /**
   * Find one message in a conversation. 
   */
  findOneByConversationId(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOneByConversationId');
    return conversation.findOne({
      where: { conversationId: req.params.conversationId },
      include:includes
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  /**
   * Find user by name
   * TODO: add area. 
   */
  findUserByName(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findUserByName');

    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);
    //remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 

    return user.findAndCountAll({
      where: {
        $or:[
          {firstName: { $like:  '%' + req.params.term + '%' }},
          {lastName: { $like: '%' + req.params.term + '%' }}
        ]
      },
      offset: offset ? offset : 0,
      limit: limit ? limit : 30
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

  /**
   * Create a message, and send a notification to the receiver user.
   */
  create(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Create conversation');


    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);
    //remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 

    let msg = ""; 
    const body = req.body;
    if (!body.senderId || !body.receiverId) {
      return res.status(400).send('senderId and ReceiverId is required');
    }
    conversation.create(body, {include:includes})
    .then(message => {

     return conversation.findAndCountAll({
        where: { $or:[{ 
                  $or:[{senderId:req.body.senderId, receiverId:req.body.receiverId},
                       {senderId:req.body.receiverId, receiverId:req.body.senderId}]
        }]},
        include:includes,
        offset: offset ? offset : 0,
        limit: limit ? limit : 30,
        order: [['createdAt', 'DESC']]
      });

    })
    .then(result =>{

      if(!result || ( result.rows && result.rows.length == 0)) { return res.status(204).send("There is no data to return")}

      let tags = "user_tag_" + req.body.receiverId;
      // gets the last message 
      let msg = result.rows[0]
      let stringMsg = msg.message + " from " +  msg.sender.firstName + ", " + msg.sender.lastName; 

      let payload = {
        type: 'chat',
        senderId: msg.senderId, 
        receiverId: msg.receiverId
      }
      /* if the receiver is not logged in dont sent notification */
      if(!msg.receiver.token){
        return res.status(200).json(result);
      }else {

        return this.sendNotification(tags, stringMsg, payload)
              .then((note) => {
                return  res.status(200).json(result);
              })
              .catch((err) => {
                return res.status(200).json(result);
        }); 
      }

    })
    .catch(next);
  }

  /**
   * Remove current conversation messages.
   */
  remove(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, conversation.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('remove converstation message');
    return conversation.update({
      deleted: 1,
      where: {
        conversationId: req.params.conversationId
      }
    })
    .then(conversation => { return res.status(200).send('Deleted') })
    .catch(next);
  }

  //Helper function for notification 
  sendNotification( tag,message, payload)
  { 

    return notificationHub.sendPush(null, tag, message, payload);
  }

};

export default new ConversationController();