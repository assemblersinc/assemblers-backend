import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const geoareas = db.models.geoarea;
const user = db.models.user;
const region = db.models.region;

let includes = [
      {model: user, required:true, as: 'manager', otherKey:'managerId'},
      {model: user, required:false, as: 'secondManager', otherKey:'managerSecondId'},
      {model: user, required:false, as: 'thirdManager', otherKey:'managerThirdId'},
      {model: user, required:false, as: 'opsManager', otherKey:'opsManagerId'},
      {model: region, required:true, as: 'region', otherKey:'regionId'},          
]; 

class GeoAreaController {

  constructor(){}
  
  /**
   * Find all geo areas. Query params are used to filter locationId or region
   */
  findAll (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, geoareas.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAll');
    return geoareas.findAll({ where: req.query, include: includes })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(err => {

      logger.error('GeoArea findAll invalid query');
      next('INVALID_QUERY');

    });
  }

  /**
   * Find all regions and geoareas
   */
  findAllRegions (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, geoareas.permissions.select)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('findAllRegions');
    return geoareas.findAll({ include: includes })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(err => {
      logger.error('GeoArea findAll invalid query');
      next('INVALID_QUERY');
    });
  }

  /**
   * Find one geoarea with region
   */
  findOne (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, geoareas.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOne geoArea');
    geoareas.findOne({ where: req.params || req.query, include:includes  })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(err => {
      logger.error('GeoArea findOne invalid query');
      next('Invalid Query');
    });
  }
  
  /**
   * Create a geo area location 
   */
  create(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, geoareas.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('create geoArea');
    geoareas.create(req.body)
    .then(doc => res.status(201).json(doc))
    .catch(next);
  }

  /**
   * Update current geo area
   */
  update(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, geoareas.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('update geoArea');
    if (!req.body) {
      logger.error('GeoArea update error');
      return res.status(400).send('Update parameters required');  
    }
    const conditions = req.params ;
    geoareas.update(req.body, { where:conditions })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(200).json(doc);
    })
    .catch(next);
  }

  /**
   * removes the current geo area. 
   * @param {*} req 
   * @param {*} res 
   * @param {*} next 
   */
  destroy(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, geoareas.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    geoareas.destroy({ where: req.params })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(200).send('Item deleted');
    })
    .catch(next);
  }

};

export default new GeoAreaController();