import moment               from 'moment'; 
import azure                from 'azure-storage';
import logger               from '../util/logger';
import {config}             from '../config';
import {db}                 from '../models';
import invoiceExactDBQueue  from '../util/queues/invoiceExactDB';
import BaseController       from './baseController';
import {REQUEST_TYPE}   from '../util/requestType';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

// using lowercase for model names is confusing when you want to do an instance call (invoice.update -> invoice is model or instance??)
// also I would not define these vars and just refer straight to db.models for find and replace purposes. -tg
const client = db.models.client;
const clientAttributes = db.models.clientattributes;
const invoice = db.models.invoice;
const invoiceType = db.models.invoicetype;
const invoiceProduct = db.models.invoiceproduct;
const location = db.models.location;
const request = db.models.request;
const requestproduct = db.models.requestproduct;
const survey = db.models.survey;
const surveyquestion = db.models.surveyquestion;
const user = db.models.user;
const requestcreatedby = db.models.requestcreatedby; 
const requestType = db.models.requesttype;
const product = db.models.product; 

/* */

const storerepair = db.models.storerepair; 
const storeservice = db.models.storeservice; 
const storerepairticket = db.models.storerepairticket;

// Azure blob storage config
const accountName = config.AZURE_STORAGE_ACCOUNT;
const accountKey = config.AZURE_STORAGE_ACCESS_KEY;
const host = config.AZURE_STORAGE_ACCOUNT + '.blob.core.windows.net';

// Establish Azure blob storage connection
const blobService = azure.createBlobService(
	accountName,
	accountKey,
	host
);

let invoiceIncludes = [
	{
		model: user,
		attributes: {
			exclude: ['token', 'refreshToken', 'tokenExpiresOn', 'createdAt', 'updatedAt', 'hiredate']
		},
		required: true, as: 'user'
	},

	{
		model: client,
		attributes: {
			exclude: ['createdAt', 'updatedAt']
		},
		required: true, as: 'client', include: [{model: clientAttributes, required: false, as: 'clientattributes'}]
	},
	{
		model: location, 
		required: true, 
		as: 'location'
		
	},
	{
		model: invoiceType,
		attributes: {
			exclude: ['createdAt', 'updatedAt']
		},
		required: true, as: 'invoicetype'
	},
	{
		model: invoiceProduct,
		attributes: {
			exclude: ['createdAt', 'updatedAt']
		}, as: 'invoiceproduct',
		include: [
			{ model: request, required: true, as: 'request', include: [
				{ model:requestType, require: true, as:'requesttype'}]
			}
		] 
	}
];

class InvoiceController extends BaseController {

	constructor() {
		super(invoice)
	}

	// ========================
	//        Invoices
	// ========================
	findAllInvoice(req, res, next) {
		
		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findAllInvoice');
		let query = req.query;
		let offset = parseInt(query.offset);
		let limit = parseInt(query.limit);

		return invoice.findAndCountAll({
			offset: offset ? offset : 0,
			limit: limit ? limit : 30,
			where: query,
			limit: 30,
			include: invoiceIncludes
		})
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	findAllInvoiceCompleted(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findAllInvoiceCompleted');
		let query = req.query;
		query.invoiceCompletedTime = {$ne: null}

		let offset = parseInt(query.offset);
		let limit = parseInt(query.limit);

		return invoice.findAndCountAll({
			offset: offset ? offset : 0,
			limit: limit ? limit : 30,
			where: query,
			include: invoiceIncludes
		})
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	findAllInvoiceCurrent(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findAllInvoiceCurrent');
		let query = req.query;

		let whereClause = {
			invoiceCompletedTime: {$eq: null},
			invoiceGeneratedTime: {$ne: null},
			invoiceGeneratedTime: {
				$between: [moment(Date.now()).add(-30, 'days').format('YY-MM-DD'), moment(Date.now()).add(1, 'days')
				.format('YY-MM-DD')]
			},
			userId: req.query.userId,
			locationId: req.query.locationId,
			clientId: req.query.clientId
		}

		let offset = parseInt(query.offset);
		let limit = parseInt(query.limit);

		return invoice.findAndCountAll({
			offset: offset ? offset : 0,
			limit: limit ? limit : 30,
			where: whereClause,
			include: invoiceIncludes,
			order: [['invoiceGeneratedTime', 'DESC']]
		})
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	findOneInvoice(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findOneInvoice');
		let query = req.query;
		query.invoiceId = req.params.invoiceId
		return invoice.findOne({
			where: query,
			include: invoiceIncludes
		})
		.then(result => res.status(200).json(result))
		.catch(next);
	}

	findInvoiceHistory(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findInvoiceHistory');
		const rick = req.query;

		//Query for Completed Invoice
		let query = Object.assign({}, req.query);
		query.invoiceCompletedTime = {$ne: null, $lt: new Date(new Date().getTime() + 9 * 24 * 60 * 60 * 1000)};

		let offset = parseInt(query.offset);
		let limit = parseInt(query.limit);

		//Query for Current Invoice
		let currentQuery = Object.assign({}, req.query);
		currentQuery.invoiceCompletedTime = null;
		currentQuery.invoiceGeneratedTime = {$ne: null};

		Promise.all([
			invoice.findAndCountAll({
				offset: offset ? offset : 0
				, limit: limit ? limit : 50
				, where: query,
				include: invoiceIncludes,
				subQuery: false, 
				order: [['invoiceGeneratedTime', 'DESC']]
			}),
			invoice.findAndCountAll({
				offset: offset ? offset : 0
				, limit: limit ? limit : 50
				, where: currentQuery,
				include: invoiceIncludes, 
				subQuery: false, 
				order: [['invoiceCompletedTime', 'DESC']]
			}).catch(next)

		]).then((values) => {

			var result = values[0];

			for (var i = 1; i < values.length; i++) {

				result.rows = result.rows.concat(values[i].rows);
				result.count += values[i].count;

			}

			res.status(200).json(result);
		});
	}

	createInvoice(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.create)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('createInvoice')
		let body = req.body;
		// TODO: INVOICE NUMBER NEEDS TO BE GENERATED BY EXACT DB
		body.invoiceNumber = Math.random().toString(36).slice(2);
		// TODO ADD EXACT DB INTEGRATION
		return invoice.create(body)
		.then(result => res.status(201).json(result))
		.catch(next);
	}

	removeInvoice(req, res, next) {
		// TODO: This should only be available to Admin role
		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.delete)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('removeInvoice');
		return invoice.destroy({
			where: {invoiceId: req.params.invoiceId}
		})
		.then(result => res.status(203).json(result))
		.catch(next);
	}

	updateInvoice(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.update)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('updateInvoice');
		let body = req.body;
		return invoice.update(
			body,
			{
				where: {
					invoiceId: req.params.invoiceId,
					// Only update uncompleted invoices
					invoiceCompletedTime: null
				}
			})
		.then(update => {
			invoice.findOne({
				where: {invoiceId: req.params.invoiceId},
				include: invoiceIncludes

			})
			.then(result => res.status(200).json(result))
		})
		.catch(next);
	}

	updateMultipleInvoices(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.update)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('updateMultipleInvoices');
		let body = req.body; // Array of invoice objects

		if (!body.length) {
			return next('INVALID_REQUEST');
		}

		let invoiceIdList = [];
		let invoiceCompletedIdList = [];
		let newCompletionIds = [];

		body.forEach((invoice) => {
			if (invoice.invoiceId) {
				invoiceIdList.push(invoice.invoiceId);
			}

			if (invoice.invoiceCompletedTime) {
				invoiceCompletedIdList.push(invoice.invoiceId);
			}
		});

		return db.sequelize.transaction(t => {

			//find new completions.
			return invoice.findAll({
				where: {
					invoiceId: invoiceCompletedIdList,
					invoiceCompletedTime: null
				},
				transaction: t
			})
			.then((incompleteInvoices) => {
				incompleteInvoices.forEach((incompleteInvoice) => {
					newCompletionIds.push(incompleteInvoice.invoiceId);
				});
			})
			.then(() => {
				// Loop through array of objects
				return Promise.all(body.map(function (invoiceObject) {
					// Copy invoiceId out of the array's objects, then remove from those objects
					let invoiceId = invoiceObject.invoiceId;
					delete invoiceObject.invoiceId;

					return invoice.update(
						invoiceObject,
						{where: {invoiceId: invoiceId}},
						{transaction: t}
					)
					.then((updated) => {
						if (req.body.force || invoiceCompletedIdList.indexOf(invoiceId) !== -1 || invoiceCompletedIdList.indexOf(invoiceId) !== 'N/A') {
							let jobIncludes = [
								{model: location, as: 'location'},
								{
									model: user,
									attributes: ['techId'],
									required: true, as: 'user'
								},
								{
									model: client,
									attributes: {
										exclude: ['createdAt', 'updatedAt']
									},
									required: true, as: 'client'
								},

								{
									model: invoiceProduct,
									attributes: {
										exclude: ['createdAt', 'updatedAt']
									}, as: 'invoiceproduct'
								},
							];

							

							//post update job
							return invoice.findById(invoiceId, {transaction: t, include: jobIncludes})
							
							.then((reloadedInvoice) => {
								// force add the invoice completed time to the job as the transaction has not yet been committed. 
								reloadedInvoice.invoiceCompletedTime =  invoiceObject.invoiceCompletedTime; 
								console.log('invoice completed time is ',reloadedInvoice.invoiceCompletedTime  );
								logger.log('Adding job for ' + reloadedInvoice.invoiceId);
								return invoiceExactDBQueue.addJob(reloadedInvoice);
							});
						} else {
							console.log('Skipping job for ' + invoiceId);
						}
						
						return Promise.resolve();
					});


				}));
			})
		})
		.then(update => {
			return invoice.findAll({
				where: {invoiceId: invoiceIdList}, // An array of invoiceIds
				include: invoiceIncludes
			})
			.then(result => res.status(200).json(result))
		})
		.catch(next);
	}

	// ========================
	//      Invoice Types
	// ========================
	findAllInvoiceType(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findAllInvoiceType');
		let query = req.query;
		return invoiceType.findAll({where: query})
		.then(result => {
			if (!result || !result.length > 0) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	findOneInvoiceType(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findOneInvoiceType');
		let query = req.query;
		query.invoiceTypeId = req.params.invoiceTypeId
		return invoiceType.findOne({where: query})
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	createInvoiceType(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoiceType.permissions.create)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('createInvoiceType')
		let body = req.body;
		return invoiceType.create(body)
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	removeInvoiceType(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoiceType.permissions.delete)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('removeInvoiceType');
		return invoiceType.destroy({
			where: {invoiceTypeId: req.params.invoiceTypeId}
		})
		.then(result => res.status(202).json(result))
		.catch(next);
	}

	updateInvoiceType(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoiceType.permissions.update)){
			return res.status(403).send("Invlid user request");
		}
		
		logger.info('updateInvoiceType');
		let body = req.body;
		return invoiceType.update(
			body,
			{
				where: {invoiceTypeId: req.params.invoiceTypeId}
			})
		.then(result => res.status(200).json(result))
		.catch(next);
	}

	// ========================
	//     Invoice Product
	// ========================
	findAllInvoiceProduct(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoiceType.permissions.select)){
			return res.status(403).send("Invlid user request");
		}
		
		logger.info('findAllInvoiceProduct');
		let query = req.query;
		return invoiceProduct.findAll({where: query})
		.then(result => {
			if (!result || !result.length > 0) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	findOneInvoiceProduct(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoiceProduct.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('findOneInvoiceProduct');
		let query = req.query;
		query.invoiceProductId = req.params.invoiceProductId
		return invoiceType.findOne({where: query})
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	createInvoiceProduct(req, res, next) {
		if(!checkAuthLevel(req.currentUser.roleId, invoiceProduct.permissions.create)){
			return res.status(403).send("Invlid user request");
		}
		logger.info('createInvoiceProduct')
		let body = req.body;
		return invoiceProduct.create(body)
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	removeInvoiceProduct(req, res, next) {
		
		if(!checkAuthLevel(req.currentUser.roleId, invoiceProduct.permissions.delete)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('removeInvoiceProduct');
		return invoiceProduct.destroy({
			where: {invoiceProductId: req.params.invoiceProductId}
		})
		.then(result => res.status(202).json(result))
		.catch(next);
	}

	updateInvoiceProduct(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoiceProduct.permissions.update)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('updateInvoiceProduct');
		let body = req.body;
		return invoiceProduct.update(
			body,
			{
				where: {invoiceProductId: req.params.invoiceProductId}
			})
		.then(result => res.status(200).json(result))
		.catch(next);
	}

	removeRepairItemFromInvoice(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.update)){
			return res.status(403).send("Invlid user request");
		}

		let body = req.body ; 
		
		logger.info('removeRepairItemFrominvoice');
		
		return db.sequelize.transaction(t => {
			
			return storerepair.update(
				{
					completed: false
				},
				{
					where: {
						id: body.requestItemId
					},
					transaction: t
				}
			)
			.then (resultUpdate =>{

				return storerepair.findOne(
					{
						where: {
							id: body.requestItemId
						},
						transaction: t
					}
				); 

			})
			.then(storeRepair => {
				
				return storerepairticket.update(
					{
						completed: false
					},
					{
						where: {
							storeRepairId: storeRepair.storeRepairId
						},
						transaction: t
					}
				);
				
			})
			.then(updatedRepairTicket =>{

				return requestcreatedby.destroy({
						where: 
						{
							requestId: body.requestId
						}
					},
					{transaction: t}
				)
				.then(deleted => {
					
					//removes the request for repairs we only insert request upon invoicing. 
					return request.destroy(
						{
							where: 
							{
								requestId: body.requestId
							}
						},
						{transaction: t}
					);

				})
				
			})
			.then(requestRemoved => {

				return invoice.findOne(
					{
						where: {
							invoiceId: body.invoiceId
						}
					},
					{transaction: t}
				);

			})
			.then(invoiceFindResult => {

				if(invoiceFindResult.total != 0 ){
					return invoiceFindResult.decrement(
						'total' // check subtracting is done correctly
					, {by:  body.pricePerUnit})
				}
				return invoiceFindResult; 
			})
			.then(invoiceDecrement => {
				
				return invoiceProduct.destroy(
					{
						where: {
							invoiceId: body.invoiceId,
							itemId: body.itemId, 
							requestId : body.requestId, 
							entryType : 'R'
						}
					},
					{transaction: t}
				)	

			})
			
		})
		.then(destroyed => {

			return invoice.findOne(
				{
					where: {invoiceId: body.invoiceId},
					include: invoiceIncludes
				}
			);

		})
		.then(finalResult => {

			if (!finalResult) {
				return res.status(204).json(body)
			}

			return res.status(200).json(finalResult);

		})
		.catch(next);
	}

	removeServiceItemFromInvoice(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.update)){
			return res.status(403).send("Invlid user request");
		}
		
		let body = req.body ; 
		logger.info('removeServiceItemFrominvoice');
		
		return request.findOne({

			where: { 
				requestId: body.requestId 
			}, 
			include: [ {model: storeservice, as: 'requestservice', where : { storeServiceId: body.requestItemId }} ]

		})
		.then(requestResult => {

			if(!requestResult){
				return res.status(400).send("Invalid request");
			}
			
			let requestServiceObj =  requestResult.requestservice[0]; // gets the service request 
	
			if(requestResult.requestTypeId == REQUEST_TYPE.ON_DEMAND_REQUEST){

				if((requestServiceObj.qtyServiced -1) <= 0 )
				{

					return db.sequelize.transaction(t => {	

						return invoiceProduct.destroy(
							{
								where: {
									invoiceId: body.invoiceId,
									itemId: body.productId, 
									requestId : body.requestId
								}
							},
							{transaction: t}
						)
						.then(invoiceDestroied => {
							return storeservice.destroy(
								{
									where: {
										storeServiceId: requestServiceObj.storeServiceId
									}
								},
								{transaction: t}
							);
						})

					})
					.then(result=>{
						return invoice.findOne(
							{
								where: {invoiceId: body.invoiceId}
							}
						)
					})
					.then(result=>{
						return result.decrement(
							'total' 
						,{ 
							by: body.pricePerUnit
						}); 
					})
					.then(result=>{
						return invoice.findOne(
							{
								where: {invoiceId: body.invoiceId}
							}
						)
					})
					.then(result =>{
						
						if (!result) {
							return res.status(204).json("no invoice available")
						}
						return res.status(200).json(result);
					
					})
					.catch(next); 	
				}
				else  // if qty is not 0 
				{
					return db.sequelize.transaction(t => {
						return invoiceProduct.findOne({
							where: {
								invoiceId: body.invoiceId,
								itemId: body.productId, 
								requestId : body.requestId
							}
						})
						.then(result => {
							return result.decrement(
								'qty' 
							,{ 
								by: 1,
								transaction: t 
							})
							.then(subtractedQty => {

								return result.decrement(
									'total' 
								,{ 
									by: result.pricePerUnit, 
									transaction: t 
								}); 
							}); 
						})
						.then(result=>{
							return storeservice.update(
								{
									qtyServiced: requestServiceObj.qtyServiced -1, 
									qtyTotal: requestServiceObj.qtyTotal -1 
								},
								{
									where: {
										storeServiceId: requestServiceObj.storeServiceId
									}
								},
								{transaction: t}
							)
						})
						
					})
					.then(result=>{
						return invoice.findOne(
							{
								where: {invoiceId: body.invoiceId}
							}
						)
					})
					.then(result=>{
						return result.decrement(
							'total' 
						,{ 
							by: body.pricePerUnit
						}); 
					})
					.then(result=>{

						return invoice.findOne(
							{
								where: {invoiceId: result.invoiceId},
								include: invoiceIncludes
							}
						)
					})
					.then(result =>{
						
						if (!result) {
							return res.status(204).json("no invoice available")
						}
						return res.status(200).json(result);
					
					})
					.catch(next); 

				}			
				
			}
			else 
			{

				return db.sequelize.transaction(t => {
					return storeservice.findOne({
						where:{ storeServiceId: body.requestItemId }
					})
					.then(result =>{

						return storeservice.update(
							{
								completed: false,
								completedTime: null,
								qtyServiced: ((result.qtyServiced == 0) ? 0 : result.qtyServiced - 1)
							},
							{
								where: {
									storeServiceId: result.storeServiceId
								},
								transaction: t
							}
						);

					})
					.then(updatedStoreService => {

						return request.update(
							{
								completed: false, 
								endTime: null, 
							},
							{
								where: {
									requestId: body.requestId
								}
							},
							{transaction: t}
						)
					})
					.then(requestUpdateResult => {
						return invoice.findOne(
							{
								where: {
									invoiceId: body.invoiceId
								}
							},
							{transaction: t}
						)
					})
					.then(invoiceFindResult => {
						return invoiceFindResult.decrement(
							'total' 
						, {by:  body.pricePerUnit})
					})
					.then(invoiceDecrement => {
						
						if((body.qty -1) <= 0 ){

							return invoiceProduct.destroy(
								{
									where: {
										invoiceId: body.invoiceId,
										itemId: body.productId, 
										requestId : body.requestId, 
										productId: body.productId
									}
								},
								{transaction: t}
							);							
						}
						else{
							return invoiceProduct.findOne({
								where: {
									invoiceId: body.invoiceId,
									itemId: body.productId, 
									requestId : body.requestId, 
									productId: body.productId
								}
							})
							.then(result => {
								return result.decrement(
									'qty' // check subtracting is done correctly
								,{ 
									by: 1,
									transaction: t 
								})
								.then(subtractedQty => {

									return result.decrement(
										'total' // check subtracting is done correctly
									,{ 
										by: result.pricePerUnit, 
										transaction: t 
									}); 
								}); 
							}); 
						}	
					})
					.then(destroyed => {
						return invoice.findOne(
							{
								where: {invoiceId: body.invoiceId},
								include: invoiceIncludes
							},
							{transaction: t}
						)
					})
					.then(finalResult => {
						if (!finalResult) {
							return res.status(204).json(body)
						}
						return res.status(200).json(finalResult);
					})
				})
				.catch(next);

			}
			
		})
		.catch(next);
				
	}

	removeInvoiceProductFromInvoice(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.update)){
			return res.status(403).send("Invlid user request");
		}
		
		logger.info('removeInvoiceProductFromInvoice');
		let body = req.body; // invoiceProduct object
		
		// checks if the item to remove is a repair and redirects the function 
		if(body.entryType == 'R'){
			return this.removeRepairItemFromInvoice(req, res, next)
		}
		else if(body.entryType == 'S'){
			return this.removeServiceItemFromInvoice(req, res, next)
		}
		else if(body.entryType == 'P') {
					
			logger.info('removeInvoiceProductFromInvoice');

			return request.findOne({
				where: { 
					requestId: body.requestId 
				},
				include: [ {model: requestproduct, as: 'requestproduct', where : { requestProductId: body.requestItemId }} ]
			})
			.then(requestResult => {

				if(!requestResult){
					return res.status(400).send("Invalid request");
				}
				
				let requestProductObj = null; 

				return db.sequelize.transaction(t => {
					
					return requestproduct.findOne(
					{
						where: { requestProductId: body.requestItemId }
					})
					.then (rpResult => {
						
						// copies the result to the object. 
						requestProductObj = rpResult; 
						// for OnDemand we reduce the total assembled and the total requested. 
						if(requestResult.requestTypeId == REQUEST_TYPE.ON_DEMAND_REQUEST)
						{
							return requestproduct.update(
								{
									completed: false,
									completedTime: null,
									qtyAssembled: ((rpResult.qtyAssembled == 0) ? 0 : rpResult.qtyAssembled - 1),
									qtyTotal: ((rpResult.qtyAssembled == 0) ? 0 : rpResult.qtyAssembled - 1)
								},
								{
									where: {
										requestProductId: requestProductObj.requestProductId
									},
									transaction: t
								}
							)
						}
						else {

							return requestproduct.update(
								{
									completed: false,
									completedTime: null,
									qtyAssembled: ((rpResult.qtyAssembled == 0) ? 0 : rpResult.qtyAssembled - 1)
								},
								{
									where: {
										requestProductId: requestProductObj.requestProductId
									},
									transaction: t
								}
							)
						}
					
					})
					.then(requestProductResult => {
						return request.update(
							{
								completed: false, 
								endTime: null, 
							},
							{
								where: {
									requestId: body.requestId
								}
							},
							{transaction: t}
						)
					})
					.then(requestUpdateResult => {
						return invoice.findOne(
							{
								where: {
									invoiceId: body.invoiceId
								}
							},
							{transaction: t}
						)
					})
					.then(invoiceFindResult => {
						return invoiceFindResult.decrement(
							'total' // check subtracting is done correctly
						, {by:  body.pricePerUnit})
					})
					.then(invoiceDecrement => {

						console.log('invoice decrement cost', invoiceDecrement)
						
						if((requestProductObj.qtyAssembled -1) <= 0 ){
							return invoiceProduct.destroy(
								{
									where: {
										invoiceId: body.invoiceId,
										itemId: body.productId, 
										requestId : body.requestId, 
										requestItemId: body.requestItemId
									}
								},
								{transaction: t}
							);
						}
						else
						{
							return invoiceProduct.findOne({
								where: {
									invoiceId: body.invoiceId,
									itemId: body.productId, 
									requestId : body.requestId, 
									productId: body.productId
								}
							})
							.then(result => {
								return result.decrement(
									'qty' // check subtracting is done correctly
								,{ 
									by: 1,
									transaction: t 
								}
								)
								.then(subtractedQty => {
									return result.decrement(
										'total' // check subtracting is done correctly
									,{ 
										by: result.pricePerUnit, 
										transaction: t 
									}
									); 
								}); 
							})
							
						}
					})
					.then(destroyed => {

						return invoice.findOne(
							{
								where: {invoiceId: body.invoiceId},
								include: invoiceIncludes
							},
							{transaction: t}
						)
					})
				})
				.then(result => {
					// removes the request product on onDemand request. 
					if(requestResult.requestTypeId == REQUEST_TYPE.ON_DEMAND_REQUEST  
						&& (requestProductObj.qtyAssembled -1) <= 0 )
					{

						return requestproduct.destroy({
							where: { requestProductId: body.requestItemId }
						})
						.then(destroyed =>
						{

							if (!result) {
								return res.status(204).json(body)
							}
							return res.status(200).json(result);
						});
						
					}
					else {
					
						if (!result) {
							return res.status(204).json(body)
						}
						return res.status(200).json(result);

					}

				})
				.catch(next);

			});
		}
		else 
		{
			return res.status(400).send("Invalid request!")
		}
	}

	// ========================
	//        Signatures
	// ========================

	saveSurveyWithInvoiceSignature(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, survey.permissions.create)){
			return res.status(403).send("Invlid user request");
		}
		/*
			Client sends a request containing:
				1. Array of invoice objects,
				2. Array of completed survey questions,
				3. Base64-encoded image object with file type indicator
		*/
		logger.info('saveSurveyWithInvoiceSignature');
		let body = req.body;
		// Check that we have everything we need in req.body
		if (!body.invoices || !body.survey || !body.signature) {
			return res.status(400).send('Missing request inputs')
		}
		// Create blobName based on 'YYYY-MM-DD-invoiceId-<invoiceNumber1-<invoiceNumber2>-etc.'
		let invoiceArrayList = req.body.invoices.join('-');
		let date = new Date();
		let blobName = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDay() + '-invoiceId-' + invoiceArrayList + '.' + body.signature.type;
		let contentTypeString = 'image/' + body.signature.type;

		// Here we need to convert the request's base64 string into binary
		if (typeof Buffer.from === "function") {
			// Buffer() functionality for Node 5.10+
			var buf = Buffer.from(body.signature.data, 'base64');
		} else {
			// Buffer() for older Node versions ... deprecated
			var buf = new Buffer(body.signature.data, 'base64');
		}

		// Step 1: Upload signature to Azure blob storage
		return new Promise((resolve, reject) => {
			blobService.createBlockBlobFromText(
				config.azurestorage.containerNameSignatures,
				blobName,
				buf,
				{
					contentSettings: {
						contentType: contentTypeString,
						contentEncoding: 'base64'
					}
				},
				function (error, result, response) {
					if (error) {
						logger.info(error);
						reject("Couldn't upload signature image string");
					} else {
						logger.info("Result blob:", result);
						resolve({url: 'https://' + host + '/' + config.azurestorage.containerNameSignatures + '/' + blobName + config.azurestorage.signature});
					}
				}
			)
		})
		// Step 2: Update the related db tables
		.then(blobResponse => {
			return db.sequelize.transaction(t => {
				// Save survey questions
				return survey.bulkCreate(
					body.survey,
					// Upsert data if client is providing previously-answered questions
					{updateOnDuplicate: ['userId', 'locationId', 'questionId', 'invoiceId', 'enteredByName', 'response', 'updatedAt']},
					{transaction: t}
				)
				.then(surveyResponse => {
					// Add blobUrl to related invoice table entries
					return invoice.update(
						{signatureImageUrl: blobResponse.url},
						{
							where:
								{
									invoiceId:
										{$in: body.invoices}
								}
						},
						{transaction: t}
					)
				})
				.then(invoiceResponse => {
					return invoice.findAll({
						where: {invoiceId: {$in: body.invoices}},
						include: invoiceIncludes
					})
				})
			})
		})
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	getSignature(req, res, next) {

		logger.info('getSignature');
		let body = req.body;
		// TODO: Finish

	}

	applySignature(req, res, next) {
		// TODO: Finish
	}

	getSurveyAnswersByInvoiceId(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, survey.permissions.select)){
			return res.status(403).send("Invlid user request");
		}

		logger.info('getSurveyAnswersByInvoiceId');
		return survey.findAll({
			where: {
				invoiceId: req.query.invoiceId,
				active: 1
			},
			include: includes
		})
		.then(result => {
			if (!result || !result.length > 0) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}

	// ========================
	//          Stamps
	// ========================

	uploadStampImagesAndUpdateInvoices(req, res, next) {

		if(!checkAuthLevel(req.currentUser.roleId, invoice.permissions.update)){
			return res.status(403).send("Invlid user request");
		}
		/*
			Client sends a request containing:
				1. Array of invoice objects,
				2. Base64-encoded image object with file type indicator
		*/
		logger.info('uploadStampImagesAndUpdateInvoices');
		let body = req.body;
		// Check that we have everything we need in req.body
		if (!body.invoices || !body.stamp) {
			return res.status(400).send('Missing request inputs')
		}
		// Create blobName based on 'YYYY-MM-DD-invoiceId-<invoiceNumber1-<invoiceNumber2>-etc.'
		let invoiceArrayList = req.body.invoices.join('-');
		let date = new Date();
		let blobName = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDay() + '-invoiceId-' + invoiceArrayList + '.' + body.stamp.type;
		let contentTypeString = 'image/' + body.stamp.type;

		// Here we need to convert the request's base64 string into binary
		if (typeof Buffer.from === "function") {
			// Buffer() functionality for Node 5.10+
			var buf = Buffer.from(body.stamp.data, 'base64');
		} else {
			// Buffer() for older Node versions ... deprecated
			var buf = new Buffer(body.stamp.data, 'base64');
		}

		// Step 1: Upload stamp to Azure blob storage
		return new Promise((resolve, reject) => {
			blobService.createBlockBlobFromText(
				config.azurestorage.containerNameStamps,
				blobName,
				buf,
				{
					contentSettings: {
						contentType: contentTypeString,
						contentEncoding: 'base64'
					}
				},
				function (error, result, response) {
					if (error) {
						logger.info("Couldn't upload stamp image");
						logger.info(error);
						reject("Couldn't upload stamp image");
					} else {
						logger.info("Result blob:", result);
						resolve({url: 'https://' + host + '/' + config.azurestorage.containerNameStamps + '/' + blobName + config.azurestorage.signature});
					}
				})
		})
		// Step 2: Update the related db tables
		.then(blobResponse => {
			return db.sequelize.transaction(t => {
				// Add blobUrl to related invoice table entries
				return invoice.update(
					{stamp: blobResponse.url},
					{
						where:
							{
								invoiceId:
									{$in: body.invoices}
							}
					},
					{transaction: t}
				)
			})
			.then(invoiceResponse => {
				return invoice.findAll({
					where: {invoiceId: {$in: body.invoices}},
					include: invoiceIncludes
				})
			})
		})
		.then(result => {
			if (!result) {
				return res.status(204).send("There is no data to return")
			}
			return res.status(200).json(result);
		})
		.catch(next);
	}
}

export default new InvoiceController(); 