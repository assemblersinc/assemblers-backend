import moment         from 'moment';
import {config}       from '../config';
import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const location = db.models.location;
const client = db.models.client;
const geoarea = db.models.geoarea; 
const region = db.models.region; 
const user = db.models.user;  
const storeassemblycounts = db.models.storeassemblycount;

//for counts performed on current week. 
const startWeekDate = moment(Date.now()).startOf('isoweek').isoWeekday(1).format('YY-MM-DD');
const endWeekDate = moment(Date.now()).endOf('week').add(1, 'd').format('YY-MM-DD')

const includes = [
  
  { model: client, 
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    required: true, as:'client'},
    
  { model: geoarea, 
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    },
    required: true, as:'geoarea', 
    include: [
          {model:region, required:true, as:'region', foreingKey:'regionId'},
          {model: user, required:false, as: 'manager', otherKey:'managerId'},
          {model: user, required:false, as: 'secondManager', otherKey:'managerSecondId'},
          {model: user, required:false, as: 'thirdManager', otherKey:'managerThirdId'},
          {model: user, required:false, as: 'opsManager', otherKey:'opsManagerId'}]
  },
  { model: storeassemblycounts, required: false, as:'storeassemblycounts', 
    where:{countedDate: {
      $between: [ startWeekDate,endWeekDate ]
    }}
  }, 
]; 

class LocationController extends BaseController {
  
  constructor(){ super(location); }
  
  findAll(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, location.permissions.select)){
			return res.status(403).send("Invlid user request");
    }
    
    logger.info('findAll Location');
    let query = req.query;
    query.active = true;
    location.findAll({
      where: query,
      include: includes
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  findOne(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, location.permissions.select)){
			return res.status(403).send("Invlid user request");
    }
    
    logger.info('findOne Location')
    let query = req.query;
    query.locationId = req.params.locationId;
    return location.findOne({
        where: query,
        include: includes
    })
    return client.findOne({
      where: { clientId : req.params.clientId, active: true},
        include: [{model: location, required:true, as: 'location' ,where:{
          $or: [{storeNumber: req.params.storeNumber}, {storeNumber: '0' + req.params.storeNumber}]
        }}]
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

   findOneByStoreNumber(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, location.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findOne Location by store number')
    if(!req.params.storeNumber || !req.params.clientId){
      res.status(400).send("Store number or client id is missing"); 
    }
    return client.findOne({
      where: { clientId : req.params.clientId, active:true},
        include: [{model: location, required:true, as: 'location' ,where:{
          $or: [{storeNumber: req.params.storeNumber}, {storeNumber: '0' + req.params.storeNumber}]
        }}]
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  searchLocationByStateCityZip(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, location.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findOne Location by store number')
    let body = req.query; 
    if(!body || !body.clientId){
      res.status(400).send("Store number or client id is missing"); 
    }

    return client.findOne({
      where: { clientId : body.clientId},
        include: [{model: location, required:true, as: 'location' ,where:body
        , attributes: ['locationId'
        , 'clientId', 'Location', 'phoneNumber', 'storeNumber', 'areaId', 'address', 'city', 'state', 'zip', 'district', 'lat', 'lon',
         'timezone', 'active',
        [db.Sequelize.literal('`client`.`name`'),'name']]}]
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  

  }

  findAllByZipCode(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, location.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllByZipCode');
    const body = req.body;
    if (!body.zip) {
      logger.error('zip code required');
      return res.status(400).send('Zip Code required'); 
    };
    return location.findAll({
      where: { zip: body.zip },
      include: includes
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  findStoresNearUserGps (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, location.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findStoresNearUserGps');
    const body = req.body;
    if (!body.lat || !body.lon) {
      logger.error('lat and lon required');
      return res.status(400).send('lat and lon required');
    }

    // This query will additionally return a 'DistMiles' measurement for each location object 
    let findNearby = "call sp_findnearbystore(:lat_input, :lon_input, :minLat, :minLon, :maxLat, :maxLon, :R , :rad)";

    let getRadians = function(degrees) {
      return degrees * Math.PI / 180;
    };

    let getDegrees = function(radians) {
      return radians / (Math.PI / 180);
    };

    const R = 3959; // average radius of Earth in miles
    let radius = 0.31; // radius of our "search area" in miles
    let maxLat = body.lat + getDegrees(radius/R);
    let minLat = body.lat - getDegrees(radius/R);
    let maxLon = body.lon + getDegrees(Math.asin(radius/R) / Math.cos(getRadians(body.lat)));
    let minLon = body.lon - getDegrees(Math.asin(radius/R) / Math.cos(getRadians(body.lat)));
    console.log(minLat, body.lat, maxLat, minLon, body.lon, maxLon)
    return db.sequelize.query(findNearby,
    { 
      replacements: {
        lat_input: getRadians(body.lat),
        lon_input: getRadians(body.lon),
        minLat: minLat,
        minLon: minLon,
        maxLat: maxLat,
        maxLon: maxLon,
        rad: radius,
        R: R
      }
    })
    .then((locations) => {
      if (!locations) {
        return res.status(204).send('No nearby locations found.');
        } else {
        return res.status(200).json(locations);
        }
    })
    .catch(next);

  }
};

export default new LocationController();
