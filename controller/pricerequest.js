import logger         from '../util/logger';
import {db}           from '../models';
import {email}        from '../util/mail';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const pricingrequest  = db.models.pricingrequest; 
const client          = db.models.client; 
const user            = db.models.user; 
const location        = db.models.location; 

let includes = [
     {model: user, required:true, as: 'user', otherKey:'userId'},
     {model: client, required:false, as: 'client', otherKey:'clientId'},
     {model: location, required:false, as: 'location', otherKey:'locationId'}     
];

  let mailOptions = {
      from: 'no-reply@assemblersinc.net', 
      to: 'n.schwartz@assemblersinc.net', // TODO: update to support email 
      subject: 'PRICE REQUEST FROM APP', // Subject line
      text: ''
  };

class PricingRequestController {

  constructor(){ }

  findAll (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, pricingrequest.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAll pricerequest')
    return pricingrequest.findAll({ where: req.query, include: includes})
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(err => {
      logger.error('pricing request findAll invalid query');
      next('INVALID_QUERY');
    });
  }

  findBySku(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, pricingrequest.permissions.select)){
			return res.status(403).send("Invlid user request");
    }
    
    logger.info('findBySku pricerequest');
    return pricingrequest.findOne({
      where: { $or :[ {sku: req.params.sku}, {priceRequestId: req.params.sku}] },
      include: includes
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  create(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, pricingrequest.permissions.create)){
			return res.status(403).send("Invlid user request");
    }
    
    logger.info('Create pricerequest');
    const body = req.body;
    if (!body.sku) {
      logger.error('sku is requred needed');
      return res.status(400).send('Sku is required');
    }
    /** Where Clause */
    let whereOrClause = {}
    
    if (req.body.sku){
      whereOrClause.sku = req.body.sku;
    }

    if (req.body.upc){
      whereClause.upc = req.body.upc;
    }

    console.log('body', req.body); 
    pricingrequest.findOrCreate({
      where: {
        $or :[ whereOrClause], 
        $and:[{locationId: req.body.locationId}]
      },
      defaults: req.body,
      include: includes
    })
    .spread((result, created) => { 
      
      console.log('result', result);

      if(!result) { return res.status(204).send("There is no data to return") }
      
      // Send mail with defined transport object
     
      if(created){

       return  pricingrequest.findOne({
          where: {
           priceRequestId: result.priceRequestId
          },
          include: includes
        })
        .then(priceRequest => {

          mailOptions.text = 'Below find the details of the application requet items \n\n'
          + ' SKU: ' + priceRequest.sku + ' \n\n'  
          + ' UPC: ' + priceRequest.upc + ' \n\n'
          + ' Description: ' + priceRequest.description + ' \n\n'
          + ' ClientId: ' + priceRequest.clientId + ' \n\n'
          + ' Client: ' + (priceRequest.client != null ? priceRequest.client.name : '') + ' \n\n'
          + ' LocationId: ' + priceRequest.locationId + ' \n\n'
          + ' Location: ' + (priceRequest.location != null  ? priceRequest.location.Location  : '') + ' \n\n'
          + ' Location phone: ' + (priceRequest.location != null  ? priceRequest.location.phoneNumber  : '') + ' \n\n'
          + ' userId: ' + priceRequest.userId + ' \n\n'
          + ' Name: ' + (priceRequest.user != null ? priceRequest.user.firstName + ' ' + priceRequest.user.lastName : '') +  ' \n\n';
          
          email.sendMail(mailOptions, (error, info) => {
            if (error) {
              logger.error(error); 
              return res.status(200).json(priceRequest);
            }
            return res.status(200).json(priceRequest);
          });

        });
        
      }else{
        return res.status(200).json(result);
      }
    })
    .catch(next);
  }

  remove(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, location.permissions.delete)){
			return res.status(403).send("Invlid user request");
    }
    

    logger.info('Remove pricerequest');
    return pricingrequest.destroy({
      where: {
        priceRequestId: req.params.priceRequestId
      }
    })
    .then(pricingrequest => { return res.status(204).send('Deleted') })
    .catch(next);
  }

};

export default new PricingRequestController();