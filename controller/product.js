import logger         from '../util/logger';
import {config}       from '../config';
import {db}           from '../models';
import BaseController from './baseController'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const product = db.models.product; 
const clientproductsku = db.models.clientproductsku; 
const pricing = db.models.pricing; 
const category = db.models.productcategory; 

let includes = [
  { model: category, required: true, as:'productcategory'}
]; 

class ProductController {

  constructor(){ }

  findAll (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, product.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAll product');
    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    //remove limit and offset to avoid search issues. 
    delete req.query["limit"];
    delete req.query["offset"]; 

    return product.findAndCountAll({  offset: offset ? offset : 0
     ,limit: limit ? limit : 30,
      where: req.query, include:includes })
    .then((result) => { 
        if(!result) {return res.status(204).send("There is no data to return");}
        return res.status(200).json(result);
    })
    .catch(err => {
      console.log(err);
      logger.error('Product findAll invalid query');
      next('INVALID_QUERY');
    });

  }


  findOne(req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, product.permissions.select)){
			return res.status(403).send("Invlid user request");
    }
    
    logger.info('findOne product');
    product.findOne({ 
      
      where: {
        $or : [
          {productId: req.params.productId},
          {upc : {$like: '%' + req.params.productId  + '%'}},
          db.sequelize.where(db.sequelize.col("sku.sku"), 'LIKE', '%' + req.params.productId  + '%')
        ]
      }, 
      include: [
        { model: clientproductsku,
          required: true,
          as:'sku',
          on: {
              upc: db.sequelize.where(db.sequelize.col("product.productId"), "=", db.sequelize.col("sku.productId")),
              productId: db.sequelize.where(db.sequelize.col("product.upc"), "=", db.sequelize.col("sku.upc")), 
              clientId: req.params.clientId
            },
        },
        { model: pricing, required: true, as:'pricing', where: {clientId: req.params.clientId}}, 
        { model: category, required: true, as:'productcategory'}]
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(err => {
      logger.error(' findOne invalid query');
      next('Invalid Query');
    });
  }

  search(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, product.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Search product');
    product.findAll({
      include:[
        { model: pricing, required: true, as:'pricing', where: {clientId: req.params.clientId}}, 
        { model: category, required: true, as:'productcategory'},
        { model: clientproductsku,
          required: true,
          as:'sku',
          where: { clientId: req.params.clientId }
        }], 
         where:{
           $or:[db.sequelize.where(db.sequelize.col("product.upc"), 'LIKE','%' + req.params.term + '%'),
           db.sequelize.where(db.sequelize.col("sku.sku"), 'LIKE', '%' + req.params.term + '%')]}
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  create(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, product.permissions.create)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Create product')
    product.create(req.body)
    .then(result => res.status(201).json(result))
    .catch(next);

  }

  update(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, product.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Update product') 
    if (!req.body) {
      logger.error('missing request body update productController');
      return res.status(400).send('Update parameters required');  
    }
    const conditions = req.params ;
    product.update(req.body, { where: conditions })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(200).json(doc);
    })
    .catch(next);

  }

  destroy(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, product.permissions.delete)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Destroy product')
    product.destroy({ where: req.params })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(204).end();
    })
    .catch(next);
    
  }

}

export default new ProductController();