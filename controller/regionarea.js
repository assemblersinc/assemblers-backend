import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const region = db.models.region;
const user = db.models.user;
const geoarea = db.models.geoarea; 

let includes = [
     {model: user, required:false, as: 'manager', otherKey:'managerId'},
     {model: user, required:false, as: 'secondManager', otherKey:'managerSecondId'}, 
     {model: geoarea, required:false, as: 'geoareas', otherKey:'regionId'},     
]; 

class RegionAreaController {

  constructor(){}

  findAll (req, res,next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, region.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAll regionarea');
    return region.findAll({ where: req.query, include: includes })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findAllRegions (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, region.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllRegions');
    return region.findAll({ include: includes })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOne (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, region.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findOne regionarea');
    region.findOne({ where: req.params || req.query, include:includes  })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  create(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, region.permissions.create)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('create regionarea');
    region.create(req.body)
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  update(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, region.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Update regionarea');
    if (!req.body) {
      logger.error('Region area update error');
      return res.status(400).send('Update parameters required');  
    }
    const conditions = req.params ;
    region.update(req.body, { where: conditions })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(200).json(doc);
    })
    .catch(next);
  }

  destroy(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, region.permissions.delete)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Destroy regionarea');
    region.destroy({ where: req.params })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(200).send('Item deleted');
    })
    .catch(next);
  }

};

export default new RegionAreaController();