import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const repair = db.models.repair; 
const repaircategory = db.models.repaircategory; 

let includes = [
    {model: repaircategory, required: true, as:'repairCategory'}]; 

class RepairController {

  constructor () { }

  findAll (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, repair.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAll repair');
    return repair.findAll({ where: req.query, include:includes })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOne (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, repair.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findOne repair');
    return repair.findOne({ where: req.params || req.query, include:includes })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  create(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, repair.permissions.create)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Create repair');
    return repair.create(req.body)
    .then(doc => res.status(201).json(doc))
    .catch(next);
  }

  search(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, repair.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Search repair');
    return repair.findAll({
      where: { name : {$like: req.params.term + '%' }}
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  update(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, repair.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('Update repair');
    if (!req.body) {
      logger.error('Repairs update error');
      return res.status(400).send('Update parameters required');  
    }
    const conditions = req.params ;
    
    return repair.update(req.body, { where:conditions })
      .then(doc => {
        if (!doc) { return res.status(404).end(); }
        return res.status(200).json(doc);
      })
      .catch(next);
  }

  remove(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, repair.permissions.delete)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('remove repairController')
    return repair.destroy({ where: req.params })
      .then(doc => {
        if (!doc) { return res.status(400).end(); }
        return res.status(200).send('Item deleted');
      })
      .catch(next);

  }

  /**
   * Handles Repair categories. 
   */

  findAllCategories (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, repaircategory.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllCategories');
    return repaircategory.findAll({ where: req.query })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOneCategory (req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, repaircategory.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findOneCategory');
    return repaircategory.findOne({
      where: { repairCategoryId: req.params.repairCategoryId }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  searchCategory(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, repaircategory.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('searchCategory');
    return repaircategory.findAll({
      where: { name : {$like: req.params.term + '%' }}
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  createCategory (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, repaircategory.permissions.create)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('createCategory');
    let body = req.body;
    return repaircategory.findOrCreate({
      where: body
    })
    .then(result => { 
      if(!result  && !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  updateCategory (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, repaircategory.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('updateCategory');
    let body = req.body;
    return repaircategory.update(
      body,
      { where: { repairCategoryId: req.params.repairCategoryId }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);

  }

  removeCategory (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, repaircategory.permissions.delete)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('removeCategory');
    return repaircategory.destroy({
      where: { repairCategoryId: req.params.repairCategoryId }
    })
    .then (result => { return res.status(200).send('Item deleted'); })
    .catch(next);

  }

};

export default new RepairController();