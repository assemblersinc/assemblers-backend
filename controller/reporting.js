import moment         from 'moment';
import logger         from '../util/logger';
import {ROLE_TYPE, ADMIN, GENERAL}    from '../util/roleType';
import {db}           from '../models';
import BaseController from './baseController';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

class ReportingController {

  constructor() {
  }

  dashboard(req, res, next) {


    if(!checkAuthLevel(req.currentUser.roleId, ADMIN)){
			return res.status(403).send("Invlid user request");
    }

    // Validate the date range
    if (!req.query.startDate) {
      return next('INVALID_REQUEST');
    }

    // Check for monday to sunday
    console.log('start date ', req.query.startDate); 
    let startDate = moment(req.query.startDate);
    if (startDate.format('d') !== '1') {
      logger.warning('Invalid start date.', startDate.format('d'));
      return next('INVALID_REQUEST');
    }
    console.log('start date format', req.query.startDate); 

    let totals = {
      serviceDays: 0, // Basically the count of requests?
      servicedOnDay: 0,
      servicedOnOther: 0,
      revenue: 0,
      requestedItems: 0,
      assembledItems: 0,
      techsClockedIn: 0
    };
    let clientTotals = {};


    let endDate = new moment(startDate);
    endDate = endDate.add(6, 'days');

    let where = {
      scheduleDate: {$between: [startDate.format(), endDate.format()]}
    };

    let clientWhere = {};
    if (req.query.clientId) {
      clientWhere.clientId = req.query.clientId;
    }

    let filterArea = {}; 
    if (req.query.areaId) {
      filterArea.areaId = req.query.areaId;
    }

    function updateData(totalsObject, requestData) {
      ++totalsObject.serviceDays;

      requestData.requestproduct.forEach((product) => {
        if (product.completed) {
          if (moment(product.completedTime).format('L') === moment(requestData.scheduleDate).format('L')) {
            ++totalsObject.servicedOnDay;
          } else {
            ++totalsObject.servicedOnOther;
          }
        }

        totalsObject.requestedItems += product.qtyTotal;
        totalsObject.assembledItems += product.qtyAssembled;
        totalsObject.revenue += product.pricing ? parseFloat(product.pricing.price):0;
      });
    }

    db.models.request.findAll({
      where: where,
      attributes: ['scheduleDate'],
      include: [
        {
          model: db.models.location,
          as: 'location',
          attributes: ['clientId'],
          include: [
            {
              model: db.models.client,
              as: 'client',
              attributes: ['clientId', 'name'],
              where: clientWhere
            }
          ], 
          where: filterArea
        },
        {
          model: db.models.requestproduct,
          as: 'requestproduct',
          attributes: ['requestProductId', 'qtyTotal', 'qtyAssembled', 'completed', 'completedTime'],
          include: [{
            model: db.models.pricing,
            attributes: ['price']
          }]

        }
      ]
    })
    .then((requests) => {

      requests.forEach((request) => {
        if (clientTotals[request.location.clientId] === undefined) {
          clientTotals[request.location.clientId] = {
            name: request.location.client.name,
            serviceDays: 0, // Basically the count of requests?
            servicedOnDay: 0,
            servicedOnOther: 0,
            revenue: 0,
            requestedItems: 0,
            assembledItems: 0,
            techsClockedIn: 0
          };
        }

        let clientTotal = clientTotals[request.location.clientId];

        // Update totals
        updateData(totals, request);

        // Update client totals
        updateData(clientTotal, request);
      });

      // Convert to strings with 2 decimals.
      totals.revenue = totals.revenue.toFixed(2);
      for (let x in clientTotals) {
        clientTotals[x].revenue = clientTotals[x].revenue.toFixed(2);
        clientTotals[x].compliance = clientTotals[x].serviceDays ? (clientTotals[x].servicedOnDay / clientTotals[x].serviceDays * 100).toFixed(0) : 0;
      }

      totals.compliance = totals.serviceDays ? (totals.servicedOnDay / totals.serviceDays * 100).toFixed(0) : 0;


      // Techs clocked in
      return db.models.timesheet.findAll({
        where: {
          timeIn: {$between: [startDate.format(), endDate.format()]}
        },
        attributes: [[db.sequelize.fn('COUNT', db.sequelize.fn('DISTINCT', db.sequelize.col('timesheet.userId'))), 'clockins'],
          'location.clientId'
        ],
        group: ['location.clientId'],
        include: [
          {
            model: db.models.location,
            as: 'location',
            attributes: ['clientId'],
            include: [
              {
                model: db.models.client,
                as: 'client',
                attributes: [],
                where: clientWhere
              }
            ]
          }
        ]
      });
    })
    .then((timesheets) => {
      
      timesheets.forEach((clocks) => {

        clocks = clocks.toJSON();

        totals.techsClockedIn += clocks.clockins;

        if(clocks.location && clientTotals[clocks.location.clientId]){
          clientTotals[clocks.location.clientId].techsClockedIn += clocks.clockins;
        }
      });

      // Values for client totals.
      clientTotals = Object.values(clientTotals);

      return res.json({totals: totals, clientTotals: clientTotals});
    })
    .catch(next);

}


}

export default new ReportingController();