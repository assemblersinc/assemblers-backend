import {config}         from '../config';
import {db}             from '../models';
import _                from 'lodash'; 
import moment           from 'moment'; 
import logger           from '../util/logger';
import {REQUEST_TYPE}   from '../util/requestType';
import BaseController   from './baseController';
import * as notificationHub from '../util/notificationHub';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const client            = db.models.client;
const clientproductsku  = db.models.clientproductsku;
const location          = db.models.location;
const pricing           = db.models.pricing;
const product           = db.models.product;
const service           = db.models.service
const requestSeenByLog  = db.models.requestseenbylog;
const requestType       = db.models.requesttype;
const request           = db.models.request;
const requestProduct    = db.models.requestproduct;
const storeWalk         = db.models.storewalk;
const storeservice      = db.models.storeservice;
const requestcreatedby  = db.models.requestcreatedby;
const user              = db.models.user; 
const invoice           = db.models.invoice; 
const invoiceProduct    = db.models.invoiceproduct;
const timesheet         = db.models.timesheet; 

let requestIncludes = [
  { model: requestcreatedby, required: false, as: 'requestcreatedby' },
  { model: storeservice, required: false, as: 'requestservice' , include: [{model:service,require:true, as:'service' }, { model: user, required: false, as: 'user' }] },
  { model: requestProduct, required: false, as: 'requestproduct',include: {model:product,require:true, as:'product' },  
    include: [
      { model: user, required: false, as: 'user' },
      { model: product, required: false, as: 'product',
      include:[{ model: clientproductsku, required: true, as: 'sku' }, 
               { model: pricing, required: true, as:'pricing' }]
    }]
  },
  { model: requestType, required: true, as:'requesttype' },
  { model: location, required: true, as:'location', include:[{ model: client, required: true, as: 'client' }]}
];

let invoiceIncludes = [
  { model: request, required: true, as:'request',
    include: [{ model: location, required: true, as: 'location',
      include:[{ model: client, required: true , as: 'client'}]
    },
    { model: requestType, required: true,as:'requesttype' }]
  }
  
];

class RequestController extends BaseController {
  
  constructor(){ super(request) }

  // ========================
  //        storeWalk
  // Storewalk Table - keeps record of the storewalk start and end times ( not currently in use). 
  // request Table - holds the storewalk table data. 
  // ========================

  findAllStoreWalk (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllStoreWalk');
    let params = req.query;
  
    if(!parseInt(params.locationId)){
      return res.status(400).send("Invalid location");
    }
    // change this from store walk where isStoreCompleted.
    params.storeWalkCompleted = 0;

    console.log('store walk params',params);

    return request.findAll({
      where: params,
      include: requestIncludes
    })
    .then(result => { 

      if(!result || !result.length > 0) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
    })
    .catch(next);

  }

  /**
   * Finds one record from the storewalk table records  
   */
  findOneStoreWalk (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findOneStoreWalk');
    return storeWalk.findOne({
      where: req.params
    })
    .then(result => { 
      if(!result) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
    })
    .catch(next);

  }

  /**
   * Updates the request as store walk. 
   */
  updateStoreWalk (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('updateStoreWalk');

    console.log('update store walk', req.body)
    let body = req.body;
      // updates the request based on service repairs or products. 
    return request.update(
      body,
      { where: { requestId: req.params.storeWalkId } // updates the request f
    })
    .then(result => { 
      if(!result) {return res.status(204).send("There is no data to return")}

      return res.status(200).json(result);
    })
    .catch(next);

  }

  /**
   * Used to create a storewalk entry, to create an actual storeWalk the request needs to be created with requestType 2
   * startStoreWalk handles the creating of store walk 
   * to deprecate. 
   */
  createStoreWalk (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeWalk.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('createStoreWalk');
    let body = req.body;

    return storeWalk.findOrCreate({
      where: {requestId: result.requestId},
      defaults:{ requestId: body.requestId, userId: body.userId, 
        started: body.started, notes: body.notes
      }
    })
    .then(res => { 
        return res.status(200).json(result);
    })
    .catch(next);
   
  }

  completeStoreWalk(req, res, next){
     
    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

     return db.sequelize.transaction(t => {
        return request.update(
          { 
            storeWalkCompleted: true, 
            endTime: req.body.endTime, 
            completed: true
           },
          { where: { requestId: req.body.requestId}
        },
        {transaction: t})
        .then((result) => { 
          return storeWalk.update({ended:req.body.endTime }, {where: { requestId: req.body.requestId}});
        },{ transaction: t});
     })
      .then(result=>{
        if(!result) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
      })  
      .catch(next); 
  }

  startStoreWalk (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.create)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('createStoreWalk');
    return db.sequelize.transaction(t => {
      return request.findOrCreate({
        where: {
          locationId: req.body.locationId,
          requestTypeId: REQUEST_TYPE.STORE_WALK,
          StoreWalkCompleted: false,  
          completed: false
        },
        defaults: {
          requestedById: req.body.userId,
          comments: req.body.comments,
          photos: req.body.photos,
          requestTypeId: REQUEST_TYPE.STORE_WALK,
          createTime: req.body.createTime
        },
        transaction: t 
      })
      .spread(result => {
        let values = result.dataValues;
        return storeWalk.findOrCreate({
          where: { requestId: values.requestId },
          defaults: {
            requestId:values.requestId ,
            userId: values.requestedById,
            started: values.createTime,
            notes: values.comments,
            requestId: values.requestId
          },
          transaction: t 
        })
        .spread(result => {
          return values; 
        });
      })
    })
    .then(result => { 
      if(!result) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result)
    })
    .catch(next);

  }

  removeStoreWalk (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.delete)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('destroyStoreWalk');
    return storeWalk.destroy({
      where: { storeWalkId: req.params.storeWalkId }
    })
    .then (result => res.status(202).json(result))
    .catch(next);
  }

  // ========================
  //     (Store) request
  // ========================

  createStoreRequest (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.create)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('createStoreRequest');
    let response = {};
    if (!req.body.request || !req.body.requestCreatedBy) {
      return res.status(400).send('Missing request body inputs')
    }
    return db.sequelize.transaction(t => {
      // Step 1: Add entry to request table
      return request.create({
        locationId: req.body.request.locationId,
        requestedById: req.body.request.requestedById,
        comments: req.body.request.comments,
        requestTypeId: req.body.request.requestTypeId,
        createTime: req.body.request.createTime
      },
      {
        transaction: t
      })
      .then(requestResult => {
        // Step 2: Add entry to requestCreatedBy table
        response = requestResult;
        return requestcreatedby.create({
          requestId: requestResult.requestId,
          userId: req.body.request.requestedById,
          firstName: req.body.requestCreatedBy.firstName,
          lastName: req.body.requestCreatedBy.lastName,
          phoneNumber: req.body.requestCreatedBy.phoneNumber,
          email: req.body.requestCreatedBy.email,
          position: req.body.requestCreatedBy.position
        }, 
        {
          transaction: t
        })
      })
      .then(result => {
        return user.findOne({
          include: [
            {
              model: timesheet, required: true, as: 'timesheet',otherKey:'timeId'
              , 
              include: [
                  {model: location, required: true, as: 'location', otherKey: 'locationId'}
              ]
              , 
              where: {
             
                locationId: req.body.request.locationId, 
                timeIn: {$ne: null},
                timeOut:null 
                
              }
            }
          ]
        }); 

      })
      .then(result => {

        if(result && response.requestTypeId == REQUEST_TYPE.BOPIS){

          console.log('this is valid');
          let tags = "user_tag_" + result.userId;
          
          this.sendNotification(tags, "new BOPIS request! View worklist items in the app to assemble it and add it to your invoice", {type:'bopis'})
          .then((data) =>
          {
            console.log('send notification'); 
            return res.status(200).json(response);
          }).catch((err) => 
          {
            return res.status(400).send("Problem sending notification, request has been created! " + err);
          });

        }else{
          return res.status(200).json(response);
        }

        
      })
    })
    .catch(next);
  }

  findAllRequest (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllRequest')
    let query = req.query;
    query.requestTypeId = query.requestTypeId  == 1 ? {'$eq' : REQUEST_TYPE.STORE_ASSEMBLY } : query.requestTypeId;   // 1
    query.completed = {'$ne': true }

    logger.info('findAllRequest');
    return request.findAll({
      where: query,
      include: requestIncludes
    })
    .then(result => { 
      if(!result || !result.length > 0) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
    })
    .catch(next);

  }

  findOneRequest (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findOneRequest')
    let userId = req.currentUser.userId;
    if (!userId) {
      logger.error('findOneRequest no userId')
      return res.status(400).send('User ID required');
    }
    logger.info('findOneRequest');
    return db.sequelize.transaction( t => {
      // Transaction Step 1: find request entry
      return request.findOne({
        where: { requestId: req.params.requestId },
        include: requestIncludes
      },
      {
        transaction: t
      })
      .then(findOneResult => {
        // Transaction Step 2: Update requestSeenByLog
        return requestSeenByLog.upsert({
          userId: userId,
          requestId: req.params.requestId
        },
        {
          transaction: t
        })
        .then(result =>{
          return findOneResult; 
        })
        .catch(next); 
      })
    })
    .then(result => { 
      if(!result) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
    })
    .catch(next);

  }

  updateRequest (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.update)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('updateRequest');
    let body = req.body;
    return db.sequelize.transaction( t => {
      // Transaction Step 1: Update request entry
      return request.update(
        body,
        { where: { requestId: req.params.requestId }},
        { transaction: t }
      )
      .then( update => {
        // Transaction Step 2: Update requestSeenByLog
        return requestSeenByLog.upsert({
          userId: req.params.userId,
          requestId: req.params.requestId
        },
        {
          transaction: t
        })
      })
    })
    .then((result) => { 
        return res.status(200).json(result);
    })
    .catch(next);

  }

  removeRequest (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.delete)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('removeRequest');
    return request.destroy({
      where: { requestId: req.params.requestId }
    })
    .then (result => res.status(202).json(result))
    .catch(next);
    
  }

  findAllBopis (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllBopis');
    let query = req.query;
    query.requestTypeId = REQUEST_TYPE.BOPIS; // '5'
    return request.findAll({
      where: query
    })
    .then(result => { 
      if(!result || !result.length > 0) {return res.status(204).send("There is no data to return");}
        return res.status(200).json(result);
    })
    .catch(next);
  }

  // ========================
  //     requestProduct
  // ========================

  findAllRequestProduct (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.product)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllRequestProduct');
    let query = req.query;
    return requestProduct.findAll({
      where: query,
    })
    .then(result => { 
      if(!result) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
    })
    .catch(next);

  }

  /*
   *  This function calls store procedure that joins products and services together for work list to display as one array. 
   */
  findAllRequestProductsByLocation(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.select)){
			return res.status(403).send("Invlid user request");
    }

    logger.info('findAllRequestProduct');
    let params = req.params;
    let query = req.query; 
    
    if(!query.userId || !params.locationId ){
       if(!result) {return res.status(204).send("Invalid params")}
    }
    return db.sequelize.query("CALL sp_getrequestproductandservicesperlocation(:locationId, :userId)", {
        replacements: {
          locationId: params.locationId, 
          userId:query.userId
        }
      })
      .then(result => {
        if(!result) {return res.status(204).send("There is no data to return")}
        // updates request last seen 

        

        return res.status(200).json(result);
      })
     .catch(next);
  }

  findAllRequestProductsByLocationWithPictures(req, res, next) {

        if(!checkAuthLevel(req.currentUser.roleId, request.permissions.select)){
          return res.status(403).send("Invlid user request");
        }
    
        logger.info('findAllRequestProductWithPictures');

        let params = req.params;
        let query = req.query; 
        let finalResult = {photos:[],items:[]}; 

        // For pictures of the current week storewalk. 
        const startWeekDate = moment(Date.now()).startOf('isoweek').isoWeekday(1).format('YY-MM-DD');
        const endWeekDate = moment(Date.now()).endOf('week').add(1, 'd').format('YY-MM-DD');

        
        if(!query.userId || !params.locationId ){
           if(!result) {return res.status(204).send("Invalid params")}
        }

        return db.sequelize.query("CALL sp_getrequestproductandservicesperlocation(:locationId, :userId)", {
            replacements: {
              locationId: params.locationId, 
              userId:query.userId
            }
            ,raw:true
          })
          .then(result => {
        
            // APPENDS PICTURES. 
            finalResult.items = result;

            return request.findAll({
              where: { 
                requestTypeId: REQUEST_TYPE.STORE_WALK,
                locationId: params.locationId,
                photos: { $ne:null }, 
                endTime: {
                  $between: [ startWeekDate,endWeekDate ]
                }
              }, 
              attributes: ['photos'],
              raw:true
            });
           
            
          })
          .then(result =>{
             
            var allPhotos = []
             _.forEach(result, function(value){
              allPhotos.push(value.photos); 
            });

            finalResult.photos = _.flatMap(allPhotos); 
                    
            if(! finalResult.photos.length > 0 && !finalResult.items.length > 0) {return res.status(204).send("There is no data to return")}
            
            return res.status(200).json(finalResult);

          })
         .catch(next);
      }

  findOneRequestProduct (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, requestproduct.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOneRequestProduct');
    return requestProduct.findOne({
      where: req.params
    })
    .then(result => { 
      if(!result || !result.length > 0) {return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
    })
    .catch(next);
  }
 
  /* 
   *  Used by store request 
   *  Creates product or service. based on flag passed on .
   */
  createRequestProduct (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, requestproduct.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    if(!req.body){
      return res.status(400).send("Invalid request");
    }
    let body = JSON.parse(JSON.stringify(req.body));

    // Check if the type of add is product or service 
    // Service Entry
    if(body && body.isService){

        // adds count as of 1 to init. 
      if(body.product && !body.product.qtyTotal){
        body.product.qtyTotal = 1;
         Object.assign(body.product,{'qtyTotal': 1}) 
      }
      // appends the request and userId from the parent request. 
      if(body.product && !body.product.userId )
         Object.assign(body.product,{'userId': body.userId }) ;
      if(body.product && !body.product.requestId)
        Object.assign(body.product,{'requestId': body.requestId });

      // hack to include priceId into the pricing productrequest table. 
      if(body.product && (!body.product.priceId || !body.product.pricing )  ){
        Object.assign(body.product,{'priceId':((body.product.pricing && body.product.pricing.priceId )) ? body.product.pricing.priceId : null})
      }
      

      return storeservice.findOrCreate({
        where:{
          serviceId: body.product.serviceId, 
          requestId: body.product.requestId
        },
        defaults: body.product
      }, 
      )
      .spread((result, created) => { 
        
        if(!result) {return res.status(204).send("There is no data to return")}

        if(!created){
          console.log('this worked has created', created);

          let finalResult = result.get({
            plain: true
          });
          return storeservice.update({
            qtyTotal: finalResult.qtyTotal + body.product.qtyTotal
          }, 
          {
            where:{ storeServiceId: finalResult.storeServiceId }
          })
          .then(update =>{
            finalResult.qtyTotal = finalResult.qtyTotal + body.product.qtyTotal;
            return res.status(200).json(finalResult);
          })
          .catch(next);
        }
        else {
          return res.status(200).json(result);
        }
      })
      .catch(next);

    }
    else if(body && body.isRepairs){
        // incase we need to add repairs to request. 
        // as of 07/31/2017 requirements, repairs get push directly into invoices. 

    } 
    else {  // enters regular product. 
    
      // adds count as of 1 to init. 
      if(body.product && !body.product.qtyTotal){
        Object.assign(body.product,{'qtyTotal': 1}) 
      }
      
      // appends the request and userId from the parent request. 
      if(body.product && !body.product.userId )
        Object.assign(body.product,{'userId': body.userId }) 
      if(body.product && !body.product.requestId)
        Object.assign(body.product,{'requestId': body.requestId })

      // hack to include priceId into the pricing productrequest table. 
      if(body.product && !body.product.priceId  && body.product.pricing ) {
        Object.assign(body.product,{'priceId':((body.product.pricing && body.product.pricing.priceId )) ? body.product.pricing.priceId : null})
      }

      return requestProduct.findOrCreate({
        where:{
          productId: body.product.productId, 
          requestId: body.product.requestId
        },
        defaults: body.product
      })
      .spread((result, created) => { 
        

        if(!result) {return res.status(204).send("There is no data to return")}

        if(!created){
          console.log('this worked has created', created);

          let finalResult = result.get({
            plain: true
          });
          return requestProduct.update({
            qtyTotal: finalResult.qtyTotal + body.product.qtyTotal
          }, 
          {
            where:{requestProductId: finalResult.requestProductId}
          })
          .then(update =>{
            finalResult.qtyTotal = finalResult.qtyTotal + body.product.qtyTotal;
            return res.status(200).json(finalResult);
          })
          .catch(next);
        }
        else {
          return res.status(200).json(result);
        }
      })
      .catch(next);

    }
  }

  updateRequestProduct (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, requestproduct.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('updateRequestProduct');
    let body = req.body;
    if(!body){
      return res.status(400).send("Error invalid params");
    }

    // Automatically remove is qty = 0 
    if(body && body.qtyTotal == 0) {
      this.deleteRequestProduct(req, res, next);
    } else {
      return requestProduct.update(
        body,
      { where: {
          requestProductId: req.params.requestProductId
        }
      })
      .then(update => { 
        if(update > 0){
          // Return updated object. 
          return requestProduct.findOne({
            where: {
              requestProductId: req.params.requestProductId
            }
          })
          .then(result=> {
            if(result.qtyAssembled == result.qtyTotal) {
              // Mark the requestproduct as complete. 
              requestProduct.update({
                completed: 1
              }, {
                where: {
                  requestProductId: req.params.requestProductId
                }
              }); 
              // Update result
              result.completed = 1; 
              return request.findOne({
                where: {
                  requestId: result.requestId, completed: 0
                }, 
                include :[{ model: storeservice, required: false, as: 'requestservice', where: {requestId: result.requestId ,completed: 0 }},
                          { model: requestProduct, required: false, as: 'requestproduct', where: {requestId: result.requestId ,completed: 0 }}]
              })
              .then(set => {
                if (set && !set.storeservice && !set.requestproduct ) {
                  request.update({
                    completed: 1
                  }, {
                    where: { 
                      requestId: result.requestId,completed: 0
                    }
                  }); 
                }
                return res.status(200).json(result);
              })
              .catch(next);         
            } else {
              if(!result) { return res.status(204).send("There is no data to return")} 
                return res.status(200).json(result);
            }   
          })
          .catch(next)
        } else {
          return requestProduct.findOne({
            where: { 
              requestProductId: req.params.requestProductId
            }
          })
          .then(result => {
            return res.status(200).json(result);
          })
          .catch(next); 
        }
      })
      .catch(next);
    }
  }

  updateRequestService(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeservice.permissions.update)){
      return res.status(403).send("Invlid user request");
    }


    logger.info('updateRequestProduct');
    let body = req.body;
    if(!body){
      return res.status(400).send("Error invalid params");
    }

    // Automatically remove is qty = 0 
    if(body && body.qtyTotal == 0) {
      this.deleteRequestService(req, res, next);
    } else {
      return storeservice.update(
        body,
      { where: {
          storeServiceId: req.params.storeServiceId 
        }
      })
      .then(update => { 
        if(update > 0){
          // Return updated object. 
          return storeservice.findOne({
            where: {
              storeServiceId: req.params.storeServiceId
            }
          })
          .then(result => {
            if(result.qtyServiced == result.qtyTotal) {
              // Mark the storeservice as complete. 
              storeservice.update({
                completed: 1
              }, {
                where: {
                  storeServiceId: req.params.storeServiceId
                }
              }); 
              // Update result
              result.completed = 1; 
              return request.findOne({
                where: {
                  requestId: result.requestId, completed: 0
                }, 
                include :[{ model: storeservice, required: false, as: 'requestservice', where: {requestId: result.requestId ,completed: 0 }},
                          { model: requestProduct, required: false, as: 'requestproduct', where: {requestId: result.requestId ,completed: 0 }}]
              })
              .then((set) => {
              
                if (set && !set.storeservice && !set.requestproduct ) {
                  request.update({
                    completed: 1
                  }, {
                    where: { 
                      requestId: result.requestId,completed: 0
                    }
                  }); 
                }
                return res.status(200).json(result);
              })
              .catch(next);         
            } else {
              if(!result) { return res.status(204).send("There is no data to return")} 
                return res.status(200).json(result);
            }   
          })
          .catch(next)
        } else {
          return storeservice.findOne({
            where: { 
              storeServiceId: req.params.storeServiceId
            }
          })
          .then(result => {
            return res.status(200).json(result);
          })
          .catch(next); 
        }
      })
      .catch(next);
    }
  }

  deleteRequestProduct (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, requestproduct.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('deleteREquestProduct');
    let body = req.body;
    return requestProduct.destroy({
      where: { requestProductId: req.params.requestProductId }
    })
    .then(result => { 
      res.status(200).json(result);
    })
    .catch(next);
  }

  deleteRequestService(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeservice.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('deleteREquestProduct');
    let body = req.body;
    return storeservice.destroy({
      where: { storeServiceID: req.params.storeServiceId }
    })
    .then(result => { 
      res.status(200).json(result);
    })
    .catch(next);
  }

  onDemandRequestProductForInvoice (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('on Demand completeRequestProductForInvoice');

    if(!req.body){
      return res.status(400).send("Invalid request");
    }

    let body = JSON.parse(JSON.stringify(req.body));

    // Check if the type of add is product or service 
    // Service Entry
    if(body && body.isService){

        // adds count as of 1 to init. 
      if(body.product && !body.product.qtyTotal){
        body.product.qtyTotal = 1;
        Object.assign(body.product,{'qtyTotal': 1}) 
      }
      // appends the request and userId from the parent request. 
      if(body.product && !body.product.userId )
         Object.assign(body.product,{'userId': body.userId }) ;

      // hack to include priceId into the pricing productrequest table. 
      if(body.product && (!body.product.priceId || !body.product.pricing )  ){
        Object.assign(body.product,{'priceId':((body.product.pricing && body.product.pricing.priceId )) ? body.product.pricing.priceId : null})
      }

      var end = moment().endOf('day');
      var start = moment().subtract(1, 'days').endOf('day');

      return db.sequelize.transaction(t => {
        return request.findOrCreate({
          where: {
            locationId: req.body.locationId,
            requestTypeId: REQUEST_TYPE.ON_DEMAND_REQUEST,
            completed: false
          },
          include :[
            { model: storeservice, required: true, as: 'requestservice',
                    include: [{ 
                      model: invoiceProduct ,require:true , as:'invoiceservicerequest', 
                        include: [{
                          model: invoice, require: true, as: 'invoiceproduct', 
                          where: { invoiceCompletedTime: null }, 
                          attributes: []
                        }], 
                        attributes: [], 
                        where: {entryType: 'S'} // service type
                    }],
                    where: [ { userId: req.body.userId}], 
                    attributes: []
           },
          ],
          subQuery:false, 
          attributes:['requestId'], 
          order:[['createTime', 'DESC']],
          defaults: {
            requestedById: req.body.userId,
            comments: 'on demand request by tech or AM',
            photos: [],
            createTime: moment().toDate(), 
            requestTypeId: REQUEST_TYPE.ON_DEMAND_REQUEST
          }, 
          transaction: t
        })
        .spread(result =>{

          // sets qtyAssembled to 0, when complete for invoice it will update this qty to plus 1. 
          // service is allowed to enter qty greater than 1 
          // HACK: - Quick Hack to invoice all items set the qtyServiced = qtyTotal - 1 as the qtyServiced will be invoiced in next step +1
          // When item is invoiced is invoiced it move the total qtyServiced + 1 into the invoice. 
         
          body.product.qtyServiced = body.product.qtyTotal - 1;  

          return storeservice.findOrCreate({
            where:{
              serviceId: body.product.serviceId, 
              requestId: result.requestId, 
              userId: body.userId
            },
            defaults: body.product, 
            transaction:t
          })
          .spread((result, created) => { 
                        
            if(!result) {return res.status(204).send("There is no data to return")}

            if(!created){
    
              let finalResult = result.get({
                plain: true
              });

              return storeservice.update({
                qtyTotal: finalResult.qtyTotal + body.product.qtyTotal, 
                completed: false, 
                completedTime: null
              }, 
              {
                where:{storeServiceId: finalResult.storeServiceId},
                transaction:t
              })
              .then(update =>{
                return result; 
              });
              
            }
            else
            {
              return result; 
            }
          });
        })
      })
      .then(result => {

        return db.sequelize.query("CALL sp_moveitemtoinvoice(:itemId, :clientId, :userId, :serialNumber, :isService)", {
          replacements: {
            itemId: result.storeServiceId,
            userId:body.userId, 
            clientId:body.clientId,
            serialNumber:body.serialNumber, 
            isService: true
          }
        })
        .then(result => {
          
          if(!result || result.length == 0) {return res.status(204).send("There is no data to return")}
          // Update request last seen 
    
          if(result[0] && result[0].Level && result[0].Level == 'Error'){
            return res.status(400).send("error saving item");
          }

          return res.status(200).send("item saved");
        });
   
      })
      .catch(next);

    }
    else 
    {  // enters regular product. 
    
      // adds count as of 1 to init. 
      if(body.product && !body.product.qtyTotal){
        Object.assign(body.product,{'qtyTotal': 1}) 
      }
      
      // appends the request and userId from the parent request. 
      if(body.product && !body.product.userId )
        Object.assign(body.product,{'userId': body.userId }) 

      // hack to include priceId into the pricing productrequest table. 
      if(body.product && !body.product.priceId  && body.product.pricing ) {
        Object.assign(body.product,{'priceId':((body.product.pricing && body.product.pricing.priceId )) ? body.product.pricing.priceId : null})
      }

      var end = moment().endOf('day');
      var start = moment().subtract(1, 'days').endOf('day');

      return db.sequelize.transaction(t => {
        return request.findOrCreate({
          where: {
            locationId: req.body.locationId,
            requestTypeId: REQUEST_TYPE.ON_DEMAND_REQUEST,
            completed: false
          },
          include :[
            { model: requestProduct, required: true, as: 'requestproduct',
                    include: [{ 
                      model: invoiceProduct ,require:true , as:'invoiceproductrequest', 
                        include: [{
                          model: invoice, require: true, as: 'invoiceproduct', 
                          where: { invoiceCompletedTime: null }, 
                          attributes: []
                        }], 
                        where: [ {entryType: 'P'}], 
                        attributes: []
                    }],
                    where: [ { userId: req.body.userId}], 
                    attributes: []
           },
          ],
          subQuery:false, 
          attributes:['requestId'], 
          order:[['createTime', 'DESC']],
          defaults: {
            requestedById: req.body.userId,
            comments: 'on demand request by tech or AM',
            createTime:  moment().toDate(),
            photos: [],
            requestTypeId: REQUEST_TYPE.ON_DEMAND_REQUEST
          }, 
          transaction: t
        })
        .spread(result =>{

            // sets qtyAssembled to 0 for defaults, when complete for invoice it will update this qty. 
            body.product.qtyAssembled = 0;  

            return requestProduct.findOrCreate({
              where:{
                productId: body.product.productId, 
                requestId: result.requestId, 
                userId: body.userId
              },
              defaults: body.product, 
              transaction:t
            })
            .spread((result, created) => { 
              
              if(!result) {return res.status(204).send("There is no data to return")}; 
              
              if(!created){
                
                let finalResult = result.get({
                  plain: true
                });
  
                return requestProduct.update({
                  qtyTotal: finalResult.qtyTotal + body.product.qtyTotal, 
                  completed: false, 
                  completedTime: null
                }, 
                {
                  where:{requestProductId: finalResult.requestProductId},
                  transaction:t
                })
                .then(update =>{

                  return result;        
                                 
                })
                .catch(next);
              }
              else {
                return result;
              }        
  
            })
        })
    })
    .then(result => {

      return db.sequelize.query("CALL sp_moveitemtoinvoice(:itemId, :clientId, :userId, :serialNumber, :isService)", {
        replacements: {
          itemId: result.requestProductId,
          userId:body.userId, 
          clientId:body.clientId,
          serialNumber:body.serialNumber, 
          isService: false
        }
      })
      .then(result => {
        
        if(!result || result.length == 0 ) {return res.status(204).send("There is no data to return")}
        // Update request last seen 

        if(result[0] && result[0].Level && result[0].Level == 'Error'){
          return res.status(400).send("error saving item");
        }

        return res.status(200).send("item saved");
      });
      
    })
    .catch(next);

   }
 }

  completeRequestProductForInvoice (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('completeRequestProductForInvoice');
    let body = req.body;
    let param = req.params;

    if(!body.isService){

      return db.sequelize.query("CALL sp_moveitemtoinvoice(:itemId, :clientId, :userId, :serialNumber, :isService)", {
          replacements: {
            itemId: param.requestProductId, 
            userId:body.userId, 
            clientId:body.clientId,
            serialNumber:body.serialNumber, 
            isService:body.isService
          }
        })
        .then(result => {

          if(!result|| result.length == 0) {return res.status(204).send("There is no data to return")}
          // Update request last seen 

          if(result[0] && result[0].Level && result[0].Level == 'Error'){
            return res.status(400).send("error saving item");
          }
          console.log('item result', result); 
          return res.status(200).send("item saved");
        })
      .catch(next);
    }
    else if(body.isService){
      return db.sequelize.query("CALL sp_moveitemtoinvoice(:itemId, :clientId, :userId, :serialNumber, :isService)", {
          replacements: {
            itemId: param.requestProductId, 
            userId:body.userId, 
            clientId:body.clientId,
            serialNumber:body.serialNumber, 
            isService:body.isService
          }
        })
        .then((result) => {
          if(!result || result.length == 0) {return res.status(204).send("There is no data to return")}
          // Update request last seen 
     
          if(result[0] && result[0].Level && result[0].Level == 'Error'){
            return res.status(400).send("error saving item");
          }
          return  res.status(200).send("item saved");
        })
      .catch(next);
    }else{
      return res.status(400).send("invalid request");
    }
  }

  /**
   * Helper functions
   * Deprecated for now
   */
  moveProductToInvoice(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

      let body = req.body;
      let itemRequestId = req.params.requestProductId
      let pricingFilter =  {clientId: body.clientId};
      let item = null; 
       
      if(body && body.locationId){
        pricingFilter.locationId = body.locationId
      }
      // Step 1: Find request entry associated with the requestProduct
      // Step 2: Find (or create) the invoice related to this completed request
      // Step 3: Update invoiceProduct table
      // Step 4: Update requestProduct table as completed
      // Step 5: Check to close the request if all items have been build. 
      return db.sequelize.transaction(t => {
      // Step 1: Find request entry associated with the requestProduct
      return request.findOne({
          include: [
               { model: requestcreatedby, required: true, as: 'requestcreatedby' },
               { model: requestProduct, required: true, as: 'requestproduct', where: {requestProductId: itemRequestId}
                  , include: [{model:product,require:true, as:'product',  
                    include:[{ model: clientproductsku, required: false, as: 'sku', where: {clientId: body.clientId} }, 
                          { model: pricing, required: false, as:'pricing', where:pricingFilter }] // change to required. 
                }]
                },
                { model: location, required: true, as:'location'
                , include:[{ model: db.models.client, required: true, as: 'client' }]} //
              ],
          subQuery:false},
        { transaction: t })
      
       // Step 2.1: Find (or create) the invoice related to this completed request
      .then(requestResult => {
        
        if(!requestResult){
           t.rollback(); 
           return res.status(400).send("Request no found!");
        }

      return invoice.findOrCreate({
          where: {
            userId: body.userId,
            requestId: requestResult.requestId, 
            invoiceTypeId : requestResult.requestproduct.product.expenseItem, 
          },
          include: invoiceIncludes,
          transaction: t,
          // If we need to create an invoice, provide the rest of required columns
          defaults: {
              invoiceNumber: Math.floor(Math.random() * (10 *10 *10)), //FIX THIS WITH MACOLA API. 
              userId: requestResult.userId,
              requestId: requestResult.requestId,
              clientId: requestResult.location.client.clientId,
            }
          }).then((res) => {
            console.log('what the fuck is going on.'); 
          })
      })
      // Step 3: Update invoiceProduct table
      .then((result) => { 
        
        console.log('result', result.invoiceId); 

        if(!result){
           t.rollback(); 
           return res.status(400).send("could not create invoice!");
        }
  
       return  requestProduct.create({
          invoiceId: result.invoiceId,
          productId: result.item.productId,
          name: result.item.product.name,
          qty: 1, //result.item.qtyAssembled,
          total: 1, // always default to one product moved to invoice. 
          pricePerUnit: item.product.pricing.price, // from product->pricing. 
          ai_code: item.product.pricing.iacode, // from product->pricing. 
          isExpenseItem: result.product.isExpenseItem, // from product table.
          serialNumber: body.serialNumber, // invoice item. 
          entryType: 'P',// P for product.
          isUserGeoFenced: body.isUserGeoFenced ? body.isUserGeoFenced : null,
          userGeoLat: body.userGeoLat ? body.userGeoLat : 0, 
          userGeoLong: body.userGeoLong ? body.userGeoLong : 0
          
        },
        { transaction: t })
      })
      // Step 4: Update requestProduct table as completed
      .then(invoiceProductResult => {
        
        return requestProduct.update({
            completed: true,
            completedTime: sequelize.literal('CURRENT_TIMESTAMP')
          },
          { where: { requestProductId: body.requestProductId }},
          { transaction: t });
         }).then((res) => {
            return invoiceProductResult; 
      })
      // Step 5: check to close the request if all items have been build. 
      .then(result => {

        return request.findOne({
                where: {
                  requestId: result.requestId, completed: 0
                }, 
                include :[{ model: storeservice, required: false, as: 'requestservice', where: {requestId: result.requestId ,completed: 0 }},
                          { model: requestProduct, required: false, as: 'requestproduct', where: {requestId: result.requestId ,completed: 0 }}]
              },{transaction: t });

      })
      .then(result =>{
          if (set && !set.storeservice && !set.requestproduct ) {
            return request.update({
              completed: 1
            }, {
              where: { 
                requestId: result.requestId,completed: 0
              }
            }).then((result)=>{
              console.log('item added successfully and closed the request'); 
              t.rollback(); 
               return res.status(200).send("Item added to Invoice!");
            }); 
          }
          else{
            t.rollback(); 
            console.log('item added successfully'); 
            return res.status(200).send("Item added to Invoice!");
          }
        });
    })
    .catch(next);
  }

  moveServiceToInvoice(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, request.permissions.update)){
      return res.status(403).send("Invlid user request");
    }
      return db.sequelize.transaction(t => {
      // Step 1: Find request entry associated with the requestProduct
      return request.findOne(
        { where: { requestId: rpObject.requestId },
          include: requestIncludes},
        { transaction: t })
      // Step 2: Find (or create) the invoice related to this completed request
      .then(requestResult => {
        return invoice.findOrCreate({
          where: {
            userId: rpObject.userId,
            requestId: rpObject.requestId
          },
          include: invoiceIncludes,
          transaction: t,
          // If we need to create an invoice, provide the rest of required columns
          defaults: {
              invoiceNumber: rpObject.invoiceNumber,
              userId: rpObject.userId,
              requestId: rpObject.requestId,
              clientId: requestResult.client.clientId,
            }
          });
      })
      // Step 3: Update invoiceProduct table
      .then(invoiceResult => { 
        return invoiceProduct.create({
          invoiceId: invoiceResult.invoiceId,
          productId: rpObject.productId,
          name: productObject.name,
          qty: rpObject.qtyAssembled,
          total: 1, // always default to one product moved to invoice. 
          pricePerUnit: '', // from product->pricing. 
          ai_code: '', // from product->pricing. 
          isExpenseItem: '', // from product table.
          serialNumber: rpObject.serialNumber
        },
        { transaction: t })
      })
      // Step 4: Update requestProduct table
      .then(invoiceProductResult => {
        return requestProduct.update({
          completed: true,
          completedTime: sequelize.literal('CURRENT_TIMESTAMP')
        },
        { where: { requestProductId: rpObject.requestProductId }},
        { transaction: t })
      })
      // Step 5: Update requestSeenByLog table
      .then(requestProductResult => {
        return requestSeenByLog.create({
          userId: rpObject.userId,
          requestId: rpObject.requestId
        },
        { transaction: t })
      })
      // Step 6 create macola Invoice & Update invoiceId. 
    })
      .catch(next);
  }

  //Helper function for notification 
  sendNotification(tag,message, payload)
  { 
    return notificationHub.sendPush(null, tag, message, payload);
  }


};

export default new RequestController(); 