import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const role = db.models.role; 

class RoleController extends BaseController {

  constructor(){ super(role) }

  findById(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, role.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findById Role');
    return role.findOne({
      where: { roleId: req.params.roleId }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findByName(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, role.permissions.select)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('findByName Role');
    return role.findOne({
      where: {
        name: { $like: '%' + req.params.name + '%' }
      }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  create(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, role.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Create Role');
    const body = req.body;
    if (!body.name) {
      logger.error('role name needed');
      return res.status(400).send('Role name needed');
    }
    role.findOrCreate({
      where: {
        name: req.body.name
      }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  remove(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, role.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    return role.destroy({
      where: {
        roleId: req.params.roleId
      }
    })
    .then(role => { return res.status(201).send('Deleted') })
    .catch(next);
  }

};

export default new RoleController();