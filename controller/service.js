import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController';
import {checkAuthLevel}     from '../util/security/accessCheck'; 


const service = db.models.service; 
const servicecategory = db.models.servicecategory; 

let includes = [
    {model: servicecategory, required: true, as:'serviceCategory'}]; 

class ServiceController {

  constructor () { }

  findAll (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, service.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAll service');
    service.findAll({ where: req.query, include:includes })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOne (req, res,next) {

    if(!checkAuthLevel(req.currentUser.roleId, service.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOne service');
    service.findOne({ where: req.params || req.query, include:includes })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  create(req, res, next) {

  if(!checkAuthLevel(req.currentUser.roleId, service.permissions.create)){
    return res.status(403).send("Invlid user request");
  }

   logger.info('Create service');
   service.create(req.body)
    .then(doc => res.status(201).json(doc))
    .catch(next);
  }

  search(req, res, next){
    
    if(!checkAuthLevel(req.currentUser.roleId, service.permissions.select)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('Search Service ');
    return service.findAll({
      where: { name : {$like: req.params.term + '%' }}, 
      attributes: [
        'serviceId', 'name', 'ai_code', 'price', 'serviceCategoryId',
        [db.sequelize.literal('JSON_OBJECT(\'priceId\', service.serviceId, \'price\', service.price, \'ai_code\', service.ai_code)' ), 'pricing']
    ],
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

  update(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, service.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Update service');
    if (!req.body) {
      logger.error('servicesCost update error');
      return res.status(400).send('Update parameters required');
    }
   const conditions = req.params ;
   
  service.update(req.body, { where:conditions })
    .then(doc => {
      if (!doc) { return res.status(404).end(); }
      return res.status(200).json(doc);
    })
    .catch(next);
  }

  remove(req, res, next) {
    logger.info('Remove service');
   service.destroy({ where: req.params })
    .then(doc => {
      if (!doc) { return res.status(400).end(); }
      return res.status(200).send('Item deleted');
    })
    .catch(next);
  }

  findAllCategories (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, servicecategory.permissions.select)){
      return res.status(403).send("Invlid user request");
    }
    logger.info('findAllCategories');
    return servicecategory.findAll({ where: req.query })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOneCategory (req, res, next) {
    logger.info('findOneCategory');
    return servicecategory.findOne({
      where: { serviceCategoryId: req.params.serviceCategoryId }
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

  searchCategory(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, servicecategory.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('searchCategory');
    return servicecategory.findAll({
      where: { name : {$like: req.params.term + '%' }}
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

  createCategory (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, servicecategory.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('createCategory');
    let body = req.body;
    return servicecategory.findOrCreate({
      where: body
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  updateCategory (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, servicecategory.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('updateCategory');
    let body = req.body;
    return servicecategory.update(
      body,
      { where: { serviceCategoryId: req.params.serviceCategoryId }}
    )
    .then(result => { 
      return res.status(200).json(result);
    })
    .catch(next);

  }

  removeCategory (req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, servicecategory.permissions.remove)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('removeCategory');
    return servicecategory.destroy({
      where: { serviceCategoryId: req.params.serviceCategoryId }
    })
    .then (result => { return res.status(202).send('Item deleted') })
    .catch(next);
  }

};

export default new ServiceController();