// TODO: In Phase 2, deprecate and remove this controller

import azure          from 'azure-storage';
import {db}           from '../models';
import {config}       from '../config';
import logger         from '../util/logger';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const accountName = config.AZURE_STORAGE_ACCOUNT;
const accountKey = config.AZURE_STORAGE_ACCESS_KEY;
const host = config.AZURE_STORAGE_ACCOUNT + '.blob.core.windows.net';

const blobService = azure.createBlobService(
  accountName,
  accountKey,
  host
);

class StorageController {

  uploadSignatureImage(req, res, next){
    logger.info('uploadSignatureImage');
    let result = {};
    result.blobName = req.file.blobName;
    result.completeUrl = (req.file.url + config.azurestorage.signature);
    return res.status(200).json(result);
  }

  deleteSignatureImage(req, res, next){
    logger.info('deleteSignatureImage');
    blobService.deleteBlob(
      
      // Blob Container Name
      'signatures',
      
      // Blob Unique ID
      req.query.blobName,

      // Response function
      function(error, response){
        if (!error) {
          res.status(200).send('Signature image deleted')
        } else {
          next(error);
        }
      });
  }

  uploadStampImage(req, res, next){
    logger.info('uploadStampImage');
    let result = {};
    result.blobName = req.file.blobName;
    result.completeUrl = (req.file.url + config.azurestorage.signature);
    return res.status(200).json(result);
  }

  deleteStampImage(req, res, next){
    logger.info('deleteStampImage');
    blobService.deleteBlob(
      
      // Blob Container Name
      'stamps',
      
      // Blob Unique ID
      req.query.blobName,

      // Response function
      function(error, response){
        if (!error) {
          res.status(200).send('Stamp image deleted')
        } else {
          console.log('error', error);
          next(error);
        }
      });
  }

  uploadRepairImage(req, res, next){
    logger.info('uploadRepairImage');
    let result = {};
    result.blobName = req.file.blobName;
    result.completeUrl = (req.file.url + config.azurestorage.signature);
    return res.status(200).json(result);
  }

  deleteRepairImage(req, res, next){
    logger.info('deleteRepairImage');
    blobService.deleteBlob(
      
      // Blob Container Name
      'repairs',
      
      // Blob Unique ID
      req.query.blobName,

      // Response function
      function(error, response){
        if (!error) {
          res.status(200).send('Repair image deleted')
        } else {
          console.log('error', error);
          next(error);
        }
      });
  }

  uploadStorewalkImage(req, res, next){
    logger.info('uploadStorewalkImage');
    let result = {};
    result.blobName = req.file.blobName;
    result.completeUrl = (req.file.url + config.azurestorage.signature);
    return res.status(200).json(result);
  }

  deleteStorewalkImage(req, res, next){
    logger.info('deleteStorewalkImage');
    blobService.deleteBlob(
      
      // Blob Container Name
      'storewalks',
      
      // Blob Unique ID
      req.query.blobName,

      // Response function
      function(error, response){
        if (!error) {
          res.status(200).send('Storewalk image deleted')
        } else {
          console.log('error', error);
          next(error);
        }
      });
  }

};

export default new StorageController();