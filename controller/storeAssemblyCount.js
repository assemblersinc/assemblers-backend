import moment         from 'moment'; 
import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const storeAssemblyCounts = db.models.storeassemblycount; 
const includes = []; 

class StoreAssemblyCounts  {

  constructor(){
   
  }

  findAll(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, storeAssemblyCounts.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Find All storeAssemblyCount');
    return storeAssemblyCounts.findOne({
      where: req.query, include: includes
    })
    .then(result => { 
        if(!result) {return res.status(204).send("There is no data to return");}
        return res.status(200).json(result);
    })
    .catch(next);
  }

  findOne(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, storeAssemblyCounts.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOne storeAssemblyCount');
    return storeAssemblyCounts.findOne({
      where: { storeAssemblyId: req.params.storeAssemblyId
       }, include: includes
    })
    .then(result => { 
        if(!result) {return res.status(204).send("There is no data to return");}
        return res.status(200).json(result);
    })
    .catch(next);
  }

  create(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeAssemblyCounts.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Create storeAssemblyCount');
    const body = req.body;
    if (!body.locationId) {
      logger.error('body location Id  is missing');
      return res.status(400).send('storeAssemblyCounts');
    }

    const startWeekDate = moment(Date.now()).startOf('isoweek').isoWeekday(1).format('YY-MM-DD');
    const endWeekDate = moment(Date.now()).endOf('week').format('YY-MM-DD')

    storeAssemblyCounts.findOrCreate({
      where: {
        locationId: body.locationId, 
        countedDate: {
          $between: [ startWeekDate,endWeekDate]
        }
      }, 
      defaults: body
    })
    .then(result => { 
        if(!result) {return res.status(204).send("There is no data to return");}
        return res.status(200).json(result);
    })
    .catch(next);
  }

  remove(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeAssemblyCounts.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('remove StoreAssemblyCount');
    return storeAssemblyCounts.destroy({
      where: {
        storeAssemblyId: req.params.storeAssemblyId
      }
    })
    .then(counts => { return res.status(200).send('Deleted') })
    .catch(next);
  }

};

export default new StoreAssemblyCounts();