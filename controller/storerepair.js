import {config}       from '../config';
import {db}           from '../models';
import logger         from '../util/logger';
import {REQUEST_TYPE} from '../util/requestType';
import BaseController from './baseController';
import _              from 'lodash';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const storerepair       = db.models.storerepair; 
const location          = db.models.location; 
const user              = db.models.user; 
const product           = db.models.product; 
const productcategory   = db.models.productcategory; 
const repair            = db.models.repair; 
const repairCategory    = db.models.repaircategory; 
const storerepairticket = db.models.storerepairticket; 
const invoice           = db.models.invoice; 
const invoiceProduct    = db.models.invoiceproduct; 
const request           = db.models.request; 
const requestcreatedby  = db.models.requestcreatedby;
const client            = db.models.client;
const clientattributes  = db.models.clientattributes;

let includes = [
   { model: location, required: true, as:'location', 
     include:[
      { model: client, required: true, as: 'client', 
        include:
        [
          { model: clientattributes, required: false, as:'clientattributes', where: { key: 'partsLink' }}
        ] 
      }
     ]
   },
   { model: user, required: true, as:'user', otherKey:'enteredBy'},
   { model: product, required: false, as:'product' , include:[
    {model: productcategory, required:true, as: 'productcategory'}
  ]},
  { model: storerepair, required: false, as:'repairs', where : {completed: 0 },include:[
      { model: location, required: true, as:'location'},
      { model: user, required: true, as:'user', otherKey:'enteredBy'},
      { model:repair, required: true, as:'repair', otherKey:'repairId', includes:[{
          model: repairCategory, required: true, as:'repairCategory'
      }]},
   ]},
]; 

let includesPerLocation = [
  { model: storerepair, required:true, as:'repairs', where : {completed: 0 }, include:
    [
      { model: location, required: true, as:'location'},
      { model: user, required: true, as:'user', otherKey:'enteredBy'},
      { model:repair, required: true, as:'repair', otherKey:'repairId', includes:[{
          model: repairCategory, required: true, as:'repairCategory'
      }]},
  ]},
  { model: location, required: true, as:'location', 
    include:
    [
      { model: client, required: true , as: 'client', 
        include:
        [
          { model: clientattributes, required: false, as:'clientattributes', where: { key: 'partsLink' }}
        ] 
     }
    ]
  },
  { model: user, required: true, as:'user', otherKey:'enteredBy'},
  { model: product, required: false, as:'product' , include:[
      {model: productcategory, required:true, as: 'productcategory'}
  ]}
]

class StoreRepairController {

  constructor(){ }

  // Find all tickets per location
  findAllPerLocation (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAllPerLocation');
    let params = req.params; 
    return storerepairticket.findAll({
      where: {locationId: req.params.locationId},
      include: includesPerLocation
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOneTicket (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOneTicket');
    return storerepairticket.findOne({
      where: { storeRepairId: req.params.storeRepairTicketId },
      include: includes
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

   findOneRepair (req, res, next) {

    logger.info('findOneRepair');
    return storerepairticket.findOne({include:
      [
        { model: location, required: true, as:'location'},
        { model: user, required: true, as:'user', otherKey:'enteredBy'},
        { model: product, required: false, as:'product' , include:[
            {model: productcategory, required:true, as: 'productcategory'}
          ]
        },
        { model: storerepair, required: false, as:'repairs', where : {id: req.params.storeItemRepairId },include:[
            { 
              model: location, required: true, as:'location', include:[
                { model: client, required: true, as: 'client', 
                  include:
                  [
                    { model: clientattributes, required: false, as:'clientattributes', where: { key: 'partsLink' }}
                  ] 
                }
              ]
            },
            { model: user, required: true, as:'user', otherKey:'enteredBy'},
            { model:repair, required: true, as:'repair', otherKey:'repairId', includes:[{
                model: repairCategory, required: true, as:'repairCategory'
            }]},
        ]}
      ]
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

   addRepair(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('addRepair');
    let storeTicket = null; 

    let body = req.body; 
    if(!body){
      return res.status(400).send('Invalid request');
    }
    return storerepairticket.update({
      completed: 0
    },
    { 
      where:{storeRepairId: body.storeRepairId}
    }
    )
    .then(result => {
      
      if(!result){
        return null
      }; 

      return storerepairticket.findOne(
        { 
          where:{storeRepairId: body.storeRepairId}
        }
      )
      .then(result => {
        
        if(!result){
          return null; 
        }
        //assings storeTicket to var
        storeTicket = JSON.parse(JSON.stringify(result));
        
        if( body.imageData )
        {
          if(storeTicket.imageData == null)
            storeTicket.imageData = []; 

          storeTicket.imageData = storeTicket.imageData.concat(body.imageData);

        }

        // updates the images for storerepairticket. 
        return storerepairticket.update({
          imageData: storeTicket.imageData
        },
        { 

          where:{storeRepairId: storeTicket.storeRepairId}

        })
        
      })
      .then(result => {

        return storerepairticket.findOne(
          { 
            where:{storeRepairId: body.storeRepairId}
          }
        );

      })
      .then(result =>{
       
        if(!result || result.storeRepairId == null) { return res.status(204).send("There is no data to return")}
        return storerepair.create({
          storeRepairId:storeTicket.storeRepairId,
          userId: body.userId,
          locationId: body.locationId,
          imageData:body.imageData,
          qrcode:storeTicket.qrcode,
          productId:storeTicket.productId,
          repairId:body.repairId
        })
      })
      .then(result => {

        return storerepairticket.findOne(
          { 
            where:{storeRepairId: storeTicket.storeRepairId}, 
            include: includes
          }
        )
        .then(resultTicket => {
          return res.status(200).json(resultTicket);
        })

      })
    })
    .catch(next); 

  }


  updateRepair (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepair.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('updateRepair');
    let body = req.body;
    if(!body){
      return res.status(400).send('Invalid request');
    }
    return storerepair.update(
      body,
      { where: {
        id: req.params.storeItemRepairId
      }
    })
    .then(result => { 

      return storerepair.update(
        body,
        { where: {
          id: req.params.storeItemRepairId
        }
      })
      .then(res => {

        return storerepair.findOne({ where: {
            id: req.params.storeItemRepairId
          }
        })
      })
      .then(result => {
       // updates the storerapir ticket images. 
       return storerepairticket.update({
          imageData: body.imageData
        },
        { 
          where: {
            storeRepairId: result.storeRepairId
          }
        }
        )
        .then(res=>{

          //returns the same repair object. 
          return result;

        });
        
      })
      .then(result =>{

        return storerepairticket.findOne({
          where:{
            storeRepairId: result.storeItemRepairId
          }, 
          include:includes
        })
      }) 
      .then(result => {
        return res.status(200).json(result);
      })
    })
    .catch(next);

  }

  removeRepair (req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, storerepair.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('removeRepair');
    let ticketId = null;
    
    return storerepair.findOne({
      where:{id: req.params.storeItemRepairId }
    })
    .then(result => {

      ticketId = result.storeRepairId
      return storerepair.destroy({

        where: {
          id: req.params.storeItemRepairId
        }

      })
      .then(deletedItem => { 

        if (deletedItem) {
          return storerepair.findAll({ 
            where: {storeRepairId: ticketId} 
          });
        } else {
          return deletedItem;
        }

      })
      .then(result => {

        if(result && result.length == 0) {
          return storerepairticket.destroy({
            where: {
              storeRepairId: ticketId,
              completed: {$ne: 1}
            }
          })
          .then (result => {
            return res.status(204).send('Ticket is now removed!')
          })

        }
         else {

          return storerepairticket.findOne({

            where:{ storeRepairId: ticketId},
            include: includes

          }).
          then(result => {

            return res.status(200).json(result);
          
          });

        }
      })
      .catch(next);
    })
    .catch(next);
    
  }

  findAllByQRcode(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.select)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('findAllByQRcode');
    return storerepairticket.findAll({
      where: {qrcode: req.params.code},
      include: includes
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

  search(req, res, next)
  {
    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('search storerepair');
    return storerepairticket.findAll({
      where: {qrcode: {$like: req.params.term + '%'}}, 
      include: includes
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);

  }

  createTicket (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('createTicket');
    let body = JSON.parse(JSON.stringify(req.body));

    if(!body && !body.repairs) {
      {return res.status(400).send("Invalid request")}
    }
    return storerepairticket.create(
      {
        userId: body.userId,
        locationId:body.locationId,
        qrcode:body.qrcode,
        productId: body.productId,
        productName: body.productName,
        productSku:body.productSku,
        imageData: body.imageData
      }
    )
    .then(result => {
      if(!result || result.storeRepairId == null) {
        return res.status(204).send("There is no data to return")
      }
      return storerepair.create({
        storeRepairId: result.storeRepairId,
        userId: body.repairs.userId,
        locationId:body.repairs.locationId,
        imageData:result.imageData,
        qrcode:result.qrcode,
        productId:result.productId,
        repairId:body.repairs.repairId
      });

    }).then(result =>{
        
      return storerepairticket.findOne(
        { 
          where: {storeRepairId:result.storeRepairId}
         ,include: includesPerLocation
        }
      )
      .then(result =>{
        if(!result) { return res.status(204).send("There is no data to return")}
        return res.status(200).json(result);
      })
      .catch(next); 
      
    
    })
    .catch(next);
  }

  completeRepairAndMoveToInvoice (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    let _repairTicketId = null; 
    let _requestId = null; 
    let _invoiceId = null; 
    let _totalToInvoice = 0; 

    if(req.body && !Array.isArray(req.body)){
      return res.status(400).send('Invalid request!');
    }    

    return db.sequelize.transaction(t => {
      return storerepair.update(
        {
          completed: true, 
        },
        { where: {
           id: {$in : req.body} 
          },
          transaction: t
        },
      )
      .then(updatedValue => {

        if(!updatedValue) { return res.status(204).send("Repair ticket was not updated")}
        return storerepair.findAll(
          { where: {
              id: {$in : req.body} 
            }
          },
          {transaction: t}
        )
      })
      .then(repairResult => {

        
        // assigns to scope var from the first item ( all items shared the same ) 
        if(repairResult.length == 0){
           return res.status(204).send("Repair ticket was not updated") //ends the prosses and passes null value.
        }

        // gets last object to obtain the storeRepaireTicket Id. 
        _repairTicketId = repairResult[0].storeRepairId; 

        return storerepair.findAll(
          { where: {
            storeRepairId: _repairTicketId,
            completed: 0
            }
          },
          {transaction: t}
        )
      })
      .then(uncompleteRepairs => {

        // if still open items ignore and move on. 
        if(uncompleteRepairs  && uncompleteRepairs.length > 0){
          
            return null; 

        }
        else{

          // Closes the repair ticket. 
          return storerepairticket.update(
              {
                completed: true
              },
              { where: {
                storeRepairId: _repairTicketId
                },
                transaction: t
              },
              
            );
        }
      })
      .then(repaireItem => {

        return storerepairticket.findOne(
          { where: {
             storeRepairId: _repairTicketId
            }, 
            include: [{ model: storerepair, required: true, as:'repairs', where : {id: {$in:req.body }
               }, include: [{ model:repair, required: true, as:'repair', otherKey:'repairId'}]
              }], 
            subQuery: false
          },
          {transaction: t}
        );
      })
      .then(result =>{

        return request.create({
            locationId: result.locationId,
            requestedById: req.currentUser.userId,
            comments: "Repaired Items!  - " + req.body.toString(),
            requestTypeId: REQUEST_TYPE.REPAIR,
            scheduleDate: Date.now(), 
            photos:result.imageData, 
            createTime: Date.now(), 
            completed: 1, 
            endTime: Date.now(), 
          },
          {
            include: [{ model: location, required: true, as: 'location'}], 
            transaction: t
          });

      })
      .then((requestResult) => {


        return location.findOne({
          where: {locationId: requestResult.locationId}
        },
        {
          include: [{ model: location, required: true, as: 'location'}], 
          transaction: t
        }).
        then(loc => {
          
          if(loc){
            requestResult.location = Object.assign({}, loc.get({plain:true})); 
            return requestResult;
          }
          else 
          {
            return t.ROLLBACK; 
          }

        });

      })
      .then(requestResult =>{

        _requestId = requestResult.requestId; 

        return requestcreatedby.create({
          requestId: requestResult.requestId,
          userId: req.currentUser.userId,
          firstName: req.currentUser.firstName,
          lastName:  req.currentUser.lastName,
          phoneNumber: req.currentUser.phone,
          email:  req.currentUser.email,
          position: "Assemblers employee"
        }, 
        {
          transaction: t
        })
        .then(res =>{
          return requestResult; 
        });
      })
      .then(result =>{

        return invoice.findOrCreate({
          where:{
            userId: req.currentUser.userId, 
            clientId: result.location.clientId, 
            locationId: result.location.locationId, 
            invoiceTypeId: 1, 
            invoiceCompletedTime: {$eq: null}
          },
          defaults: {
            userId: req.currentUser.userId, 
            invoiceNumber: 'N/A', 
            requestId: null,
            clientId: result.location.cientId, 
            invoiceTypeId: 1,
            locationId: result.location.locationId

          },
          order: [['invoiceGeneratedTime','DESC']], 
          limit:1,
          transaction: t
        });

      })
      .spread(invoice => {

        if(!invoice) { return null ; }
    
        _invoiceId = invoice.invoiceId; 

        return storerepairticket.findOne(
          { where: {
              storeRepairId: _repairTicketId
            }, 
            include: [{ model: storerepair, required: false, as:'repairs'
                        , where : {id: {$in:req.body}}, include:[ 
                                { model:repair, required: true, as:'repair', otherKey:'repairId'}] 
                      }], 
            subQuery: false
          },
          {transaction: t
          }
        ).
        then(result =>{

          let res = result.get({plain:true});
          let arrayOfItems = []; 

          /* push all repairs to invoice */
          _.forEach(res.repairs, (repair)=>{
           
            _totalToInvoice += repair.repair.price; 

            arrayOfItems.push({
              invoiceId: _invoiceId, 
              productId: repair.id, 
              itemId: repair.id,
              requestId: _requestId,
              requestItemId: repair.id, // references the repair on invoice item. 
              name: repair.repair.name,
              qty:1,
              total: repair.repair.price,
              pricePerUnit:repair.repair.price,
              ai_code: repair.repair.ai_code,
              isExpenseItem: 0, 
              isService:0, 
              serialNumber: null, 
              entryType : 'R'
            })
          });

          return invoiceProduct.bulkCreate(arrayOfItems, { individualHooks: true, transaction: t });
          
        });

      })
      .then(result => {

        return invoice.findOne({
          where: {
            invoiceId: _invoiceId
          }
        }, 
        { transaction:t }
        )
        .then(invoice => {
          console.log('Total '  + invoice.total + _totalToInvoice)
          return invoice.update(
            { total : invoice.total + _totalToInvoice},
            { where: { invoiceId: _invoiceId }, 
              transaction:t 
            });

        }) 
       
      })
      .then(result => {

        return storerepairticket.findOne(
          { where: {
              storeRepairId: _repairTicketId
            }, 
            include: [{ model: storerepair, required: false, as:'repairs'
                        , where : {id: {$in:req.body}}, include:[ 
                                { model:repair, required: true, as:'repair', otherKey:'repairId'}] 
                      }], 
            subQuery: false
          },
          {transaction: t}
        );


      })
      .then(finalResult =>{
        console.log('final response'); 
        if(!finalResult) { return res.status(204).send("Item repair doesn't exist")}
        return res.status(200).json(finalResult);
      })
    })
    .catch(next);
  }

  updateTicket (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('updateTicket');
    
    let body = req.body;
    
    return storerepairticket.update(
      body,
      { where: {
        storeRepairId: req.params.storeRepairTicketId,
        locationId: req.params.locationId
      }
    })
    .then(update => { 

      return storerepairticket.findOne({
        where: {
          storeRepairId: req.params.storeRepairTicketId,
          locationId: req.params.locationId
        }
      })
      .then(result => {
        console.log(result); 
        return res.status(200).json(result);
      })
      .catch(next);   
    })
    .catch(next);
  }


  removeTicket (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storerepairticket.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('removeTicket');
    return storerepairticket.destroy({
      where: {
        storeRepairId: req.params.storeRepairTicketId,
        locationId: req.params.locationId
      }
    })
    .then (result => { return res.status(200).send('Item Deleted')})
    .catch(next);
  }
}

export default new StoreRepairController();