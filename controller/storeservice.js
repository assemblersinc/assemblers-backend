import logger         from '../util/logger';
import {db}           from '../models';
import BaseController from './baseController';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const location = db.models.location; 
const service = db.models.service;
const serviceCategory = db.models.serviceCategory;
const storeservice = db.models.storeservice;
const user = db.models.user; 

let includes = [
   { model: location, required: true, as:'location'},
   { model: user, required: true, as:'user', otherKey:'enteredBy'},
   { model:service, required: true, as:'service', otherKey:'serviceId', includes:{
      model: serviceCategory, required: true, as:'serviceCategory'
   }}
]; 

class StoreServiceController {
  
  constructor(){
  }

  findAll (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeservice.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAll Services');
    return storeservice.findAll({
      where: {locationId: req.params.locationId},
      include: includes
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOne (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeservice.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOne Service');
    return storeservice.findOne({
      where: { storeServiceId: req.params.storeServiceId },
      include: includes
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  create (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeservice.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Create Service');
    let body = req.body;
    return storeservice.findOrCreate({
      where: body,  include:includes
    })
    .then(result => { 
      if(!result) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  update (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeservice.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Update Service');
    let body = req.body;
    return storeservice.update(
      body,
      { where: { storeServiceId: req.params.storeServiceId }
    })
    .then(result => { 
      return res.status(200).json(result);
    })
    .catch(next);
  }

  remove (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, storeservice.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('Remove Service');
    return storeservice.destroy({
      where: { storeServiceId: req.params.storeServiceId }
    })
    .then (result => { return res.status(200).send('Item deleted') })
    .catch(next);
  }
}

export default new StoreServiceController(); 
