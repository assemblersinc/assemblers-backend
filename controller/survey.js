import logger         from '../util/logger';
import {config}       from '../config';
import {db}           from '../models';
import BaseController from './baseController'; 
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const client          = db.models.client; 
const invoice         = db.models.invoice; 
const location        = db.models.location; 
const survey          = db.models.survey;
const surveyquestion  = db.models.surveyquestion;
const user            = db.models.user; 

let includes = [
  { model: surveyquestion, required: true, as:'survey'},
  { model: user, required: true, as:'user'},
  { model: invoice, required: true, as:'invoice'},
  { model: location, required: true, as:'location', 
    include:[{model: client, required:true, as: 'client', otherKey:'clientId'}]}
  //{ model: clientattributes, required: false, as:'clientattributes'}
]; 

class SurveyController extends BaseController { constructor(){ super(surveyquestion); }

  findAllSurvey (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, survey.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAllSurvey');
    let query = req.query;
    return survey.findAll({
      where: query
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findAllSurveyQuestion (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, surveyquestion.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findAllSurveyQuestion');
    let query = req.query;
    query.active = true;
    return surveyquestion.findAll({
      where: query
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return") }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  findOneSurvey (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, survey.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('findOneSurvey');
    survey.findOne({
      where: {surveyId: req.params.surveyId}
    })
    .then(result => { 
      if(!result) {
        return res.status(204).send("There is no data to return")
      } else {
        return res.status(200).json(result);
      }
    })
    .catch(next);
  }

  saveCompletedSurveyQuestions(req,res, next){
  logger.info('saveCompletedSurveyQuestions');
  let completedSurvey = req.body;
  return survey.bulkCreate(
    completedSurvey,
    // If the question has already been answered, `updateOnDuplicate` will upsert data
    {updateOnDuplicate: ['userId', 'locationId', 'questionId', 'invoiceId', 'response']}
    )
    .then(result => res.status(200).json(result))
    .catch(next);
  }

}

export default new SurveyController();