import {email}        from '../util/mail';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

class TestController  {    

    sendEmail(req, res, next){
    
    let mailOptions = {
        from: 'no-reply@assemblersinc.net', // sender address
        to: 'n.schwartz@assemblersinc.net', // list of receivers
        subject: 'PRICE REQUEST FROM APP', // Subject line
        text: 'Below find the details of the application requet items \n'
    };
     // send mail with defined transport object
    email.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log(error);
            return res.status(200).json({"test failed": "test"});
        }
        console.log('Message %s sent: %s', info.messageId, info.response);
        return res.status(200).json({"test": "test"});
    });
    }
}
 export default new TestController(); 