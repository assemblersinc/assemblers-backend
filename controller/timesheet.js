import _                  from 'lodash';
import logger             from '../util/logger';
import {db}               from '../models';
import userController     from '../controller/user';
import BaseController     from './baseController';
import moment             from 'moment';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const timesheet           = db.models.timesheet;
const user                = db.models.user;
const location            = db.models.location;
const storeassemblycounts = db.models.storeassemblycount;
const region              = db.models.region;
const geoarea             = db.models.geoarea;
const role                = db.models.role;


// For counts performed on current week. 
const startWeekDate = moment(Date.now()).startOf('isoweek').isoWeekday(1).format('YY-MM-DD');
const endWeekDate = moment(Date.now()).endOf('week').add(1, 'd').format('YY-MM-DD');

let includes = [
  { model: user, required: true, as:'user'},
  { model: location, required: true, as:'location',
    include:{ model: storeassemblycounts, required: false, as:'storeassemblycounts', 
    where:{countedDate: {
      $between: [ startWeekDate,endWeekDate ]
    }}
  }
  }
];

let includesUser = [
	{model: role, required: true, as: 'role'},
	{
		model: geoarea, required: true, as: 'geoarea'
		, include:
		[{model: user, required: false, as: 'manager', otherKey: 'managerId'},
			{model: user, required: false, as: 'secondManager', otherKey: 'managerSecondId'},
			{model: user, required: false, as: 'thirdManager', otherKey: 'managerThirdId'},
			{model: user, required: false, as: 'opsManager', otherKey: 'opsManagerId'},
			{model: region, required: false, as: 'region', foreingKey: 'regionId'}
		]
	},
	{
		model: timesheet, required: false, as: 'timesheet',otherKey:'timeId', include: [
				{model: location, required: true, as: 'location', otherKey: 'locationId'}]
	}
];

// TODO: integrate with the Optimun database. 

class TimesheetController extends BaseController {

  clockin (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.create)){
      return res.status(403).send("Invlid user request");
    }
    
    logger.info('Clock In');
    const body = req.body
    
    if (!body.userId || !body.locationId || !body.timeIn) {
      logger.error('Clockin missing inputs');
      return res.status(400).send('Missing required inputs');
    } else {
      logger.info('Clocking in userId:', body.userId);
      return db.sequelize.query("CALL sp_getlastusertimesheet(:userId)", {
        replacements: {
          userId: body.userId
        }
      })
      .spread((ts) => {

        if (ts && ts.timeId) {
          logger.info("User already logged in");

          // returns 400 cos user is clocked in, returls actual timeline object.
          return user.findOne({
            where: {
              userId:  body.userId,
              active: 1
            },
            include: includesUser
            ,subQuery:false
          })
          .then(user => {
            if (user) {
              //returns user information with error 400 indicating user is already clocked in
              logger.warning('Already clocked in, userId:', body.userId);
              return res.status(400).json(user);
            } else {
              return res.status(400).send('No user found');
            }
          })
          .catch(next);
        } 
        else 
        {

          return db.sequelize.transaction((t)=> {
            return timesheet.create({
                userId: body.userId,
                timeIn: body.timeIn,
                locationId: body.locationId
              },
              {
                include: includes,
                transaction: t
              },
            )
            .then((ts) => {
               return user.update({
                  timeSheetId: ts.timeId,
                  clockStatus: 'clock-in'
                }, {
                  where: {
                    userId:  body.userId
                  },
                  transaction: t
                })
                .then(res => {
                  return ts;
              })
              .catch(next);
            })
            .then((ts) => {
              return timesheet.findOne({
                where: {
                  timeId: ts.timeId
                },
                include:includes,
                transaction: t}
                , {}
              )
            })
            .then((result) => {
  
             let query = "INSERT INTO [dbo].[ClockData] (ClockDataGUID, Clock, Node, Hours, EventSource," 
                          + "BadgeNumber, DateTime, EventTypeID,updatedBy,LastUpdated, timestamp,ClockDescription)"
                          + "VALUES (NEWID ( ) ,1, 1, 0, 2, '" +  result.user.techId  + "',GETDATE(), 'CI', '" + result.user.firstName + ' ' + result.user.lastName + "', GETDATE(), DEFAULT, 'App clock-in' );";
              
              return db.optimum.query(query)
              .then(optimum => {
                logger.info('optimum', optimum);

                return result;

              }).catch(next =>{

                 t.rollback();
                 return res.status(400).send('Problem with Optimun clock. Contact administrator.');

              });
            
              
            })
            .catch(next);
              
        })
        .then(result => {
          logger.info('Clocked in userId:', result.userId, 'timeId:', result.timeId);
          return res.status(200).json(result);
        })
        .catch(next);
      }          
    })
    .catch(next);
    }
  }

  mealout(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    const body = req.body
    if (!body.timeId || !body.userId || !body.mealOut) {
      logger.error('Mealout missing inputs');
      return res.status(400).send('Missing required inputs');
    } 
    else {
    
      return db.sequelize.transaction((t) => 
      {

        return timesheet.update({
          mealOut: body.mealOut
        }, 
        {
          where: {
            timeId: body.timeId,
            userId: body.userId,
            timeOut: null,
            mealOut: null
          },
          include: includes
        }, 
        { 
          transaction: t 
        })
        .spread((ts) => {

          // Update user timesheet Id to null for clockout. 
          if (ts > 0 ) 
          {
            // Remove the timeId from the user table. 
            return userController.updateUserTimesheetId(
              body.timeId,
              body.userId,
              'meal-out'
            );

          }
          else{
            return null; 
          }
      })
      .then(result => 
      {
        if (!result)
          return null; 

        return timesheet.findOne({
          where: {
            timeId: body.timeId
          },
          include:includes,
          transaction: t
        });
      })
      .then(result => {
         
         if(!result){
           return null; 
         }

          logger.info('Meal out userId:', result.userId, 'timeId:', body.timeId);
          
          let query = "INSERT INTO [dbo].[ClockData] (ClockDataGUID, Clock, Node, Hours, EventSource," 
                      + "BadgeNumber, DateTime, EventTypeID,updatedBy,LastUpdated, timestamp,ClockDescription)"
                      + "VALUES (NEWID ( ) ,1, 1, 0, 2, '" +  result.user.techId  + "',GETDATE(), 'MO', '" + result.user.firstName + ' ' + result.user.lastName + "', GETDATE(), DEFAULT, 'App meal-out' );";
          
          return db.optimum.query(query)
           
        })
        .then(optimum => {
          
          logger.info('optimum', optimum);

          if(!optimum){
            return null
          }

          return timesheet.findOne({
            where: {
              timeId: body.timeId
            },
            include:includes,
            transaction: t
          });

        })
        .then(result => {

          if(!result){
            return res.status(400).send('Error setting mealout out or user is already on meal out');
          }

          return res.status(200).json(result); 

        })
        .catch((next) =>{
          t.rollback();
          return res.status(400).send('Problem with Optimun clock. Contact administrator.');
        });

    });
   }

  }

  mealin(req, res, next){

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.create)){
      return res.status(403).send("Invlid user request");
    }

    const body = req.body;
    if (!body.timeId || !body.userId || !body.mealIn) {
      logger.error('mealIn missing inputs');
      return res.status(400).send('Missing required inputs');
    } 
    else {
    
      return db.sequelize.transaction((t) => 
      {

        return timesheet.update({
          mealIn: body.mealIn
        }, 
        {
          where: {
            timeId: body.timeId,
            userId: body.userId,
            timeOut: null,
            mealOut: {$ne:null}, 
            mealIn: null, 
          },
          include: includes
        }, 
        { 
          transaction: t 
        })
        .spread((ts) => {

          // Update user timesheet Id to null for clockout. 
          if (ts > 0 ) 
          {
            // Remove the timeId from the user table. 
            return userController.updateUserTimesheetId(
              body.timeId,
              body.userId,
              'clock-in' // back to clock in (from meal in). 
            );

          }
          else
          {
            return null; 
          }
      })
      .then(result => 
      {
        if (!result)
          return null; 

        return timesheet.findOne({
          where: {
            timeId: body.timeId
          },
          include:includes,
          transaction: t
        });
      })
      .then(result => {
         
         if(!result){
           return null; 
         }

          logger.info('Meal out userId:', result.userId, 'timeId:', body.timeId);
          
          let query = "INSERT INTO [dbo].[ClockData] (ClockDataGUID, Clock, Node, Hours, EventSource," 
                      + "BadgeNumber, DateTime, EventTypeID,updatedBy,LastUpdated, timestamp,ClockDescription)"
                      + "VALUES (NEWID ( ) ,1, 1, 0, 2, '" +  result.user.techId  + "',GETDATE(), 'MI', '" + result.user.firstName + ' ' + result.user.lastName + "', GETDATE(), DEFAULT, 'App meal-out' );";
          
          return db.optimum.query(query)
           
        })
        .then(optimum => {

          if(!optimum){
            return null
          }
          
          return timesheet.findOne({
            where: {
              timeId: body.timeId
            },
            include:includes,
            transaction: t
          });

        })
        .then(result => {
          
          if(!result){
            return res.status(400).send('Error setting meal in or user is already on meal IN')
          }

          return res.status(200).json(result); 

        })
        .catch((next) =>{
          t.rollback();
          return res.status(400).send('Problem with Optimun clock. Contact administrator.');
        });

    });
   }

  }

  clockout (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.update)){
      return res.status(403).send("Invlid user request");
    }
    
    const body = req.body
    if (!body.timeId || !body.userId || !body.timeOut) {
      logger.error('Clockout missing inputs');
      return res.status(400).send('Missing required inputs');
    } else {
      return db.sequelize.transaction((t) => {
        return timesheet.update({
          timeOut: body.timeOut
        }, {
          where: {
            timeId: body.timeId,
            userId: body.userId,
            timeOut: null
          },
          include: includes
        }, 
        { transaction: t })
        .spread((ts) => {
          // Update user timesheet Id to null for clockout. 
          if (ts > 0 ) {
            // Remove the timeId from the user table. 
            return userController.updateUserTimesheetId(
              body.timeId,
              body.userId,
              'clock-out'
            );
          }else{
            return null; 
          }
      })
      .then(result => {
        if (!result)
          return null; 

        return timesheet.findOne({
          where: {
            timeId: body.timeId
          },
          include:includes,
          transaction: t
        });
      })
      .then(result => {
         
         if(!result){
           return null; 
         }

          logger.info('Clocked out userId:', result.userId, 'timeId:', body.timeId);
          
          let query = "INSERT INTO [dbo].[ClockData] (ClockDataGUID, Clock, Node, Hours, EventSource," 
                      + "BadgeNumber, DateTime, EventTypeID,updatedBy,LastUpdated, timestamp,ClockDescription)"
                      + "VALUES (NEWID ( ) ,1, 1, 0, 2, '" +  result.user.techId  + "',GETDATE(), 'CO', '" + result.user.firstName + ' ' + result.user.lastName + "', GETDATE(), DEFAULT, 'App clock-in' );";
          
          return db.optimum.query(query)
           
        })
        .then(optimum => {
          if(!optimum){
            return res.status(400).send('Error clocking out or user is already clocked out');
          }
          logger.info('optimum', optimum);
            return res.status(200).send('User clocked out');
        }).catch((next) =>{
          t.rollback();
          return res.status(400).send('Problem with Optimun clock. Contact administrator.');
        })
        .catch(next);
    });
  }

}

  fixTime (req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    const body = req.body
    if (!body.timeId || !body.userId || !body.timeOut) {
      logger.error('fixtime missing inputs');
      return res.status(400).send('Missing required inputs');
    } else {
      return timesheet.update({
        timeOut: body.timeOut,
        hasProblem: true,
        comment: body.comment
      }, {
        where: {
          timeId: body.timeId,
          userId: body.userId
        }, 
        include: includes
      })
      .then(result => {
        logger.info('fixTime, updated userId: ', body.userId, 'timeId: ', body.timeId)
        return res.status(200).send('Timesheet updated.')
      })
      .catch(next);
    }
  }

  findRecentTimesheetsForUser (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    if(!req.params.userId){
      return //TODO
    }
     db.sequelize.query('CALL sp_gettimesheetsperuser(:userId)',
       { replacements: { userId: req.params.userId } })
       .then(times => {
        let grouped = _.map(_.groupBy(times, 'weekdate'),(b) => {
          return _.extend(_.pick(b[0], 'weekdate', 'weekId'), {
            days: _.map(b, (elem) => {
              return _.pick(elem, 'timeId', 'userId', 'timeIn', 'timeOut', 'hasProblem', 'comment', 'locationId',
                'location', 'address', 'city', 'state', 'zip' )
              })
          });
      });
      if(!grouped) {return res.status(204).send("There is no data to return")}
      return res.status(200).json(grouped);
    })
    .catch(next);
  }

  updateTravelToAnotherStore(req, res ,next){

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.update)){
      return res.status(403).send("Invlid user request");
    }
    
    if(!req.body){
      return res.status(400).send("Invalid request");
    }

    let body = req.body;

    return timesheet.findOne({
      where: {  
        timeId: req.params.timeId,
        locationId: body.locationId
      }, 
      include: includes
    })
    .then(result =>{
      // returns the same locations object. 
      if(result){
        logger.warning('user is traveling to the same store?')
        return res.status(200).json(result); 
      }
       // updates user timesheet per location //logs them out and re clocks them in at the new traveling location.
      return timesheet.update({
        timeOut: body.timeOut
      }, 
      {
        where: {
          timeId: req.params.timeId,
          userId: req.params.userId,
          timeOut: null
        },
        include: includes
    })
    .then(result => { 
      
      if(!result || !result.length > 0) 
      { 
        return res.status(204).send("There is no data to return")
      }

      return timesheet.create({
              userId: req.params.userId,
              timeIn:  body.timeOut, // time out location from the traveling store. 
              locationId: body.locationId
              },
              {include: includes }
      );
            
    })
    // Updates the user result 
    .then(result => {

        return user.update({
          timeSheetId: result.timeId,
          clockStatus: 'clock-in'
        }, {
          where: {
            userId: req.params.userId
          }
        })
        .then(userResult => {
          // returns the created item.
          return result; 
        })
        .then(result =>{

          return timesheet.findOne({
            where:{timeId: result.timeId},
            include:includes
          })  
          
        })
        .then (result=>{
          if(!result) {res.status(204).send("there is no data to display!");}
          return res.status(200).json(result);

        })

      })    
    })
    .catch(next); 
   

  }

  findOneTimesheetForUser (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    timesheet.findAll({
      where: {
        userId: req.params.userId,
        timeId: req.params.timeId
      }, 
      include: includes
    })
    .then(result => { 
      if(!result || !result.length > 0) { return res.status(204).send("There is no data to return")}
      return res.status(200).json(result);
    })
    .catch(next);
  }

  remove (req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, timesheet.permissions.delete)){
      return res.status(403).send("Invlid user request");
    }

    return timesheet.destroy({
      where: {
        userId: req.params.userId,
        timeId: req.params.timeId
      }
    })
    .then(timesheet => {
      logger.info('userid', req.params.userId, 'timeid', req.params.timeId, 'destroyed');
      return res.status(200).send('Timesheet removed');
    })
    .catch(next);
  }
};

export default new TimesheetController();
