import MobileDetect         from 'mobile-detect';
import * as notificationHub from '../util/notificationHub';
import {ROLE_TYPE}          from '../util/roleType';
import logger               from '../util/logger';
import {config}             from '../config';
import {db}                 from '../models';
import BaseController       from './baseController';
import {checkAuthLevel}     from '../util/security/accessCheck'; 

const geoarea = db.models.geoarea;
const location = db.models.location;
const region = db.models.region;
const role = db.models.role;
const timesheet = db.models.timesheet;
const user = db.models.user;
const managerclaimedtech = db.models.managerclaimedtech;


let includes = [
  {model: role, required: true, as: 'role'},
   {
     model:managerclaimedtech, required: false, as:'claimedby', 
     on: {
      $and:[
        db.sequelize.where(db.sequelize.col("user.userId"), "=", db.sequelize.col("claimedby.techId")),
        db.sequelize.where(db.sequelize.col("user.managerId"), "=", db.sequelize.col("claimedby.managerId"))]
      },
     attributes: ['managerId', 'techId', 'chatName', 'createdAt', 'updatedAt'], 
    include:[{model:user, as: 'manager' }]},
  {
    model: geoarea, required: true, as: 'geoarea'
    , include:
    [ 
      {model: user, required: false, as: 'manager', otherKey: 'managerId'},
      {model: user, required: false, as: 'secondManager', otherKey: 'managerSecondId'},
      {model: user, required: false, as: 'thirdManager', otherKey: 'managerThirdId'},
      {model: user, required: false, as: 'opsManager', otherKey: 'opsManagerId'},
      {model: region, required: false, as: 'region', foreingKey: 'regionId'}
    ]
  },
  {
    model: timesheet, required: false, as: 'timesheet',otherKey:'timeId', include: [
        {model: location, required: true, as: 'location', otherKey: 'locationId'}]
  }
];


class UserController extends BaseController {

  constructor() {
    super(user)
  }

  /**
   * Find all users
   */
  findAll(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.debug('findAll User');
    user.findAll({
      where: {
        active: 1
      },
      include: includes
    })
    .then(result => {
      if (!result || !result.length > 0) {
        return res.status(204).send("There is no data to return")
      }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  /**
   * Find all users by areaId
   */
  findAllByArea(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.debug('findAllByArea');
   
    let whereClause = {}; 
    Object.assign(whereClause, req.query, {areaId: req.params.areaId}); 

    return user.findAll({
      where: whereClause,
      include: includes
    })
    .then(result => {
      if (!result || !result.length > 0) {
        return res.status(204).send("There is no data to return")
      }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  /**
   * Returns a user by userid
   */
  findOneById(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.debug('findOneById');
    user.findOne({
      where: {
        userId: req.params.userId,
        active: 1
      },
      include: includes
    })
    .then(result => {
      if (!result) {
        return res.status(204).send("There is no data to return")
      }
      return res.status(200).json(result);
    })
    .catch(next);
  }

  /**
   * Returns user by username
   */
  findOneByUsername(req, res, next) {

    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.debug('findOneByUsername');
    user.findOne({
      where: {
        username: req.params.username,
        active: 1
      },
      include: includes
    })
    .then(user => {
      return res.status(200).json(user)
    })
    .catch(next);
  }

  /**
   * Returns the locations for a particture user id
   */
  findAllUserLocations(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.debug('findAllUserLocations');
    let query = {};
    if (req.query.clientId) {
      query.clientId = req.query.clientId;
    }
    region.findAll({
      where: {
        active: 1
      },
      include: [{
        model: geoarea, required: true, as: 'geoareas',
        include: [{model: location, required: true, as: 'locations', where: query},
          {
            model: user, required: true, as: 'users', where: {userId: req.params.userId}
          }]
      }]
    })
    .then(user => {
      return res.status(200).json(user)
    })
    .catch(next);
  }

  /**
   * Returns the authenticated user and updates the token values.
   * DEPRICATED - we should no longer store the tokens.
   */
  getAuthenticatedUser(req, res, next) {


    let tokenUpdate = {
      token: req.param.accessToken,
      refreshToken: req.param.refreshToken,
      tokenExpiresOn: req.param.expiresOn
    }
    return user.findOne({
      where: {
        username: req.param.username,
        active: 1
      },
      include: includes
      ,subQuery:false
    })
    .then(user => {
      if (user) {
        user.update(tokenUpdate);
        return res.status(200).json(user);
      } else {
        return res.status(400).send('No user found');
      }

    })
    .catch(next);
  }

  updateUser(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.debug('Update User');
    if (!req.body) {
      logger.error('User update body error');
      return res.status(400).send('Missing required inputs');
    }

    return user.update(req.body,
      {where: {userId: req.body.userId}})
    .then(doc => {
      if (!doc) {
        return res.status(400).send("Error updating user");
      }
      return res.status(200).json(doc);
    })
    .catch(next);
  }
  /**
   * Updates the user for logout.
   */
  update(req, res, next) {
  

    if (!req.body) {
      logger.error('User update body error');
      return res.status(400).send('Missing required inputs');
    }
    const conditions = req.params;

    return user.findOne({
      where: {userId:req.body.userId} 
    })
    .then(result => {

        if(!result) {
          return res.status(400).send("user doesnt exist")
        }

        /*updates notifications to not receive any after logout */
        if(result.userDeviceOS && result.notificationToken){

           return notificationHub.register(result.userDeviceOS, result.notificationToken, ['no_logged_in'])
            .then((notification) => {
            
                  return user.update({
                    token: null,
                    refreshToken: null,
                    tokenExpiresOn: null
                  },
                  {where: {userId: req.body.userId}})
                  .then(doc => {
                    if (!doc) {
                      return res.status(400).end()
                    }
                    return res.status(200).send('Logged out')
                })
            });
      }else {
        return res.status(200).send('Logged out');
      }
    })
    .catch(next); 


    
  }

  /**
   * Helper function to update the timesheet fields everytime a user clocks-in/clocks-out.
   */
  updateUserTimesheetId(timeId, userId, event) {

    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.update)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('updateUserTimesheetId');
    if (!timeId || !userId || !event) {
      logger.error('User clock missing inputs');
      return Promise.resolve({'message': 'Missing inputs'});
    } else {
      // Update user table
      return user.update({
        timeSheetId: timeId,
        clockStatus: event
      }, {
        where: {
          userId: userId
        }
      });
    }
  }

  /**
   * Token for push notifications.
   */
  registerPush(req, res, next) {
    
    if(!checkAuthLevel(req.currentUser.roleId, user.permissions.select)){
      return res.status(403).send("Invlid user request");
    }

    logger.info('registerPush');
    if (!req.body.registration.token || !req.body.registration.os || !req.body.userId) {
      return next('INVALID_REQUEST');
    }

    user.update({
      notificationToken: req.body.registration.token,
      userDeviceOS: req.body.registration.os
    }, {
      where: {
        userId: req.body.userId
      }
    })
    .then(result => {
      return user.findById(req.body.userId);
    })
    .then(user => {
      if (!user) {return Promise.reject('No user?');}

      const tags = ['user_tag_' + req.currentUser.userId, 'global'];

      if (user.roleId === ROLE_TYPE.AREA_MANAGER) {
        tags.push('area_manager');
      } else if (user.roleId === ROLE_TYPE.TECH) {
        tags.push('tech');
      } else if (user.roleId === ROLE_TYPE.OPS_MANAGER) {
        tags.push('ops_manager');
      }
      else if (user.roleId === ROLE_TYPE.STORE) {
        tags.push('store');
      }
      return notificationHub.register(req.body.registration.os, req.body.registration.token, tags)
      .then(() => {
        return  res.status(200).json(user)
      });
    })
    .catch(next);
  }

};

export default new UserController();
