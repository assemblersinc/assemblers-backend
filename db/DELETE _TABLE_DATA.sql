
DELETE FROM `timesheet`;
DELETE FROM `survey`;
DELETE FROM `storewalk`;
DELETE FROM `storerepair`;
DELETE FROM `storerepairticket`;
DELETE FROM `storeassemblycount`;
DELETE FROM `requestseenbylog`;

DELETE FROM `requestproduct`;
DELETE FROM `requestcreatedby`;
DELETE FROM `invoiceproduct`;
DELETE FROM `invoice`;


DELETE FROM `request`;

DELETE FROM `conversation`;
DELETE FROM `announcement`;

DELETE FROM `pricingrequest`;
DELETE FROM `managerclaimedtech`;


