-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `clientId` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `storeId` varchar(15) NOT NULL,
  `vendorNumber` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `phoneNumber` varchar(15) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `contact` varchar(45) DEFAULT NULL,
  `active` bit(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientId`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (95,'LOW','LMAF17N93Z',NULL,'Lowes','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(96,'AIH','9EFA5V6UIE','40982','Academy Sports and Outdoors','','mike@4allthingsweb.com','','','2017-06-08 13:37:41','2017-09-11 17:38:56'),(97,'PHY','F1P7793JWU',NULL,'Physique Fitness Equipment','','info@physiquefitnessequipment.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(98,'TRU','VAE7BY1APZ',NULL,'True Fitness','','lisa@assemblersinc.net','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(99,'DSG','WZ4RIA24XZ',NULL,'Dicks Sporting Goods','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(100,'LISA','8AMX67F9Z1',NULL,'Lisa\'s Shop','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(101,'SHD','IHYZTYB1HB',NULL,'Sears','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(102,'CIH','AT9ZCW151Q',NULL,'Customer In Home','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(103,'SC','F1WU90QBB2',NULL,'Service Call','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(104,'KEYS','BW8W0392QU',NULL,'Keys Fitness','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(105,'QTF','WGZVBJP5RJ',NULL,'Quantum Fitness','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(106,'PMSER','S8XCK3O5SD',NULL,'PM Service','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(107,'DEMO','SNMXVGG4T6',NULL,'demo','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(108,'UTS','XBKQ9CHOO8',NULL,'UTS','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(109,'ALG','CX7U3HYEQP',NULL,'ALG','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(110,'HOR','1N8HRL9RW4',NULL,'Horizon','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(111,'NAU','1DG3CNJ7E2',NULL,'Nautilus','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(112,'PHO','PWDH3SGKF9',NULL,'Phoenix','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(113,'SPR','ZP528ZLMJ6',NULL,'Spirit','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(114,'BDNU','HGPA8H1TAM',NULL,'Batca-do not use','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(115,'EE','76D21GZJAJ',NULL,'Exercise Essentials','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(116,'FCW','UOAJZ10STL',NULL,'Fitness Club Warehouse','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(117,'FES','XJDZ9XIGLW',NULL,'Fitness Equipment Sales','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(118,'BAT','YCSSD6NW8Y',NULL,'Batca Fitness','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(119,'EF','N8P0N17NOJ',NULL,'Expresso Fitness','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(120,'FFTS','XJN0QCR1LQ',NULL,'Fitco Fitness','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(121,'SMA','I9V7Q2BS0M',NULL,'Smart Furniture','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(122,'NUS','M8WIQX8TY3',NULL,'Nustep','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(123,'BG','61AQINTU3C',NULL,'Body Guard','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(124,'SVL','7D62OU2HCN',NULL,'Service Live','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(125,'FTCO','52B0X3TVKL',NULL,'Fitco','','mike@4allthingsweb.com','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(126,'ACFT','O246F2X4X4',NULL,'Accell Fitness','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(127,'SPCT','4PQZPQUQ9A',NULL,'SportsCraft','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(128,'USAM','FR2NY1YLZE',NULL,'US Assembly','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(129,'EGC','JSB90NPN20',NULL,'Ergonomically Correct','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(130,'PRO','CQVVHADPHK',NULL,'ProMaxima','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(131,'MTX','T8MXS036CG',NULL,'Matrix Fitness','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(132,'ICN','S93CA1SHPU',NULL,'Icon','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(133,'SAR','WAND6G572A',NULL,'SportsArt','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(134,'VIS','GOL2QZT1KV',NULL,'Vision Fitness','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(135,'GFIT','XJVCDR5FK4',NULL,'Global Fitness','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(136,'MNCOM','PZ15YXREVP',NULL,'Meehan & Company','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(137,'SAMG','IKQ07ECB01',NULL,'Samsung-Plus One Solutions','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(138,'HDP','QCJU6L20X9','68282','Home Depot','','','','','2017-06-08 13:37:41','2017-09-11 17:38:56'),(139,'TOY','DLIXD4AM6E',NULL,'Toys R Us','','','','','2017-06-08 13:37:41','2017-09-11 17:38:56'),(140,'STPLS','YONFMJUFNY',NULL,'Staples','','mcrane@assemblersinc.net','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(141,'OD','FT83B542U7',NULL,'Office Depot','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(142,'CCO','GDYNBY275M',NULL,'Costco','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(143,'SA','AMJF1NIEDA',NULL,'Sports Authority','','','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(144,'OSH','U0A4EWCLQH',NULL,'Orchard Supply','','osh@assemblersinc.net','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(145,'AMZ','AT435HKS3UB',NULL,'Amazon','','amazon@assemblersinc.net','','','2017-06-08 13:37:41','2017-09-11 17:38:56'),(146,'WW','5TYU9IJ3DY',NULL,'Weather Watch','','n.schwartz@assemblersinc.net','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(147,'WAY','MJKYG56OPL',NULL,'Wayfair','','wayfair@assemblersinc.net','','\0','2017-06-08 13:37:41','2017-09-11 17:39:06'),(148,'MFF','KIH48JFTGL',NULL,'Mills Fleet Farm','','','','','2017-06-08 13:37:41','2017-09-11 17:38:56'),(149,'WMRT','8R2OJ291ZP',NULL,'Wal-Mart','','testemail@testyemaily.com',NULL,'','2017-08-09 14:33:01','2017-09-11 17:38:56');
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:49:26
