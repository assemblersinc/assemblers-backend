-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientattributes`
--

DROP TABLE IF EXISTS `clientattributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientattributes` (
  `clientId` int(11) NOT NULL,
  `key` varchar(100) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `index` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`clientId`,`key`),
  CONSTRAINT `attribute_client` FOREIGN KEY (`clientId`) REFERENCES `client` (`clientId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientattributes`
--

LOCK TABLES `clientattributes` WRITE;
/*!40000 ALTER TABLE `clientattributes` DISABLE KEYS */;
INSERT INTO `clientattributes` VALUES (138,'partsLink','http://www.replacementpartspros.com/catalog/#',1,'2017-09-18 14:53:44','2017-09-18 14:56:56'),(138,'reqKeyRec','1',2,'0000-00-00 00:00:00','2017-08-22 14:54:24'),(138,'reqPoNumber','1',1,'0000-00-00 00:00:00','2017-08-22 14:54:24'),(138,'reqWeeklyCount','1',0,'0000-00-00 00:00:00','2017-08-22 14:54:24'),(148,'reqWeeklyCount','1',0,'2017-08-01 18:23:40','2017-08-22 14:54:25'),(149,'partsLink','',1,'2017-09-18 14:54:31','2017-09-18 14:56:56'),(149,'reqInvoiceStamp','1',1,'2017-08-09 14:42:44','2017-08-22 14:54:25');
/*!40000 ALTER TABLE `clientattributes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:49:20
