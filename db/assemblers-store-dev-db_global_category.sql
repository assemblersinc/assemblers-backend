-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `global_category`
--

DROP TABLE IF EXISTS `global_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `global_category` (
  `cat_id` double NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(50) NOT NULL,
  `bt` int(5) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `global_category`
--

LOCK TABLES `global_category` WRITE;
/*!40000 ALTER TABLE `global_category` DISABLE KEYS */;
INSERT INTO `global_category` VALUES (2,'Basketball Goal',0),(3,'Bike - Boys',0),(4,'Bike - Girls',0),(5,'Bike - Mens',0),(6,'Bike - Womens',0),(7,'Fitness - Boxing Stand',0),(8,'Fitness - Exer Bike/Ski Mach',0),(9,'Fitness - Freeweight Stand',0),(10,'Fitness - Home Gyms',0),(11,'Fitness - Misc.',0),(12,'Fitness - Steppers/Ellipticals',0),(13,'Fitness - Treadmills',0),(14,'Fitness - Weight Benches',0),(15,'Game Tables',0),(16,'Game Tables (Accessory)',0),(17,'Grills - Charcoal',40),(18,'Grills - Gas',40),(19,'Grills - Smokers',40),(20,'Hunting Stands/Blinds',0),(21,'Patio - Furniture',20),(22,'Playcenter',0),(23,'Cooking - Misc',0),(24,'Gun Safes / Cabinets',0),(25,'Patio - Gazebo',0),(26,'Repair',15),(27,'Trampoline Ladder',0),(28,'Trampoline',0),(29,'Scooters',0),(30,'Motorized Goods',0),(31,'Trailers',0),(32,'Pet',0),(33,'Office Package',0),(34,'Bike - Charity',0),(35,'Bike - Special',0),(36,'Bike - Repair',0),(37,'Wheel Barrow',15),(38,'Bike - Repair (No Serial #)',0),(39,'Safe',0),(40,'Grills - Holiday',40),(41,'Riders (HD Only)',20),(42,'N/A',0),(43,'Attachment / Implementation',0),(44,'Garden Carts',0),(45,'Snowthrowers',0),(46,'Edgers',0),(47,'Front Tiller',0),(48,'Riders',20),(49,'Chippers',0),(50,'Rear Tiller',15),(51,'Log Splitters',0),(52,'John Deere Baggers',0),(53,'Repair (HD Only)',15),(54,'Dump Carts',30),(55,'Tool Box',0),(56,'Rocking Chair',0),(57,'Deckbox',0),(58,'RTA',0),(59,'Patio - Heat',0),(60,'Cooler',0),(61,'Hose Reel Cart',0),(62,'Push Mower',0);
/*!40000 ALTER TABLE `global_category` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:48:30
