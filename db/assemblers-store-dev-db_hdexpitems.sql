-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hdexpitems`
--

DROP TABLE IF EXISTS `hdexpitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hdexpitems` (
  `itemid` int(11) NOT NULL AUTO_INCREMENT,
  `itemname` varchar(45) NOT NULL,
  `macola_code` varchar(50) NOT NULL,
  `price` varchar(8) NOT NULL,
  `sortorder` int(11) NOT NULL,
  `active` varchar(3) NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=InnoDB AUTO_INCREMENT=163 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hdexpitems`
--

LOCK TABLES `hdexpitems` WRITE;
/*!40000 ALTER TABLE `hdexpitems` DISABLE KEYS */;
INSERT INTO `hdexpitems` VALUES (2,'Hose Reel - Metal / Plastic','Hose Reel-Metal/Plastic (HDE)','18',1,'yes'),(3,'Spreader - Push','Spreader-Push (HDE)','12',3,'yes'),(4,'ATV - 2 Wheel','ATV-2 Wheel (HDE)','60',5,'yes'),(5,'ATV - 4 Wheel','ATV-4 Wheel (HDE)','90',7,'yes'),(6,'Blower / Trimmer','Blower/Trimmer (HDE)','12',9,'yes'),(7,'Chainsaw','Chainsaw (HDE)','12',11,'yes'),(8,'Push Mower - Full Assemble','Push Mower-Full Assemble (HDE)','12',13,'yes'),(9,'Push Mower - Trimmer Mower (Out of Box)','Push Mower-Trim Out of Box HDE','5',15,'yes'),(10,'Reel Mower','Reel Mower (HDE)','18',17,'yes'),(11,'Fryer / Electric / Portable','Fryer/Electric/Portable (HDE)','12',19,'yes'),(12,'Grill - Oversize - Must Obtain Approval','Grill-Oversize (HDE)','40',21,'yes'),(13,'Cub Cadet Vacuum / Chipper','Cub Cadet Vacuum/Chipper (HDE)','12',23,'yes'),(14,'Riding Mower Accessory','Riding Mower Accessory (HDE)','15',25,'yes'),(16,'Canopy','Canopy (HDE)','30',29,'yes'),(17,'Canopy Swing','Canopy Swing (HDE)','40',31,'yes'),(18,'Garden / Dump Cart','Garden Cart (HDE)','25',33,'yes'),(19,'Gazebo','Gazebo (HDE)','60',35,'yes'),(20,'Gazebo - Large','Gazebo-Large (HDE)','125',37,'yes'),(21,'Gazebo Swing','Gazebo Swing (HDE)','145',39,'yes'),(22,'Patio Bar','Patio Bar (HDE)','25',41,'yes'),(23,'Patio Chair / Swivel Chair','Patio Chair/Swivel Chair (HDE)','10',43,'yes'),(24,'Patio Chaise','Patio Chaise (HDE)','12',45,'yes'),(25,'Patio Couch / Sola','Patio Couch/Sofa (HDE)','12',47,'yes'),(26,'Patio Glider','Patio Glider (HDE)','45',49,'yes'),(27,'Patio Love Seat','Patio Love Seat (HDE)','12',51,'yes'),(28,'Patio Ottoman','Patio Ottoman (HDE)','10',53,'yes'),(29,'Patio Park Bench','Patio Park Bench (HDE)','20',55,'yes'),(30,'Patio Sectional','Patio Sectional (HDE)','40',57,'yes'),(31,'Patio Side Table','Patio Side Table (HDE)','10',59,'yes'),(32,'Patio Table','Patio Table (HDE)','16',61,'yes'),(33,'Patio 3 Piece Bistro','Patio 3 Piece Bistro (HDE)','20',63,'yes'),(34,'Wood Patio Bench','Wood Patio Bench (HDE)','20',65,'yes'),(35,'Wood Patio Chair','Wood Patio Chair (HDE)','15',67,'yes'),(36,'Wood Patio Chaise','Wood Patio Chaise (HDE)','20',69,'yes'),(37,'Wood Patio Glider','Wood Patio Glider (HDE)','30',71,'yes'),(38,'Wood Patio Side Table','Wood Patio Side Table (HDE)','20',73,'yes'),(39,'Wood Patio Table','Wood Patio Table (HDE)','30',75,'yes'),(40,'Wood Picnic Table','Wood Picnic Table (HDE)','20',77,'yes'),(41,'Wood Rocker Full Assemble','Wood Rocker Full Assemble(HDE)','20',79,'no'),(42,'Wood Rocker Partial Assemble','Wood Rocker Part Assemble(HDE)','8',80,'yes'),(49,'Fan - Commercial / Portable','Fan-Commercial/Portable (HDE)','18',14,'yes'),(50,'Fire Pit (Chimnea)','Fire Pit (Chimnea) (HDE)','25',16,'yes'),(51,'Fireplace Accessory','Fireplace Accessory (HDE)','12',18,'yes'),(52,'Fireplace Insert / Mantle Combo','Firepl Ins/Mantle Combo (HDE)','60',20,'yes'),(53,'Fireplace Mantle','Fireplace Mantle (HDE)','40',22,'yes'),(54,'Heater - Commercial / Portable','Heater-Commercial/Port (HDE)','18',24,'yes'),(55,'Heater - Patio / Outdoor','Heater-Patio/Outdoor (HDE)','25',26,'yes'),(56,'Stove - Electric / Gas / Wood','Stove-Electric/Gas/Wood (HDE)','25',28,'yes'),(59,'Level 1 Repair','Level 1 Repair (HDE)','6',34,'yes'),(60,'Level 2 Repair','Level 2 Repair (HDE)','12',36,'yes'),(61,'Level 3 Repair','Level 3 Repair (HDE)','25',38,'yes'),(62,'Air Compressor','Air Compressor (HDE)','12',40,'yes'),(63,'Bench Top Tool - Grinder / Sander','Bench Top Tool-Grinder/Sand(HDE)','12',42,'yes'),(64,'Bench Top Tool - Router Table','Bench Top Tool-Router Table(HDE)','28',44,'yes'),(65,'Bench Top Tool - Saw / Drill Press / Band Saw','Bench Top Tool-S/DP/B (HDE)','18',46,'yes'),(66,'Cement Mixer','Cement Mixer (HDE)','45',48,'yes'),(67,'Generator','Generator (HDE)','12',50,'yes'),(68,'Pressure Washer','Pressure Washer (HDE)','12',52,'yes'),(72,'Tool Chest','Tool Chest (HDE)','20',60,'yes'),(73,'Work Bench (Wood and Metal)','Work Bench (HDE)','55',62,'yes'),(74,'Deck Box','Deck Box (HDE)','12',64,'yes'),(75,'Plastic Shed - Horizontal / Flip Lid','Plastic Shed-Hor/Flip Lid(HDE)','18',66,'yes'),(76,'Plastic Shed - Horizontal / Slide Lid','Plastic Shed-Hor/Slide Lid(HDE','55',68,'yes'),(77,'Plastic Shed - Vertical w / Doors','Plastic Shed-Vert w/ Door(HDE)','25',70,'yes'),(79,'Arbor','Arbor (HDE)','38',74,'yes'),(80,'Dog Kennel','Dog Kennel (HDE)','55',76,'yes'),(82,'Setup and Install Scanner','Setup and Install Scanner (HDE)','0',0,'yes'),(83,'Christmas - Large Trees','Christmas - Large Trees (HDE)','12',0,'yes'),(84,'Christmas - Yard Decor','Christmas - Yard Decor (HDE)','12',0,'yes'),(100,'ALU - 10X12','ALU-10X12 (HDE)','650',100,'yes'),(101,'ALU - 10X13','ALU-10X13 (HDE)','650',101,'yes'),(102,'ALU - 10X14','ALU-10X14 (HDE)','650',102,'yes'),(103,'ALU - 10X17','ALU-10X17 (HDE)','700',103,'yes'),(104,'ALU - 10X6','ALU-10X6 (HDE)','575',104,'yes'),(105,'ALU - 10X8','ALU-10X8 (HDE)','600',105,'yes'),(106,'ALU - 10X9','ALU-10X9 (HDE)','600',106,'yes'),(107,'ALU - 6x5','ALU-6x5 (HDE)','400',107,'yes'),(108,'ALU - 8x6','ALU-8x6 (HDE)','550',108,'yes'),(109,'ALU - 8X9','ALU-8X9 (HDE)','550',109,'yes'),(110,'ALU - Yardsaver','ALU-Yardsaver (HDE)','275',110,'yes'),(111,'Vinyl - 10x10','Vinyl-10x10 (HDE)','425',111,'yes'),(112,'Vinyl - 10x12','Vinyl - 10x12 (HDE)','450',112,'yes'),(113,'Vinyl - 10x8','Vinyl-10x8 (HDE)','400',113,'yes'),(114,'Vinyl - 6x5','Vinyl-6x5 (HDE)','300',114,'yes'),(115,'Vinyl - 6x6','Vinyl-6x6 (HDE)','350',115,'yes'),(116,'Vinyl - 8x6','Vinyl - 8x6 (HDE)','375',116,'yes'),(117,'Vinyl - 8x8','Vinyl-8x8 (HDE)','375',117,'yes'),(118,'Vinyl - Yardsaver','Vinyl-Yardsaver (HDE)','275',118,'yes'),(119,'Wood-10x10','Wood-10x10 (HDE)','700',119,'yes'),(120,'Wood - 10x12','Wood - 10x12 (HDE)','700',120,'yes'),(121,'Wood - 10x14','Wood - 10x14 (HDE)','750',121,'yes'),(122,'Wood - 10x16','Wood - 10x16 (HDE)','800',122,'yes'),(123,'Wood - 10x18','Wood - 10x18 (HDE)','800',123,'yes'),(124,'Wood - 10x8','Wood - 10x8 (HDE)','675',124,'yes'),(125,'Wood - 8x12','Wood-8x12 (HDE)','650',125,'yes'),(126,'Wood - 8x14','Wood - 8x14 (HDE)','700',126,'yes'),(127,'Wood - 8x16','Wood - 8x16 (HDE)','700',127,'yes'),(128,'Wood - 8x8','Wood-8x8 (HDE)','550',128,'yes'),(129,'Anchor Kit - Concrete','Anchor Kit-Concrete (HDE)','75',129,'yes'),(130,'Anchor Kit - Ground','Anchor Kit-Ground (HDE)','50',130,'yes'),(131,'Anchor Kit Concrete','Anchor Kit Concrete (HDE)','75',131,'yes'),(132,'Base - Large','Base-Large (HDE)','100',132,'yes'),(133,'Base - Small','Base-Small (HDE)','75',133,'yes'),(134,'Extension - 3.5','Extension-3.5 (HDE)','75',134,'yes'),(135,'Extension - 4','Extension-4 (HDE)','100',135,'yes'),(136,'Extension - 7','Extension-7 (HDE)','125',136,'yes'),(137,'Floor Kit - Large','Floor Kit-Large (HDE)','200',137,'yes'),(138,'Floor Kit - Small','Floor Kit-Small (HDE)','150',138,'yes'),(139,'Flower Box','Flower Box (HDE)','35',139,'yes'),(140,'Garage Door','Garage Door (HDE)','100',140,'yes'),(141,'Roll Up Door','Roll Up Door (HDE)','100',141,'yes'),(142,'Shingle Building','Shingle Building (HDE)','125',142,'yes'),(143,'Shutter Kit','Shutter Kit (HDE)','35',143,'yes'),(144,'Skylight Kit','Skylight Kit (HDE)','35',144,'yes'),(145,'Window - Round','Window - Round (HDE)','50',145,'yes'),(146,'Window - Square','Window - Square (HDE)','50',146,'yes'),(147,'Window Kit','Window Kit (HDE)','35',147,'yes'),(148,'Solar Umbrella','Solar Umbrella (HDE)','35',0,'yes'),(149,'Hammock Stand','Hammock Stand (HDE)','20',0,'yes'),(150,'Recycle Bin','Recycle Bin (HDE)','20',0,'yes'),(151,'Swing - 2 Person','Swing-2 Person (HDE)','35',0,'yes'),(152,'Swing - 3 Person','Swing-3 Person (HDE)','45',0,'yes'),(153,'Metal Cooler','Metal Cooler (HDE)','15',33,'yes'),(154,'Go-Kart','Go-Kart (HDE)','25',40,'yes'),(155,'Patio Table (Umbrella)','Patio Table (Umbrella) (HDE)','15',61,'yes'),(156,'Halloween - Yard Decor','Halloween-Yard Decor (HDE)','12',0,'yes'),(158,'Chicken Coup','Chicken Coup (HDE)','80',0,'yes'),(159,'3-Tier Wine Cart','3-Tier Wine Cart (HDE)','20',0,'yes'),(160,'Tool Chest - Single','Tool Chest-Single (HDE)','15',0,'yes'),(161,'Tool Chest - Combo','Tool Chest-Combo (HDE)','30',0,'yes'),(162,'Cooler','Cooler (HDE)','15',103,'yes');
/*!40000 ALTER TABLE `hdexpitems` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:49:23
