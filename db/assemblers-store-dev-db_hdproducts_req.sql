-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hdproducts_req`
--

DROP TABLE IF EXISTS `hdproducts_req`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hdproducts_req` (
  `reqid` int(11) NOT NULL AUTO_INCREMENT,
  `prodsku` varchar(45) NOT NULL,
  `produpc` varchar(45) NOT NULL,
  `proddesc` varchar(75) NOT NULL,
  `prodman` varchar(50) NOT NULL,
  `prodprice` varchar(10) NOT NULL,
  `prodcat` int(11) NOT NULL,
  `aisku` varchar(10) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `reqby` varchar(5) NOT NULL,
  `status` varchar(15) NOT NULL DEFAULT 'Entered',
  `statuschange` varchar(10) NOT NULL,
  PRIMARY KEY (`reqid`),
  KEY `prodsku` (`prodsku`),
  KEY `aisku` (`aisku`)
) ENGINE=InnoDB AUTO_INCREMENT=2449 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hdproducts_req`
--

LOCK TABLES `hdproducts_req` WRITE;
/*!40000 ALTER TABLE `hdproducts_req` DISABLE KEYS */;
INSERT INTO `hdproducts_req` VALUES (1825,'1000096415','722571011574','Worx 4 cu ft Aerocart','','',54,'18','2017-02-23 16:17:39','18075','Checked',''),(1834,'588756','090489238384','Wood Picnic Table 28 x 72','','',21,'40','2017-02-24 02:14:28','10451','Checked',''),(1849,'824220','722571010683','Gorilla Carts 800lbs Steel Utility Cart','','',54,'18','2017-02-24 18:29:43','10911','Checked',''),(1871,'1000012638','98071000012638','Gardensun 40000BTU Pyramid Propane Gas Patio Heater','','',59,'55','2017-02-27 18:47:17','20263','Checked',''),(1872,'560394','9807560394','Gardensun 41000BTU Propane Patio Heater','','',59,'55','2017-02-27 18:48:20','20263','Checked',''),(1883,'464963','9807464963','Harper 700lbs Convertible Hand Truck','','',54,'18','2017-02-28 15:29:30','20263','Checked',''),(1893,'1001571701','722571010881','Gorilla Carts Rolling Garden Scooter','','',54,'18','2017-02-28 17:23:21','10451','Checked',''),(1894,'104703','026479030447','Scotts 14in 5 Blade Reel Mower','','',62,'8','2017-02-28 17:24:26','10451','Checked',''),(1898,'1002067574','848681051719','Hampton Bay Statesville 3 Piece Bistro Set','','',21,'33','2017-02-28 22:26:22','10451','Checked',''),(1899,'1000005206','98071000005206','Husky 16in 2 Shelf Utility Cart','','',54,'18','2017-02-28 22:30:21','10451','Checked',''),(1904,'1000051172','028776102421','Hampton Bay Spring Haven Wicker Patio Lounger','','',21,'23','2017-03-01 16:13:12','12379','Checked',''),(1906,'1001243467','8936058510128','Milwaukee 46in 8 Drawer Storage Cabinet','','',55,'161','2017-03-01 16:21:08','12379','Checked',''),(1908,'1001681738','8936058510289','Husky 62in Work Table','','',55,'73','2017-03-01 16:31:11','12379','Checked',''),(1909,'1001399836','98071001399836','Milwaukee 46in 16 Drawer Tool Chest','','',55,'161','2017-03-01 19:28:38','20263','Checked',''),(1912,'1002109682','98071002109682','Hampton Bay 4 Wheel Metal Hose Cart','','',61,'2','2017-03-01 20:06:47','18075','Checked',''),(1932,'1002109682','044365024707','Hampton Bay 4 Wheel Hose Reel Cart','','',61,'2','2017-03-02 20:17:47','18074','Checked',''),(1937,'1001802049','715117280503','Heatwave 28in Steel Outdoor Fireplace','','',59,'50','2017-03-04 17:44:50','12379','Checked',''),(1940,'824154','9807824154','Gorilla Carts 600 lb. Poly Garden Dump Cart','','',54,'18','2017-03-06 12:35:04','18084','Checked',''),(1941,'824220','9807824220','Gorilla Carts 800 lb. Steel Utility Cart','','',54,'18','2017-03-06 12:35:46','18084','Checked',''),(1947,'1002085956','6944937601630','Cauldron Cast Iron Fire Pit 34 in. ','','',59,'50','2017-03-06 18:36:51','18075','Checked',''),(1956,'1002078743','046396014658','Ryobi 20in Battery Push Mower','','',62,'9','2017-03-07 13:39:03','15806','Checked',''),(1964,'300187156','0726583920947','Bradley Slat Midnight Patio Rocking Chair','','',56,'26','2017-03-07 19:00:05','20606','Checked',''),(1973,'209238','9807209238','Milwaukee 800lbs Appliance Hand Truck','','',54,'18','2017-03-09 17:49:01','20263','Checked',''),(1974,'290206','9807290206','Milwaukee 1000 lbs Capacity Dual Handle Hand Truck','','',54,'18','2017-03-09 17:50:35','20263','Checked',''),(1998,'1000051192','98071000051192','Hampton Bay Spring Haven Wicker Patio Swivel','','',21,'23','2017-03-13 14:44:22','20263','Checked',''),(2011,'1001665192','841057126868','80 Qt 4 Wheeled Hammered Copper Cooler','','',60,'160','2017-03-15 18:39:22','10458','Checked',''),(2020,'1001754323','8936058510371','Husky 60in 10 Drawer Mobile Workbench','','',55,'161','2017-03-16 20:41:34','Tavis','Checked',''),(2044,'1001665192','0841057126868','80 QT 4 Wheeled Hammered Copper Cooler','','',60,'160','2017-03-23 17:19:51','20263','Checked',''),(2052,'1001823283','6944937601579','Hampton Bay 30in Outdoor Gas Fire Pit','','',59,'50','2017-03-27 17:14:04','12008','Checked',''),(2058,'1001820646','98071001820646','36in Galvanized Round Fire Ring','','',59,'50','2017-03-28 21:08:41','20763','Checked',''),(2059,'1001509635','6930849600045','Husky Steel Tall Garage Cabinet','','',55,'160','2017-03-28 22:28:32','14282','Checked',''),(2084,'1000051190','0028776102438','Hampton Bay Spring Haven Wicker Patio Loveseat','','',21,'27','2017-04-05 23:41:34','17543','Checked',''),(2085,'1000051172','0028776102421','Hampton Bay Spring Haven Wicker Patio Lounger','','',21,'23','2017-04-05 23:58:15','17543','Checked',''),(2088,'1002078715','0848681051726','Hampton Bay Elmont 3-Piece Patio','','',21,'33','2017-04-06 18:55:19','20721','Checked',''),(2100,'1000013394','6944937600664','Hampton Bay Quadripod 26in Round Fire Pit','','',59,'50','2017-04-13 22:06:46','Home ','Checked',''),(2112,'588756','0090489238384','Wood Picnic Table 28 x 72','','',21,'40','2017-04-18 14:49:13','20999','Checked',''),(2116,'1001665192','98071001665192','80 QT 4 Wheeled Hammered Copper Cooler','','',60,'160','2017-04-19 00:28:05','20263','Checked',''),(2122,'553145','0644376111160','Polar Trailer 15 cu ft 1200 Trailer','','',54,'14','2017-04-20 14:41:00','18417','Checked',''),(2135,'1001399836','8936058519114','Milwaukee 46in 16-Drawer Tool Chest','','',55,'161','2017-04-25 17:04:36','21209','Checked',''),(2148,'1002067471','0644794338286','Black Wood Outdoor Rocking Chair','','',56,'41','2017-04-27 19:44:19','16478','Checked',''),(2155,'1002227127','852004626977','Husky 60in 10 Drawer Mobile Workbench','','',55,'161','2017-05-01 14:35:55','20970','Checked',''),(2173,'716504910010','9807342752','AllfitHD 12.5 cu ft 1000lbs Capacity Poly Dump Cart','','',54,'18','2017-05-09 11:52:33','21357','Checked',''),(2176,'1000047958','0715117259158','Hampton Bay Crossfire 29.50 Fire Pit','','',59,'50','2017-05-11 13:00:08','18417','Checked',''),(2177,'1001802046','6944937601562','Hampton Bay 24in Ashmore Round Steel Fire Pit','','',59,'50','2017-05-11 14:06:13','18417','Checked',''),(2178,'1000048060','6944937600985','Hampton Bay Woodspire 30in Square Slate Steel Fire Pit','','',59,'50','2017-05-11 14:27:41','18417','Checked',''),(2192,'726583920145','98071002153539','Bradley Black Slat Patio Rocking Chair','','',56,'26','2017-05-16 21:39:07','91364','Checked',''),(2202,'1000096415','0845534012118','Worx 4 cu ft Aerocart','','',54,'18','2017-05-18 19:17:26','20721','Checked',''),(2210,'1002067475','0644794138282','Glossy White Wood Outdoor Rocking Chair','','',56,'41','2017-05-22 17:59:22','20293','Checked',''),(2235,'1001786557','0873388008575','Husky 52in 15 Drawer Tool Chest','','',55,'161','2017-05-25 15:46:35','Chris','Checked',''),(2263,'1000051190','028776102438','Hampton Bay Spring Haven Wicker Patio Loveseat','','',21,'27','2017-06-01 19:11:21','20020','Checked',''),(2276,'1000051191','0028776102452','Hampton Bay Spring Haven Wicker Patio Ottoman','','',21,'28','2017-06-09 18:22:52','todd','Checked',''),(2277,'1000051192','0028776102445','Hampton Bay Spring Haven Wicker Patio Swivel','','',21,'23','2017-06-09 18:26:25','todd','Checked',''),(2280,'1000051192','0022176102425','Hampton Bay Spring Haven Wicker Patio Swivel','','',21,'23','2017-06-09 19:26:51','','Checked',''),(2300,'1002087039','0814603013417','Hampton Nantucket Round Metal Table','','',21,'32','2017-06-20 16:47:35','18417','Checked',''),(2320,'1002291686','0873388009152','Husky 36in 12 Drawer Tool Chest','','',55,'161','2017-06-23 15:05:26','21953','Checked',''),(2324,'404997','0786102004510','Honda 21in Self Propelled Mower','','',30,'12','2017-06-23 20:07:56','21992','Checked',''),(2372,'1001754323','1001754323','Husky 60in 10 Drawer Mobile Workbench','','',55,'161','2017-07-03 16:54:01','17463','Checked',''),(2373,'873388009176','98071001754323','Milwaukee 52in 11 Drawer Mobile Workcenter','','',55,'161','2017-07-03 16:56:24','17463','Checked',''),(2374,'873388008575','873388008575','Husky 52in 15 Drawer Tool Chest','','',55,'161','2017-07-03 16:58:54','17463','Checked',''),(2379,'1000051849','98071000051849','Husky 21in 4 Drawer Tool Cabinet','','',55,'160','2017-07-03 21:18:21','12379','Checked',''),(2381,'1000051172','0067000002113','Hampton Bay Spring Haven Wicker Patio Lounger','','',21,'23','2017-07-04 14:25:58','','Checked',''),(2382,'1002062621','044376287726','Husky 39in 5 Drawer Mobile Workbench','','',55,'73','2017-07-04 18:03:49','18383','Checked',''),(2388,'1002109665','044365024622','Hampton Bay 2- wheel hose cart','','',61,'2','2017-07-05 20:52:37','18383','Checked',''),(2399,'1002060495','8935199103602','Hampton Bay Adelaide Bench','','',21,'29','2017-07-10 17:01:10','12317','Checked',''),(2410,'1002085956','98071002085956','34in Cauldron Cast Iron Fire Pit','','',59,'50','2017-07-11 21:35:42','12379','Checked',''),(2427,'1001810259','872076020875','Hampton Bay Tipton 34in Fire Bit','','',59,'50','2017-07-19 18:45:43','12317','Checked',''),(2428,'1001802023','816404014211','37in Clay KD Chiminea','','',59,'50','2017-07-19 18:47:13','12317','Checked',''),(2448,'1000017605','8936058510692','Husky 52in 18 Drawer Tool Chest','','',55,'161','2017-07-25 22:34:57','18383','Checked','');
/*!40000 ALTER TABLE `hdproducts_req` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:49:00
