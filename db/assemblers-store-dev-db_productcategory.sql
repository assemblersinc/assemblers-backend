-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `productcategory`
--

DROP TABLE IF EXISTS `productcategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productcategory` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `active` bit(1) DEFAULT b'1',
  `bt` int(11) DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`categoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productcategory`
--

LOCK TABLES `productcategory` WRITE;
/*!40000 ALTER TABLE `productcategory` DISABLE KEYS */;
INSERT INTO `productcategory` VALUES (2,'Basketball Goal','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(3,'Bike - Boys','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(4,'Bike - Girls','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(5,'Bike - Mens','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(6,'Bike - Womens','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(7,'Fitness - Boxing Stand','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(8,'Fitness - Exer Bike/Ski Mach','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(9,'Fitness - Freeweight Stand','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(10,'Fitness - Home Gyms','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(11,'Fitness - Misc.','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(12,'Fitness - Steppers/Ellipticals','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(13,'Fitness - Treadmills','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(14,'Fitness - Weight Benches','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(15,'Game Tables','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(16,'Game Tables (Accessory)','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(17,'Grills - Charcoal','',40,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(18,'Grills - Gas','',40,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(19,'Grills - Smokers','',40,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(20,'Hunting Stands/Blinds','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(21,'Patio - Furniture','',20,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(22,'Playcenter','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(23,'Cooking - Misc','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(24,'Gun Safes / Cabinets','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(25,'Patio - Gazebo','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(26,'Repair','',15,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(27,'Trampoline Ladder','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(28,'Trampoline','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(29,'Scooters','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(30,'Motorized Goods','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(31,'Trailers','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(32,'Pet','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(33,'Office Package','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(34,'Bike - Charity','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(35,'Bike - Special','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(36,'Bike - Repair','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(37,'Wheel Barrow','',15,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(38,'Bike - Repair (No Serial #)','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(39,'Safe','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(40,'Grills - Holiday','',40,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(41,'Riders (HD Only)','',20,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(42,'N/A','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(43,'Attachment / Implementation','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(44,'Garden Carts','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(45,'Snowthrowers','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(46,'Edgers','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(47,'Front Tiller','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(48,'Riders','',20,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(49,'Chippers','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(50,'Rear Tiller','',15,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(51,'Log Splitters','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(52,'John Deere Baggers','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(53,'Repair (HD Only)','',15,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(54,'Dump Carts','',30,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(55,'Tool Box','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(56,'Rocking Chair','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(57,'Deckbox','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(58,'RTA','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55'),(59,'Patio - Heat','',0,'2017-07-15 02:18:55','2017-07-15 02:18:55');
/*!40000 ALTER TABLE `productcategory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:49:08
