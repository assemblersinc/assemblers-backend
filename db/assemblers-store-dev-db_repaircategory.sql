-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `repaircategory`
--

DROP TABLE IF EXISTS `repaircategory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repaircategory` (
  `repairCategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`repairCategoryId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repaircategory`
--

LOCK TABLES `repaircategory` WRITE;
/*!40000 ALTER TABLE `repaircategory` DISABLE KEYS */;
INSERT INTO `repaircategory` VALUES (1,'Bike Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(2,'Bike Repair - No Serial Required','2017-07-10 00:27:52','2017-07-10 00:27:52'),(3,'Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(4,'Fitness Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(5,'Home Gym Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(6,'CAP','2017-07-10 00:27:52','2017-07-10 00:27:52'),(7,'Recumbent Bike Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(8,'Treadmill Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(9,'Elliptical Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(10,'Basketball Goal Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(11,'Grill Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(12,'Misc Repair','2017-07-10 00:27:52','2017-07-10 00:27:52'),(13,'test category','2017-08-20 19:03:11','2017-08-20 19:03:11');
/*!40000 ALTER TABLE `repaircategory` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:48:23
