-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: assemblers-db.mysql.database.azure.com    Database: assemblers-store-dev-db
-- ------------------------------------------------------
-- Server version	5.6.26.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping events for database 'assemblers-store-dev-db'
--

--
-- Dumping routines for database 'assemblers-store-dev-db'
--
/*!50003 DROP PROCEDURE IF EXISTS `findNearByStore` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `findNearByStore`(IN lat_input float, IN lon_input float, IN minLat Float, IN minLon float, 
IN maxLat float, IN maxLon float, IN R int, IN rad float)
BEGIN
	  Select c.*,JSON_ARRAY(JSON_OBJECT('clientId',location.clientId,
	'locationId', location.locationId 
    ,'Location', location.location
    ,'phoneNumber',location.phoneNumber
    ,'storeNumber',location.storeNumber
    ,'areaId',location.areaId
    ,'address',location.address
    ,'city',location.city
    ,'state',location.state
    ,'zip',location.zip
    ,'district',location.district
    ,'lat',location.lat
    ,'lon',location.lon
    ,'timezone',location.timezone
    ,'active',location.active
    ,'createdAt',location.createdAt
    ,'updatedAt',location.updatedAt)) location,  acos(sin(lat_input)*sin(radians(location.lat)) + 
      cos(lat_input)*cos(radians(location.lat))*cos(radians(location.lon)-lon_input)) * R As DistMiles 
      From location 
      INNER JOIN Client c 
         ON c.clientId = location.clientId 
      Where location.lat Between minLat And maxLat And location.lon 
      Between minLon And maxLon AND acos(sin(lat_input)*sin(radians(location.lat)) +
      cos(lat_input)*cos(radians(location.lat))*cos(radians(location.lon)-lon_input)) * R < rad Order by DistMiles;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getLastChatConversationByReceiverId` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `getLastChatConversationByReceiverId`(IN userId int,IN offst INT,IN lim int)
BEGIN
	
    SELECT c.*, 
	userId chatSenderId, 
    CASE WHEN userId = sender.userId THEN receiver.userId ELSE sender.userId END chatReceiverId, 
	JSON_OBJECT('senderId', sender.userId, 'firstName', sender.firstName, 'lastName', sender.lastName) sender,
	JSON_OBJECT('receiverId', receiver.userId, 'firstName', receiver.firstName, 'lastName', receiver.lastName) receiver 
	FROM .conversation c
	JOIN (SELECT max(conversationId) conversationId, max(createdAt) updatedAt , concat('chat_', senderId + receiverId,'_',  senderId + receiverId)
			from conversation 
			WHERE senderId in (userId) or receiverId in (userId)
			GROUP BY  concat('chat_', senderId + receiverId,'_',  senderId + receiverId)
  ) lc
	ON lc.conversationId = c.conversationId
	JOIN .`user` sender
	  ON c.senderId = sender.userId
	JOIN .`user` receiver
	  ON c.receiverId = receiver.userId
	WHERE (c.receiverId = userId OR c.senderId = userId)
    -- and deleted = 0
	ORDER BY c.createdAt DESC
    LIMIT offst, lim;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getLastUserTimesheet` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `getLastUserTimesheet`(IN userId int)
BEGIN

  SELECT s.*, abs(TIMESTAMPDIFF(MINUTE, CURDATE(), s.timeIn)) minutes
  FROM timesheet s 
  INNER JOIN (select timeId 
  FROM timesheet s
  WHERE s.userId = userId 
  ORDER BY updatedAt DESC LIMIT 1) a
  ON a.timeId = s.timeId AND s.timeOut IS NULL;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `getTimesheetsPerUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `getTimesheetsPerUser`(IN usrId int)
BEGIN

    SET @dates = DATE_SUB(now(), INTERVAL 30 day) and now();

    SELECT CONCAT( MONTHNAME(DATE_ADD(timeIn, INTERVAL - (WEEKDAY(timeIn) +1) DAY))
	, ' ',  DAY(DATE_ADD(timeIn, INTERVAL - (WEEKDAY(timeIn) +1) DAY))
	, ' - ',  MONTHNAME(DATE_ADD(timeIn, INTERVAL + (5 - WEEKDAY(timeIn) ) DAY))
	, ' ',  DAY(DATE_ADD(timeIn, INTERVAL + (5 - WEEKDAY(timeIn)) DAY))) as weekdate
	,YEARWEEK(timeIn, 0) as weekId, 
    ts.*, l.location, l.address, l.city, l.state, l.zip
    FROM timesheet ts
    JOIN `location` l
      on l.locationId = ts.locationId
    WHERE userId =  usrId
    and timeIn between @dates and now()
    ORDER BY timeIn DESC;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_areasbyregions` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_areasbyregions`(in _curRegion VARCHAR(2))
begin
	SELECT areaId, name, regionId FROM geoarea WHERE regionId=_curRegion;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_clienttenchars` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_clienttenchars`(in _curClientTenChars VARCHAR(10))
begin
	SELECT clientId, storeId FROM client WHERE storeId=_curClientTenChars;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_findareas_1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_findareas_1`(in _curRegion VARCHAR(2), in _curActive INTEGER, in _curOrderBy VARCHAR(1000))
begin
    /* (4,1,'name ASC') */
	set @Q='SELECT areaId, name, regionId FROM geoarea WHERE ';
	set @curRegion='regionId=';
	set @_curRegion=_curRegion;
	set @andOne= ' AND ';
	set @curActive='active=';
	set @_curActive=_curActive;
	set @orderBy=' ORDER BY ';
	set @_orderBy=_curOrderBy;
	
	set @finQ=CONCAT(@Q,@curRegion,@_curRegion,@andOne,@curActive,@_curActive,@orderBy,_curOrderBy);
	
	PREPARE stmt FROM @finQ;
    EXECUTE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_findareas_2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_findareas_2`(in _curRegion VARCHAR(2), in _curActive INTEGER, in _curMgrs VARCHAR(500),  in _curOrderBy VARCHAR(1000))
begin
	/*(2,1,'(managerId = 712 OR managerSecondId = 712 OR managerThirdId = 712 OR opsManagerId = 712)','name ASC') */
	set @Q='SELECT areaId, name, regionId, managerId, managerSecondId, managerThirdId, opsManagerId FROM geoarea WHERE ';
	set @curRegion='regionId=';
	set @_curRegion=_curRegion;
	set @andOne= ' AND ';
	set @curActive='active=';
	set @_curActive=_curActive;
	set @andTwo= ' AND ';
	/* set @curMgrs='curMgrs='; I send the whole OR Mgrs statement here */
	set @_curMgrs=_curMgrs;	
	set @orderBy=' ORDER BY ';
	set @_orderBy=_curOrderBy;
	
	set @finQ=CONCAT(@Q,@curRegion,@_curRegion,@andOne,@curActive,@_curActive,@andTwo,@_curMgrs,@orderBy,_curOrderBy);
	
	PREPARE stmt FROM @finQ;
    EXECUTE stmt;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_findclients` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_findclients`(in _curActive INTEGER, in _curOrderBy VARCHAR(1000))
begin
	set @Q='SELECT clientId, code, storeId, name, phoneNumber, email FROM client WHERE ';
	set @curActive='active=';
	set @_curActive=_curActive;
	set @orderBy=' ORDER BY ';
	set @_orderBy=_curOrderBy;
	
	set @finQ=CONCAT(@Q,@curActive,@_curActive,@orderBy,_curOrderBy);
	
	PREPARE stmt FROM @finQ;
    EXECUTE stmt; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_finddistricts` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_finddistricts`(in _whereClauseOnly VARCHAR(200), in _curOrderBy VARCHAR(1000))
begin
	set @Q='SELECT DISTINCT district FROM location WHERE ';
	set @_curWhereClauseOnly=_whereClauseOnly;	
	set @orderBy=' ORDER BY ';
	set @_orderBy=_curOrderBy;
	
	set @finQ=CONCAT(@Q,@_curWhereClauseOnly,@orderBy,_curOrderBy);
	
	PREPARE stmt FROM @finQ;
    EXECUTE stmt; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_findnearbystore` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_findnearbystore`(IN lat_input float, IN lon_input float, IN minLat Float, IN minLon float, 
IN maxLat float, IN maxLon float, IN R int, IN rad float)
BEGIN
	  Select c.*,JSON_ARRAY(JSON_OBJECT('clientId',location.clientId,
	'locationId', location.locationId 
    ,'Location', location.location
    ,'phoneNumber',location.phoneNumber
    ,'storeNumber',location.storeNumber
    ,'areaId',location.areaId
    ,'address',location.address
    ,'city',location.city
    ,'state',location.state
    ,'zip',location.zip
    ,'district',location.district
    ,'lat',location.lat
    ,'lon',location.lon
    ,'timezone',location.timezone
    ,'active',location.active
    ,'createdAt',location.createdAt
    ,'updatedAt',location.updatedAt)) location,  acos(sin(lat_input)*sin(radians(location.lat)) + 
      cos(lat_input)*cos(radians(location.lat))*cos(radians(location.lon)-lon_input)) * R As DistMiles 
      From location 
      INNER JOIN Client c 
         ON c.clientId = location.clientId 
      Where location.lat Between minLat And maxLat And location.lon 
      Between minLon And maxLon AND acos(sin(lat_input)*sin(radians(location.lat)) +
      cos(lat_input)*cos(radians(location.lat))*cos(radians(location.lon)-lon_input)) * R < rad Order by DistMiles;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_findregions_1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_findregions_1`(in _curActive INTEGER, in _curOrderBy VARCHAR(1000))
begin
	set @Q='SELECT regionId, name FROM region WHERE ';
	set @curActive='active=';
	set @_curActive=_curActive;
	set @orderBy=' ORDER BY ';
	set @_orderBy=_curOrderBy;
	
	set @finQ=CONCAT(@Q,@curActive,@_curActive,@orderBy,_curOrderBy);
	
	PREPARE stmt FROM @finQ;
    EXECUTE stmt; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_findregions_2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_findregions_2`(in _curActive INTEGER,in _curMgrs VARCHAR(500), in _curOrderBy VARCHAR(1000))
begin
	set @Q='SELECT regionId, name FROM region WHERE ';
	set @curActive='active=';
	set @_curActive=_curActive;
	set @andOne= ' AND ';
	/* set @curMgrs='curMgrs='; I send the whole OR Mgrs statement here */
	set @_curMgrs=_curMgrs;		
	set @orderBy=' ORDER BY ';
	set @_orderBy=_curOrderBy;
	
	set @finQ=CONCAT(@Q,@curActive,@_curActive,@andOne,@_curMgrs,@orderBy,_curOrderBy);
	
	PREPARE stmt FROM @finQ;
    EXECUTE stmt; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getbicyclespids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_getbicyclespids`()
begin
	select productId from product where ((description like '%bike%') or (description like '%bicycle%') or (description like '%trike%')) order by CAST(productId AS UNSIGNED);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getgrillspids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_getgrillspids`()
begin
	select productId from product where ((description like '%grill%') or (description like '%burner%')  or (description like '%barbecue%')  or (description like '%bbq%')) order by CAST(productId AS UNSIGNED); 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getlastchatconversationbyreceiverid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_getlastchatconversationbyreceiverid`(IN userId int,IN offst INT,IN lim int)
BEGIN
	
    SELECT 
		c.conversationId,
		c.senderId,
        c.receiverId, 
        c.locationId, 
        c.message,
        c.read,
        c.deleted,
        c.createdAt,
        c.updatedAt,
        chatName,
		userId chatSenderId, 
        /*shows priority only on the receiver user */
        CASE WHEN userId = sender.userId THEN 0 ELSE  c.priority END priority,
		/*maps the sender user */
		CASE WHEN userId = sender.userId THEN sender.firstName ELSE receiver.firstName END chatSenderName, 
		CASE WHEN userId = sender.userId THEN sender.lastName ELSE receiver.lastName END chatSenderLastName, 
		/*if the last message was sent by the requester sets the read as true to ignore the flag*/
		CASE WHEN userId = sender.userId THEN 1 ELSE c.read END readMessage, 
		/*maps the receiver user (AM usually) */
		CASE WHEN userId = sender.userId THEN receiver.firstName ELSE sender.firstName END chatReceiverName, 
		CASE WHEN userId = sender.userId THEN receiver.lastName ELSE sender.lastName END chatReceiverLastName, 
		 /*maps the receiver user (AM usually) */
		CASE WHEN userId = sender.userId THEN receiver.userId ELSE sender.userId END chatReceiverId, 

		JSON_OBJECT('senderId', sender.userId, 'firstName', sender.firstName, 'lastName', sender.lastName) sender,
		JSON_OBJECT('receiverId', receiver.userId, 'firstName', receiver.firstName, 'lastName', receiver.lastName) receiver 
	FROM .conversation c
	JOIN (
			SELECT max(conversationId) conversationId, max(c.createdAt) updatedAt , ct.chatName
			from conversation c
            JOIN managerclaimedtech  ct
              ON ((ct.managerId = c.receiverId AND ct.techId = c.senderId) 
			  OR (ct.managerId = c.senderId AND ct.techId = c.receiverId)) 
			WHERE senderId in (userId) or receiverId in (userId)
            GROUP BY  ct.chatName

  ) lc
	ON lc.conversationId = c.conversationId
	JOIN .`user` sender
	  ON c.senderId = sender.userId
	JOIN .`user` receiver
	  ON c.receiverId = receiver.userId
	WHERE (c.receiverId = userId OR c.senderId = userId)
    -- and deleted = 0
	ORDER BY c.createdAt DESC
    LIMIT offst, lim;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getlastusertimesheet` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_getlastusertimesheet`(IN userId int)
BEGIN

  SELECT s.*, abs(TIMESTAMPDIFF(MINUTE, CURDATE(), s.timeIn)) minutes
  FROM timesheet s 
  INNER JOIN (select timeId 
  FROM timesheet s
  WHERE s.userId = userId 
  ORDER BY updatedAt DESC LIMIT 1) a
  ON a.timeId = s.timeId AND s.timeOut IS NULL;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getrequestproductandservicesperlocation` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_getrequestproductandservicesperlocation`(IN locId int, IN user_id int)
BEGIN
	/*updates last seen by user all request for that locaiton*/
	UPDATE `requestseenbylog` rl JOIN request r ON r.requestId = rl.requestId
    SET userId = user_id 
    WHERE  locationId = locId
    and (r.requestTypeId != 2) or (r.requestTypeId = 2 and r.storeWalkCompleted = 1)
	and r.scheduleDate  <= CURDATE();
	
	/* Wrapped the query to get valid JSON returned.*/
    
    SELECT x.*
		,JSON_OBJECT('productId', x.itemId,
			'upc', x.upc,
			'sku', x.skus,
			'clientId', x.clientId
         ) sku
    FROM (
		SELECT r.requestId,
			   rp.requestProductId
			  ,r.locationId
			  ,rt.requestTypeId
			  ,rt.request requestType
			  ,rp.qtyAssembled
			  ,rp.qtyTotal
              ,pr.price 
              ,pr.ai_code
			  ,p.productId itemId
			  ,p.upc
			  ,pc.sku as skus
              ,pc.clientId clientId
			  ,p.name productName
			  ,cat.categoryId
			  ,cat.name
			  ,rp.completed
			  ,0 isService
              ,r.createdAt
		FROM  request r
		INNER JOIN requestproduct rp
		  ON rp.requestId = r.requestId 
		INNER JOIN location l 
		  ON l.locationId = r.locationId
		  and l.locationid = locId
		INNER JOIN requestType rt
		  on r.requestTypeId = rt.requestTypeId 
		INNER JOIN product p 
		  ON p.productId = rp.productId 
		INNER JOIN pricing pr
		  on p.productId = pr.productId
		INNER JOIN productcategory cat 
		  ON cat.categoryId =p.categoryId
		LEFT OUTER JOIN (SELECT productId, clientId, max(sku) sku FROM clientproductsku pc GROUP BY productId, clientId) pc
		  ON p.productId = pc.productId 
		  and pc.clientId = l.clientId
		WHERE ((r.requestTypeId != 2) or (r.requestTypeId = 2 
		  and storeWalkCompleted = 1)) and rp.completed = 0
		  /*and r.scheduleDate  <= CURDATE()*/

		UNION ALL 

		SELECT r.requestId,
			   rp.storeServiceId
			  ,r.locationId
			  ,rt.requestTypeId
			  ,rt.request requestType
			  ,rp.qtyServiced
			  ,rp.qtyTotal
              ,p.price 
              ,p.ai_code
			  ,p.serviceId itemId
			  ,null upc
			  ,p.serviceId skus
              ,null clientId
			  ,p.name productName
			  ,cat.serviceCategoryId
			  ,cat.name
			  ,rp.completed
			  ,1 isService
              ,r.createdAt
		FROM  request r
		INNER JOIN storeservice rp
		  ON rp.requestId = r.requestId 
		INNER JOIN service p 
		  ON p.serviceId = rp.serviceId 
		INNER JOIN servicecategory cat 
		  ON p.serviceCategoryId = cat.serviceCategoryId 
		INNER JOIN requestType rt
		  on r.requestTypeId = rt.requestTypeId 
		WHERE locationId = locId 
		and ((r.requestTypeId != 2) or (r.requestTypeId = 2 and storeWalkCompleted = 1))
		  /*and r.scheduleDate  <= CURDATE()*/
		and rp.completed = 0 and r.locationId = locId
  ) x
  ORDER BY x.requestTypeId, x.createdAt ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_gettimesheetsperuser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_gettimesheetsperuser`(IN usrId int)
BEGIN

    SET @dates = DATE_SUB(now(), INTERVAL 30 day) and now();

    SELECT CONCAT( MONTHNAME(DATE_ADD(timeIn, INTERVAL - (WEEKDAY(timeIn) +1) DAY))
	, ' ',  DAY(DATE_ADD(timeIn, INTERVAL - (WEEKDAY(timeIn) +1) DAY))
	, ' - ',  MONTHNAME(DATE_ADD(timeIn, INTERVAL + (5 - WEEKDAY(timeIn) ) DAY))
	, ' ',  DAY(DATE_ADD(timeIn, INTERVAL + (5 - WEEKDAY(timeIn)) DAY))) as weekdate
	,YEARWEEK(timeIn, 0) as weekId, 
    ts.*, l.location, l.address, l.city, l.state, l.zip
    FROM timesheet ts
    JOIN `location` l
      on l.locationId = ts.locationId
    WHERE userId =  usrId
    and timeIn between @dates and now()
    ORDER BY timeIn DESC;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_getwheelbarrowspids` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_getwheelbarrowspids`()
begin
	select productId from product where description like '%barrow%' order by CAST(productId AS UNSIGNED);  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insidelayer1_multi` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_insidelayer1_multi`(in _locationId INTEGER, in _prev_satDate VARCHAR(10), in _selected_date VARCHAR(10), in _sunDate VARCHAR(10), in _monDate VARCHAR(10), in _tueDate VARCHAR(10), in _wedDate VARCHAR(10), in _thuDate VARCHAR(10), in _friDate VARCHAR(10), in _satDate VARCHAR(10))
begin
	
	set @_locationId=_locationId;
	
	set @_prev_satDate=_prev_satDate;
	
	set @_sunDate=_sunDate;
	set @_monDate=_monDate;
	set @_monDatePlus=CONCAT(@_monDate,' ','23:59:59');
	set @_tueDate=_tueDate;
	set @_wedDate=_wedDate;
	set @_thuDate=_thuDate;
	set @_friDate=_friDate;
	set @_friDatePlus=CONCAT(@_friDate,' ','23:59:59'); /* called friDate_sql in the old system */
	set @_satDate=_satDate;

	/*set @_createdAt="%2017-08-11%";*/
    set @_selected_date=_selected_date;
	
    /* case 1 req_ssm */
	select
	t1.locationId, treq.requestId, treq.locationId, treq.createdAt, treqprod.requestId, treqprod.productId, treqprod.createdAt 
	FROM 
	location t1 
	RIGHT JOIN 
	request treq on t1.locationId=treq.locationId 
	RIGHT JOIN 
	requestproduct treqprod ON treq.requestId=treqprod.requestId 
	WHERE (t1.locationId=@_locationId) and treq.createdAt BETWEEN @_prev_satDate AND @_monDatePlus;

    /* case 2 tot_req */
    SELECT t1.locationId, treq.requestId, treq.locationId, treq.createdAt, treqprod.requestId, treqprod.productId, treqprod.createdAt 
    FROM location t1 RIGHT JOIN request treq on t1.locationId=treq.locationId RIGHT JOIN requestproduct treqprod ON treq.requestId=treqprod.requestId
    WHERE (t1.locationId=@_locationId) and (treq.createdAt BETWEEN @_sunDate AND @_satDate);
    /* WHERE (t1.locationId=@_locationId) and (treq.createdAt LIKE CONCAT('%', @_selected_date , '%')); */
    
    /* case 3 total_inv */
    SELECT t1.locationId as locationId, tinv.invoiceId as tinvInvoiceId, tinv.locationId as tinvLocationId, tinv.createdAt as tinvCreatedAt, tinvprod.invoiceId as tinvprodInvoiceId, tinvprod.productId as tinvprodProductId, tinvprod.createdAt as tinvprodCreatedAt
    FROM location t1 RIGHT JOIN invoice tinv on t1.locationId=tinv.locationId RIGHT JOIN invoiceproduct tinvprod ON tinv.invoiceId=tinvprod.invoiceId
    WHERE (t1.locationId=@_locationId) and (tinv.createdAt BETWEEN @_sunDate AND @_satDate);
    /* WHERE (t1.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_selected_date , '%')); */
    
    /* case 4 peak which is 7 days part 1*/
    SELECT locid, peaksun, peakmon, peaktue, peakwed, peakthu, peakfri, peaksat, offpeaksun, offpeakmon, offpeaktue, offpeakwed, offpeakthu, offpeakfri, offpeaksat, toggle 
    FROM global_schedule 
    WHERE locid=@_locationId;
    
    
    /* case 5 invoices which is 7 days part 2 */
    select 
    ( SELECT count(locationId) FROM invoice tinv WHERE (tinv.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_sunDate , '%')) ) as inv_sun_cnt,
    ( SELECT count(locationId) FROM invoice tinv WHERE (tinv.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_monDate , '%')) ) as inv_mon_cnt,
    ( SELECT count(locationId) FROM invoice tinv WHERE (tinv.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_tueDate , '%')) ) as inv_tue_cnt,
    ( SELECT count(locationId) FROM invoice tinv WHERE (tinv.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_wedDate , '%')) ) as inv_wed_cnt,
    ( SELECT count(locationId) FROM invoice tinv WHERE (tinv.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_thuDate , '%')) ) as inv_thu_cnt, 
    ( SELECT count(locationId) FROM invoice tinv WHERE (tinv.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_friDate , '%')) ) as inv_fri_cnt,
    ( SELECT count(locationId) FROM invoice tinv WHERE (tinv.locationId=@_locationId) and (tinv.createdAt LIKE CONCAT('%', @_satDate , '%')) ) as inv_sat_cnt,
    ( SELECT count(locationId) FROM request treq WHERE (treq.locationId=@_locationId) and (treq.requestTypeId=1) and (treq.createdAt LIKE CONCAT('%', @_sunDate , '%')) ) as req_sun_ns,
    ( SELECT count(locationId) FROM request treq WHERE (treq.locationId=@_locationId) and (treq.requestTypeId=1) and (treq.createdAt LIKE CONCAT('%', @_monDate , '%')) ) as req_mon_ns,
    ( SELECT count(locationId) FROM request treq WHERE (treq.locationId=@_locationId) and (treq.requestTypeId=1) and (treq.createdAt LIKE CONCAT('%', @_tueDate , '%')) ) as req_tue_ns,
    ( SELECT count(locationId) FROM request treq WHERE (treq.locationId=@_locationId) and (treq.requestTypeId=1) and (treq.createdAt LIKE CONCAT('%', @_wedDate , '%')) ) as req_wed_ns,
    ( SELECT count(locationId) FROM request treq WHERE (treq.locationId=@_locationId) and (treq.requestTypeId=1) and (treq.createdAt LIKE CONCAT('%', @_thuDate , '%')) ) as req_thu_ns, 
    ( SELECT count(locationId) FROM request treq WHERE (treq.locationId=@_locationId) and (treq.requestTypeId=1) and (treq.createdAt LIKE CONCAT('%', @_friDate , '%')) ) as req_fri_ns,
    ( SELECT count(locationId) FROM request treq WHERE (treq.locationId=@_locationId) and (treq.requestTypeId=1) and (treq.createdAt LIKE CONCAT('%', @_satDate , '%')) ) as req_sat_ns;
    
    /* case 6 store walk. blue paint of the store info cell */
    SELECT t1.locationId, count(treq.requestId) as store_walk_cnt, treq.locationId as req_locationId, treq.requestTypeId as req_requestTypeId, treq.createdAt as createdAt
    FROM location t1 RIGHT JOIN request treq on t1.locationId=treq.locationId
    WHERE (t1.locationId=@_locationId) and (treq.requestTypeId=2) and (treq.createdAt BETWEEN @_prev_satDate AND @_friDatePlus);
    
    /* case 7 Week-end Saturday */
	select
	t1.locationId as t1_locationId,
	/*
    t2.requestId as t2_requestId, t2.locationId as t2_locationId, t2.createdAt as t2_createdAt,
    stsrv.storeServiceId as stsrv_storeServiceId, stsrv.userId as stsrv_userId, stsrv.requestId as stsrv_requestId, stsrv.serviceId as stsrv_serviceId, stsrv.createdAt as stsrv_createdAt,
    */
    us.firstName as firstName, us.lastName as lastName
    FROM 
    location t1 
    right join request t2 on t1.locationId = t2.locationId
    right join storeservice stsrv on stsrv.requestId = t2.requestId 
    left outer join user us on us.userId = stsrv.userId
    WHERE (t1.locationId=@_locationId) and (t2.createdAt LIKE CONCAT('%', @_prev_satDate , '%'));
    
    /* case 8 Week-end Sunday */
	select
	t1.locationId as t1_locationId,
	/*
    t2.requestId as t2_requestId, t2.locationId as t2_locationId, t2.createdAt as t2_createdAt,
    stsrv.storeServiceId as stsrv_storeServiceId, stsrv.userId as stsrv_userId, stsrv.requestId as stsrv_requestId, stsrv.serviceId as stsrv_serviceId, stsrv.createdAt as stsrv_createdAt,
    */
    us.firstName as firstName, us.lastName as lastName
    FROM 
    location t1 
    right join request t2 on t1.locationId = t2.locationId
    right join storeservice stsrv on stsrv.requestId = t2.requestId 
    left outer join user us on us.userId = stsrv.userId
    WHERE (t1.locationId=@_locationId) and (t2.createdAt LIKE CONCAT('%', @_sunDate , '%')); 
    
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_inv_loc_crea` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_inv_loc_crea`(in _locationId INTEGER, in _createdAt DATETIME, out outcount INTEGER)
begin
/* SELECT invoiceId,locationId,createdAt FROM invoice USE INDEX (inv_loc_crea_idx) where (locationId=".$curAssemblersLocId.") and (createdAt LIKE '".$sundate."%') */ 
	/* USE INDEX (inv_loc_crea_idx)  */
	set @_locationId=_LocationId;
	set @_createdAt=_createdAt;

   /* select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAt LIKE "@_createdAt%")) into outcount;*/
	
	/*select (SELECT distinct count(invoiceId) FROM invoice USE INDEX (inv_loc_crea_idx) where (locationId=@_locationId) and (createdAt LIKE '2017-07-10%')) into outcount;*/
	select (SELECT distinct count(invoiceId) FROM invoice USE INDEX (inv_loc_crea_idx) where (locationId=@_locationId) and (createdAt LIKE "@_createdAt%")) into outcount;
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_inv_loc_crea_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_inv_loc_crea_all`(in _locationId INTEGER,
in _createdAtSun DATETIME,in _createdAtMon DATETIME,in _createdAtTue DATETIME,in _createdAtWed DATETIME,in _createdAtThu DATETIME,in _createdAtFri DATETIME,in _createdAtSat DATETIME,
out outcountSun INTEGER, out outcountMon INTEGER, out outcountTue INTEGER, out outcountWed INTEGER, out outcountThu INTEGER, out outcountFri INTEGER, out outcountSat INTEGER)
begin
	
	set @_locationId=_LocationId;
	set @_createdAtSun=_createdAtSun;
	set @_createdAtMon=_createdAtMon;
	set @_createdAtTue=_createdAtTue;
	set @_createdAtWed=_createdAtWed;
	set @_createdAtThu=_createdAtThu;
	set @_createdAtFri=_createdAtFri;
	set @_createdAtSat=_createdAtSat;
/* invoice USE INDEX (inv_loc_crea_idx)  */
	select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAtSun LIKE "@_createdAtSun%")) into outcountSun;
		select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAtMon LIKE "@_createdAtMon%")) into outcountMon;
			select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAtTue LIKE "@_createdAtTue%")) into outcountTue;
				select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAtWed LIKE "@_createdAtWed%")) into outcountWed;
					select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAtThu LIKE "@_createdAtThu%")) into outcountThu;
						select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAtFri LIKE "@_createdAtFri%")) into outcountFri;
							select (SELECT distinct count(invoiceId) FROM invoice where (locationId=@_locationId) and (createdAtSat LIKE "@_createdAtSat%")) into outcountSat;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_layer1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_layer1`(in _whereAndOrderByClauses VARCHAR(10000))
begin
	
    SET @Q = 'SELECT locationId, clientId, storeNumber, city, areaId, district FROM location WHERE ';
	set @_whereAndOrderByClauses=_whereAndOrderByClauses;	
	
	set @finQ=CONCAT(@Q,@_whereAndOrderByClauses);
	
	PREPARE stmt FROM @finQ;
    EXECUTE stmt; 
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_moveitemtoinvoice` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_moveitemtoinvoice`(
	 IN itemId INT
    ,IN client_id INT
    ,IN user_id INT
    ,IN serialNum VARCHAR(20)
    ,IN isService BOOLEAN
	)
proc:BEGIN

	DECLARE exit handler for sqlexception
	 BEGIN
	 SHOW ERRORS;
	  SELECT 'ERROR HAPPENED REVERTING'; 
	 
	  ROLLBACK;
	END;
    
      START TRANSACTION;
      
		/**
        
			INITS THE VARIABLES REQUIRED TO INSERT ITEMS INTO THE INVOICE. 
        
        */
		
		 SET @invoiceId = null;  /* the invoice number auto created from the db*/
		 SET @itemId = null;  /* the productId from product table  if is a product, service is diff. */
		 SET @invoiceType = null;  /* the productId from product table  if is a product, service is diff. */
		 SET @locationId = null;  /* the location of the product where items is being selected */
		 SET @vendorNumber = null;  /* the Unique identifier vender number per client*/
		 SET @requestId = null;    /* the requestId where the product comes from */
         SET @invoiceProductId = null; /* the actual created productId/itemId. */
         SET @requestProductId = null; /* the actial request ProductId if item is a product */
         SET @storeServiceId = null; /* references the storeServiceId if item is a service */
         SET @invalidEntry = null; /* checks if the entry is valid or already exist and qty vs assembled are a match*/
         
         
      /**
      * CHECKS IF THE ITEM IS A SERVICE IF NOT, IT PROCESS A REGULAR PRODUCT. 
      */
      IF isService = 0 THEN 
			
		 /*Checks if product has been completed already*/
		 SELECT requestProductId 
         INTO @invalidEntry
		 FROM requestProduct 
		 WHERE requestProductId = itemId 
         and qtyAssembled = qtyTotal;

		 IF @invalidEntry IS NOT NULL OR @invalidEntry = '' THEN 
			SELECT null;
			LEAVE proc;
		 END IF ;
         
        
        /*Sets item to requestProductId*/
        SELECT itemId INTO @requestProductId;

         
		/*marks product as complete*/

		UPDATE requestProduct 
		SET 
			qtyAssembled = qtyAssembled + 1
		WHERE requestProductId = @requestProductId
		AND qtyAssembled < qtyTotal;
		 
		UPDATE requestProduct 
		SET 
			completed = 1,
			completedTime = NOW()
		WHERE
		requestProductId = @requestProductId
		AND qtyAssembled = qtyTotal;
		 
		 
		/*init variables*/
		SELECT 
			r.requestId, r.locationId
		INTO @requestId, @locationId
		FROM
			requestProduct pr
		JOIN request r 
		  ON r.requestId = pr.requestId
		WHERE requestProductId = @requestProductId;
						
		SELECT vendorNumber
        INTO  @vendorNumber
		FROM `client`
		WHERE
			clientId = client_id;
				 
		SET @invoiceType := 1;
				 
		SELECT 
			CASE
				WHEN expenseItem THEN 2 ELSE 1
			END, 
            p.productId
            INTO @invoiceType, @itemId
		FROM
			product p
				JOIN
			requestProduct rp ON rp.productId = p.productId
				JOIN
			pricing pr ON p.productId = pr.productId
				AND pr.clientId = client_id
		WHERE
			rp.requestProductId = @requestProductId;
				
				
				
		/*checks if needs to close request*/
		 UPDATE request r
				LEFT JOIN
			(
				SELECT DISTINCT
					r.requestId,
						rp.requestProductId product,
						rs.storeServiceId service
				FROM
					request r
				LEFT JOIN requestproduct rp ON r.requestId = rp.requestId
					AND rp.completed = 0
				LEFT JOIN storeservice rs ON r.requestId = rs.requestId
					AND rs.completed = 0
				WHERE
					r.requestId = @requestId
             ) closedreq 
				ON r.requestId = closedreq.requestId 
		SET 
			completed = CASE
				WHEN closedreq.product IS NULL THEN 1
				ELSE 0
			END,
			endTime = CASE
				WHEN closedreq.service IS NULL THEN NOW()
				ELSE NULL
			END
		WHERE
			r.requestId = @requestId;
				
		/*Finds invoice or creates invoice*/
				
		SELECT 
			invoiceId
		INTO @invoiceId
		FROM
			invoice
		WHERE
			(locationId = @locationId
				AND userId = user_id
				AND clientId = client_id
				AND invoiceTypeId = @invoiceType
				AND invoiceCompletedTime IS NULL)
		ORDER BY invoiceGeneratedTime DESC
        LIMIT 1;
				           
		IF(@invoiceId IS NULL OR @invoiceId = '') THEN
		 
		 
			INSERT INTO `invoice`
				(
				`invoiceNumber`,
				`userId`,
				`requestId`,
				`signatureImageUrl`,
				`invoiceTypeId`,
				`clientId`,
				`locationId`,
				`workNumber`,
				`vendorNumber`,
				`invoiceGeneratedTime`,
				`invoiceCompletedTime`,
				`keyRecTime`,
				`poNumber`,
				`keyRecNumber`,
				`total`,
				`stamp`)
				VALUES ('N/A'
					 ,user_id
					 , null /* requestId is null cos this needs to go away*/
					 , null
					 , @invoiceType
					 , client_id
					 , @locationId
					 , null
					 , @vendorNumber
					 , NOW()
					 , null
					 , null
					 , null
					 , null
					 , 0
					 , null);
				
			SELECT LAST_INSERT_ID()
            INTO @invoiceId;
            
            /*inserts into invoice product*/
			INSERT INTO `invoiceproduct`
				(
					`invoiceId`,
					`productId`,
					`itemId`, /* productId is deprectated should start using itemId*/
					`requestId`, /*request needs is move to invoice product*/
					`requestItemId`, /*represents productId or serviceId */
					`name`,
					`qty`,
					`total`,
					`pricePerUnit`,
					`ai_code`,
					`isExpenseItem`,
					`serialNumber`,
					`entryType`
               )
					
			SELECT @invoiceId
				 ,p.productId
                 ,p.productId
                 ,@requestId
                 ,@requestProductId
				 ,p.name
				 ,1
				 ,pr.price
				 ,pr.price
				 ,pr.ai_code
				 ,pr.expenseItem
				 ,serialNum
				 ,'P'
			 FROM product p 
			JOIN requestProduct rp 
			 ON rp.productId = p.productId
			JOIN pricing pr
			 ON p.productId = pr.productId 
			 and pr.clientId = client_id 
			WHERE rp.requestProductId = itemId;
		
          
				
		ELSE 
        
			/*SELECT @invoiceId , @vendorNumber, @locationId, @requestId;*/
            
            /*Requires new signature if new item is selected and marks invoice as incomplete*/
			UPDATE `invoice`
            SET signatureImageUrl = null, 
                invoiceCompletedTime = null
            WHERE invoiceId = @invoiceId;
        	  
            /*checks if there is an item already intersted*/
           SELECT 
				invoiceId
			INTO @invoiceProductId
			FROM
				`invoiceproduct`
			WHERE
				invoiceId = @invoiceId AND
                productId = @itemId AND 
                requestId = @requestId AND 
                entryType = 'P'
			LIMIT 1;
		
            
            /*inserts into invoice product*/
			IF(@invoiceProductId IS NULL OR @invoiceProductId = '') THEN
                      
				/*inserts into invoice product*/
				INSERT INTO `invoiceproduct`
				(
					`invoiceId`,
					`productId`,
					`itemId`, /* productId is deprectated should start using itemId*/
					`requestId`, /*request needs is move to invoice product*/
					`requestItemId`, /*represents productId or serviceId */
					`name`,
					`qty`,
					`total`,
					`pricePerUnit`,
					`ai_code`,
					`isExpenseItem`,
					`serialNumber`,
					`entryType`
                )
					
				SELECT @invoiceId
					 ,p.productId
					 ,p.productId
					 ,@requestId
					 ,@requestProductId
					 ,p.name
					 ,1
					 ,pr.price
					 ,pr.price
					 ,pr.ai_code
					 ,pr.expenseItem
					 ,serialNum
					 ,'P'
				 FROM product p 
					JOIN requestProduct rp 
					 ON rp.productId = p.productId
					JOIN pricing pr
					 ON p.productId = pr.productId 
					 and pr.clientId = client_id 
					WHERE rp.requestProductId = @requestProductId;
                
			ELSE
				
                /*Updates the qty of the invoice product 
                */
    
				UPDATE `invoiceproduct`
				SET
				`qty` = (qty + 1),
				`total` = (qty) * pricePerUnit
				WHERE 
					invoiceId = @invoiceId AND
                    productId = @itemId AND 
                    requestId = @requestId AND 
					entryType = 'P';
            END IF;
						
		END IF;
        
        
        
        
/************************************************************
**** SERVICE ADD HERE 
************************************************************/
        
		ELSEIF isService = 1 THEN 
			
             /*Checks if is a valid add*/
			 SELECT storeServiceId 
             INTO @invalidEntry
			 FROM storeservice 
			 WHERE storeServiceId = itemId 
             and qtyServiced = qtyTotal;
			 
	 
			 IF @invalidEntry IS NOT NULL THEN 
				SELECT null;
				LEAVE proc;
			 END IF ;
             
             SELECT itemId INTO @storeServiceId;
			/*marks product as complete*/

			UPDATE storeservice 
			SET 
				qtyServiced = qtyServiced + 1
			WHERE storeServiceId = @storeServiceId
			AND qtyServiced < qtyTotal;
			 
			UPDATE storeservice 
			SET 
				completed = 1,
				completedTime = NOW()
			WHERE
			storeServiceId = @storeServiceId
			AND qtyServiced = qtyTotal;
		 
		 
			/*init variables */
			SELECT 
				r.requestId,r.locationId, pr.serviceId
			INTO @requestId, @locationId, @itemid
			FROM
				storeservice pr
			JOIN request r ON r.requestId = pr.requestId
			WHERE storeServiceId = @storeServiceId;
								
			/*SELECTS Vendor id From client*/
			SELECT vendorNumber
			INTO @vendorNumber
			FROM `client`
			WHERE
				clientId = client_id;
				 
			 SET @invoiceType := 1;
					
			/*checks if needs to close request*/
			 UPDATE request r
					LEFT JOIN
				(
					SELECT DISTINCT
						r.requestId,
							rp.storeServiceId product,
							rs.storeServiceId service
					FROM
						request r
					LEFT JOIN storeservice rp ON r.requestId = rp.requestId
						AND rp.completed = 0
					LEFT JOIN storeservice rs ON r.requestId = rs.requestId
						AND rs.completed = 0
					WHERE
						r.requestId = @requestId
				) closedreq ON r.requestId = closedreq.requestId 
			SET 
				completed = CASE
					WHEN closedreq.product IS NULL THEN 1
					ELSE 0
				END,
				endTime = CASE
					WHEN closedreq.service IS NULL THEN NOW()
					ELSE NULL
				END
			WHERE
				r.requestId = @requestId;
				
		/*Finds latest invoice or creates invoice*/
				
		SELECT 
			invoiceId
		INTO @invoiceId
		FROM
			invoice
		WHERE
			(locationId = @locationId
				AND userId = user_id
				AND clientId = client_id
				AND invoiceTypeId = @invoiceType
				AND invoiceCompletedTime IS NULL)
		ORDER BY invoiceGeneratedTime DESC
        LIMIT 1;
				      

		IF(@invoiceId IS NULL OR @invoiceId = '') THEN
		 
			
			INSERT INTO `invoice`
				(
				`invoiceNumber`,
				`userId`,
				`requestId`,
				`signatureImageUrl`,
				`invoiceTypeId`,
				`clientId`,
				`locationId`,
				`workNumber`,
				`vendorNumber`,
				`invoiceGeneratedTime`,
				`invoiceCompletedTime`,
				`keyRecTime`,
				`poNumber`,
				`keyRecNumber`,
				`total`,
				`stamp`)
				VALUES ( 'N/A' /*enters a -1 for invoice number for now*/
					 ,user_id
					 , null /*removing requestId */
					 , null
					 , @invoiceType
					 , client_id
					 , @locationId
					 , null
					 , @vendorNumber
					 , NOW()
					 , null
					 , null
					 , null
					 , null
					 , 0
					 , null);
				
			SELECT LAST_INSERT_ID()
            INTO @invoiceId;
            
		 
			/*inserts into invoice the service*/
			INSERT INTO `invoiceproduct`
				(
                `invoiceId`,
				`productId`,
				`itemId`, /* productId is deprectated should start using itemId*/
				`requestId`, /*request needs is move to invoice product*/
				`requestItemId`, /*represents productId or serviceId */
				`name`,
				`qty`,
				`total`,
				`pricePerUnit`,
				`ai_code`,
				`isExpenseItem`,
				`serialNumber`,
				`entryType`
                )
					
			SELECT @invoiceId
				 ,p.serviceId
                 ,p.serviceId
                 ,@requestId
                 ,@storeServiceId
				 ,p.name
				 ,1
				 ,p.price
				 ,p.price
				 ,p.ai_code
				 ,0
				 ,serialNum
				 ,'S'
			FROM service p 
			JOIN storeservice rp 
			 ON rp.serviceId = p.serviceId
			WHERE rp.storeServiceId = @storeServiceId;

				
		ELSE 
			
		  /*Requires new signature if new item is selected and marks invoice as incomplete*/
			UPDATE `invoice`
			SET signatureImageUrl = null, 
				invoiceCompletedTime = null
			WHERE invoiceId = @invoiceId;
        
			
		  /*checks if there is an item already intersted*/
           SELECT 
				invoiceId
			INTO @invoiceProductId
			FROM
				`invoiceproduct`
			WHERE
				invoiceId = @invoiceId AND
                productId = @itemId AND
                requestId = @requestId AND
                entryType = 'S'
			LIMIT 1;
                
            
            /*inserts into invoice product*/
			IF(@invoiceProductId IS NULL  OR @invoiceProductId = '') THEN
          
				INSERT INTO `invoiceproduct`
				(
					`invoiceId`,
					`productId`,
					`itemId`, /* productId is deprectated should start using itemId*/
					`requestId`, /*request needs is move to invoice product*/
					`requestItemId`, /*represents productId or serviceId */
					`name`,
					`qty`,
					`total`,
					`pricePerUnit`,
					`ai_code`,
					`isExpenseItem`,
					`serialNumber`,
					`entryType`
                )
				SELECT @invoiceId
					 ,p.serviceId
                     ,p.serviceId
                     ,@requestId
                     ,@storeServiceId
					 ,p.name
					 ,1
					 ,p.price
					 ,p.price
					 ,p.ai_code
					 ,0
					 ,serialNum
					 ,'S'
				FROM service p 
				JOIN storeservice rp 
				  ON rp.serviceId = p.serviceId
				WHERE rp.storeServiceId = @storeServiceId;
			ELSE
				
                UPDATE `invoiceproduct` ip
				SET
				`qty` = ip.qty + 1,
				`total` = (ip.qty) * ip.pricePerUnit
				WHERE 
					invoiceId = @invoiceId AND
					productId = @itemId AND 
                    requestId = @requestId AND
					entryType = 'S';
                
            END IF;
			
		END IF;
            
	END IF;
	
	/*Invoice total */
	
	UPDATE invoice c
	INNER JOIN (
	  SELECT invoiceId, SUM(total) as total
	  FROM invoiceProduct
	  GROUP BY invoiceId
	) x ON c.invoiceId = x.invoiceId
	SET c.total = x.total
	WHERE c.invoiceId = @invoiceId; 
    
		
	COMMIT; 
	
    /* returns invoice*/
	SELECT * FROM invoice
	WHERE invoiceId = @invoiceId; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_moveitemtoinvoice2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`assemblersdb`@`%` PROCEDURE `sp_moveitemtoinvoice2`(
	 IN itemId INT
    ,IN client_id INT
    ,IN user_id INT
    ,IN serialNum VARCHAR(20)
    ,IN isService BOOLEAN
	)
proc:BEGIN

	DECLARE exit handler for sqlexception
	 BEGIN
	 SHOW ERRORS;
	  SELECT 'ERROR HAPPENED REVERTING'; 
	 
	  ROLLBACK;
	END;
    
      START TRANSACTION;
      
		/**
        
			INITS THE VARIABLES REQUIRED TO INSERT ITEMS INTO THE INVOICE. 
        
        */
		
		 SET @invoiceId = null;  /* the invoice number auto created from the db*/
		 SET @itemId = null;  /* the productId from product table  if is a product, service is diff. */
		 SET @invoiceType = null;  /* the productId from product table  if is a product, service is diff. */
		 SET @locationId = null;  /* the location of the product where items is being selected */
		 SET @vendorNumber = null;  /* the Unique identifier vender number per client*/
		 SET @requestId = null;    /* the requestId where the product comes from */
         SET @invoiceProductId = null; /* the actual created productId/itemId. */
         SET @requestProductId = null; /* the actial request ProductId if item is a product */
         SET @storeServiceId = null; /* references the storeServiceId if item is a service */
         SET @invalidEntry = null; /* checks if the entry is valid or already exist and qty vs assembled are a match*/
         
         
      /**
      * CHECKS IF THE ITEM IS A SERVICE IF NOT, IT PROCESS A REGULAR PRODUCT. 
      */
      IF isService = 0 THEN 
			
		 /*Checks if product has been completed already*/
		 SELECT requestProductId 
         INTO @invalidEntry
		 FROM requestProduct 
		 WHERE requestProductId = itemId 
         and qtyAssembled = qtyTotal;

		 IF @invalidEntry IS NOT NULL OR @invalidEntry = '' THEN 
			SELECT null;
			LEAVE proc;
		 END IF ;
         
        
        /*Sets item to requestProductId*/
        SELECT itemId INTO @requestProductId;

         
		/*marks product as complete*/

		UPDATE requestProduct 
		SET 
			qtyAssembled = qtyAssembled + 1
		WHERE requestProductId = @requestProductId
		AND qtyAssembled < qtyTotal;
		 
		UPDATE requestProduct 
		SET 
			completed = 1,
			completedTime = NOW()
		WHERE
		requestProductId = @requestProductId
		AND qtyAssembled = qtyTotal;
		 
		 
		/*init variables*/
		SELECT 
			r.requestId, r.locationId
		INTO @requestId, @locationId
		FROM
			requestProduct pr
		JOIN request r 
		  ON r.requestId = pr.requestId
		WHERE requestProductId = @requestProductId;
						
		SELECT vendorNumber
        INTO  @vendorNumber
		FROM `client`
		WHERE
			clientId = client_id;
				 
		SET @invoiceType := 1;
				 
		SELECT 
			CASE
				WHEN expenseItem THEN 2 ELSE 1
			END, 
            p.productId
            INTO @invoiceType, @itemId
		FROM
			product p
				JOIN
			requestProduct rp ON rp.productId = p.productId
				JOIN
			pricing pr ON p.productId = pr.productId
				AND pr.clientId = client_id
		WHERE
			rp.requestProductId = @requestProductId;
				
				
				
		/*checks if needs to close request*/
		 UPDATE request r
				LEFT JOIN
			(
				SELECT DISTINCT
					r.requestId,
						rp.requestProductId product,
						rs.storeServiceId service
				FROM
					request r
				LEFT JOIN requestproduct rp ON r.requestId = rp.requestId
					AND rp.completed = 0
				LEFT JOIN storeservice rs ON r.requestId = rs.requestId
					AND rs.completed = 0
				WHERE
					r.requestId = @requestId
             ) closedreq 
				ON r.requestId = closedreq.requestId 
		SET 
			completed = CASE
				WHEN closedreq.product IS NULL THEN 1
				ELSE 0
			END,
			endTime = CASE
				WHEN closedreq.service IS NULL THEN NOW()
				ELSE NULL
			END
		WHERE
			r.requestId = @requestId;
				
		/*Finds invoice or creates invoice*/
				
		SELECT 
			invoiceId
		INTO @invoiceId
		FROM
			invoice
		WHERE
			(locationId = @locationId
				AND userId = user_id
				AND clientId = client_id
				AND invoiceTypeId = @invoiceType
				AND invoiceCompletedTime IS NULL)
		ORDER BY invoiceGeneratedTime DESC
        LIMIT 1;
				           
		IF(@invoiceId IS NULL OR @invoiceId = '') THEN
		 
		 
			INSERT INTO `invoice`
				(
				`invoiceNumber`,
				`userId`,
				`requestId`,
				`signatureImageUrl`,
				`invoiceTypeId`,
				`clientId`,
				`locationId`,
				`workNumber`,
				`vendorNumber`,
				`invoiceGeneratedTime`,
				`invoiceCompletedTime`,
				`keyRecTime`,
				`poNumber`,
				`keyRecNumber`,
				`total`,
				`stamp`)
				VALUES ('N/A'
					 ,user_id
					 , null /* requestId is null cos this needs to go away*/
					 , null
					 , @invoiceType
					 , client_id
					 , @locationId
					 , null
					 , @vendorNumber
					 , NOW()
					 , null
					 , null
					 , null
					 , null
					 , 0
					 , null);
				
			SELECT LAST_INSERT_ID()
            INTO @invoiceId;
            
            /*inserts into invoice product*/
			INSERT INTO `invoiceproduct`
				(
					`invoiceId`,
					`productId`,
					`itemId`, /* productId is deprectated should start using itemId*/
					`requestId`, /*request needs is move to invoice product*/
					`requestItemId`, /*represents productId or serviceId */
					`name`,
					`qty`,
					`total`,
					`pricePerUnit`,
					`ai_code`,
					`isExpenseItem`,
					`serialNumber`,
					`entryType`
               )
					
			SELECT @invoiceId
				 ,p.productId
                 ,p.productId
                 ,@requestId
                 ,@requestProductId
				 ,p.name
				 ,1
				 ,pr.price
				 ,pr.price
				 ,pr.ai_code
				 ,pr.expenseItem
				 ,serialNum
				 ,'P'
			 FROM product p 
			JOIN requestProduct rp 
			 ON rp.productId = p.productId
			JOIN pricing pr
			 ON p.productId = pr.productId 
			 and pr.clientId = client_id 
			WHERE rp.requestProductId = itemId;
		
          
				
		ELSE 
        
			/*SELECT @invoiceId , @vendorNumber, @locationId, @requestId;*/
            
            /*Requires new signature if new item is selected and marks invoice as incomplete*/
			UPDATE `invoice`
            SET signatureImageUrl = null, 
                invoiceCompletedTime = null
            WHERE invoiceId = @invoiceId;
        	  
            /*checks if there is an item already intersted*/
           SELECT 
				invoiceId
			INTO @invoiceProductId
			FROM
				`invoiceproduct`
			WHERE
				invoiceId = @invoiceId AND
                productId = @itemId AND 
                requestId = @requestId AND 
                entryType = 'P'
			LIMIT 1;
		
            
            /*inserts into invoice product*/
			IF(@invoiceProductId IS NULL OR @invoiceProductId = '') THEN
                      
				/*inserts into invoice product*/
				INSERT INTO `invoiceproduct`
				(
					`invoiceId`,
					`productId`,
					`itemId`, /* productId is deprectated should start using itemId*/
					`requestId`, /*request needs is move to invoice product*/
					`requestItemId`, /*represents productId or serviceId */
					`name`,
					`qty`,
					`total`,
					`pricePerUnit`,
					`ai_code`,
					`isExpenseItem`,
					`serialNumber`,
					`entryType`
                )
					
				SELECT @invoiceId
					 ,p.productId
					 ,p.productId
					 ,@requestId
					 ,@requestProductId
					 ,p.name
					 ,1
					 ,pr.price
					 ,pr.price
					 ,pr.ai_code
					 ,pr.expenseItem
					 ,serialNum
					 ,'P'
				 FROM product p 
					JOIN requestProduct rp 
					 ON rp.productId = p.productId
					JOIN pricing pr
					 ON p.productId = pr.productId 
					 and pr.clientId = client_id 
					WHERE rp.requestProductId = @requestProductId;
                
			ELSE
				
                /*Updates the qty of the invoice product 
                */
    
				UPDATE `invoiceproduct`
				SET
				`qty` = (qty + 1),
				`total` = (qty) * pricePerUnit
				WHERE 
					invoiceId = @invoiceId AND
                    productId = @itemId AND 
                    requestId = @requestId AND 
					entryType = 'P';
            END IF;
						
		END IF;
        
        
        
        
/************************************************************
**** SERVICE ADD HERE 
************************************************************/
        
		ELSEIF isService = 1 THEN 
			
             /*Checks if is a valid add*/
			 SELECT storeServiceId 
             INTO @invalidEntry
			 FROM storeservice 
			 WHERE storeServiceId = itemId 
             and qtyServiced = qtyTotal;
			 
	 
			 IF @invalidEntry IS NOT NULL THEN 
				SELECT null;
				LEAVE proc;
			 END IF ;
             
             SELECT itemId INTO @storeServiceId;
			/*marks product as complete*/

			UPDATE storeservice 
			SET 
				qtyServiced = qtyServiced + 1
			WHERE storeServiceId = @storeServiceId
			AND qtyServiced < qtyTotal;
			 
			UPDATE storeservice 
			SET 
				completed = 1,
				completedTime = NOW()
			WHERE
			storeServiceId = @storeServiceId
			AND qtyServiced = qtyTotal;
		 
		 
			/*init variables */
			SELECT 
				r.requestId,r.locationId, pr.serviceId
			INTO @requestId, @locationId, @itemid
			FROM
				storeservice pr
			JOIN request r ON r.requestId = pr.requestId
			WHERE storeServiceId = @storeServiceId;
								
			/*SELECTS Vendor id From client*/
			SELECT vendorNumber
			INTO @vendorNumber
			FROM `client`
			WHERE
				clientId = client_id;
				 
			 SET @invoiceType := 1;
					
			/*checks if needs to close request*/
			 UPDATE request r
					LEFT JOIN
				(
					SELECT DISTINCT
						r.requestId,
							rp.storeServiceId product,
							rs.storeServiceId service
					FROM
						request r
					LEFT JOIN storeservice rp ON r.requestId = rp.requestId
						AND rp.completed = 0
					LEFT JOIN storeservice rs ON r.requestId = rs.requestId
						AND rs.completed = 0
					WHERE
						r.requestId = @requestId
				) closedreq ON r.requestId = closedreq.requestId 
			SET 
				completed = CASE
					WHEN closedreq.product IS NULL THEN 1
					ELSE 0
				END,
				endTime = CASE
					WHEN closedreq.service IS NULL THEN NOW()
					ELSE NULL
				END
			WHERE
				r.requestId = @requestId;
				
		/*Finds latest invoice or creates invoice*/
				
		SELECT 
			invoiceId
		INTO @invoiceId
		FROM
			invoice
		WHERE
			(locationId = @locationId
				AND userId = user_id
				AND clientId = client_id
				AND invoiceTypeId = @invoiceType
				AND invoiceCompletedTime IS NULL)
		ORDER BY invoiceGeneratedTime DESC
        LIMIT 1;
				      

		IF(@invoiceId IS NULL OR @invoiceId = '') THEN
		 
			
			INSERT INTO `invoice`
				(
				`invoiceNumber`,
				`userId`,
				`requestId`,
				`signatureImageUrl`,
				`invoiceTypeId`,
				`clientId`,
				`locationId`,
				`workNumber`,
				`vendorNumber`,
				`invoiceGeneratedTime`,
				`invoiceCompletedTime`,
				`keyRecTime`,
				`poNumber`,
				`keyRecNumber`,
				`total`,
				`stamp`)
				VALUES ( 'N/A' /*enters a -1 for invoice number for now*/
					 ,user_id
					 , null /*removing requestId */
					 , null
					 , @invoiceType
					 , client_id
					 , @locationId
					 , null
					 , @vendorNumber
					 , NOW()
					 , null
					 , null
					 , null
					 , null
					 , 0
					 , null);
				
			SELECT LAST_INSERT_ID()
            INTO @invoiceId;
            
		 
			/*inserts into invoice the service*/
			INSERT INTO `invoiceproduct`
				(
                `invoiceId`,
				`productId`,
				`itemId`, /* productId is deprectated should start using itemId*/
				`requestId`, /*request needs is move to invoice product*/
				`requestItemId`, /*represents productId or serviceId */
				`name`,
				`qty`,
				`total`,
				`pricePerUnit`,
				`ai_code`,
				`isExpenseItem`,
				`serialNumber`,
				`entryType`
                )
					
			SELECT @invoiceId
				 ,p.serviceId
                 ,p.serviceId
                 ,@requestId
                 ,@storeServiceId
				 ,p.name
				 ,1
				 ,p.price
				 ,p.price
				 ,p.ai_code
				 ,0
				 ,serialNum
				 ,'S'
			FROM service p 
			JOIN storeservice rp 
			 ON rp.serviceId = p.serviceId
			WHERE rp.storeServiceId = @storeServiceId;

				
		ELSE 
			
		  /*Requires new signature if new item is selected and marks invoice as incomplete*/
			UPDATE `invoice`
			SET signatureImageUrl = null, 
				invoiceCompletedTime = null
			WHERE invoiceId = @invoiceId;
        
			
		  /*checks if there is an item already intersted*/
           SELECT 
				invoiceId
			INTO @invoiceProductId
			FROM
				`invoiceproduct`
			WHERE
				invoiceId = @invoiceId AND
                productId = @itemId AND
                requestId = @requestId AND
                entryType = 'S'
			LIMIT 1;
                
            
            /*inserts into invoice product*/
			IF(@invoiceProductId IS NULL  OR @invoiceProductId = '') THEN
          
				INSERT INTO `invoiceproduct`
				(
					`invoiceId`,
					`productId`,
					`itemId`, /* productId is deprectated should start using itemId*/
					`requestId`, /*request needs is move to invoice product*/
					`requestItemId`, /*represents productId or serviceId */
					`name`,
					`qty`,
					`total`,
					`pricePerUnit`,
					`ai_code`,
					`isExpenseItem`,
					`serialNumber`,
					`entryType`
                )
				SELECT @invoiceId
					 ,p.serviceId
                     ,p.serviceId
                     ,@requestId
                     ,@storeServiceId
					 ,p.name
					 ,1
					 ,p.price
					 ,p.price
					 ,p.ai_code
					 ,0
					 ,serialNum
					 ,'S'
				FROM service p 
				JOIN storeservice rp 
				  ON rp.serviceId = p.serviceId
				WHERE rp.storeServiceId = @storeServiceId;
			ELSE
				
                UPDATE `invoiceproduct` ip
				SET
				`qty` = ip.qty + 1,
				`total` = (ip.qty) * ip.pricePerUnit
				WHERE 
					invoiceId = @invoiceId AND
					productId = @itemId AND 
                    requestId = @requestId AND
					entryType = 'S';
                
            END IF;
			
		END IF;
            
	END IF;
	
	/*Invoice total */
	
	UPDATE invoice c
	INNER JOIN (
	  SELECT invoiceId, SUM(total) as total
	  FROM invoiceProduct
	  GROUP BY invoiceId
	) x ON c.invoiceId = x.invoiceId
	SET c.total = x.total
	WHERE c.invoiceId = @invoiceId; 
    
		
	COMMIT; 
	
    /* returns invoice*/
	SELECT * FROM invoice
	WHERE invoiceId = @invoiceId; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-19 15:49:41
