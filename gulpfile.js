//
const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const babel = require('gulp-babel');
const concat= require('gulp-concat');
const del = require('del');
const Cache = require('gulp-file-cache');
const cache = new Cache(); 
const uglify = require('gulp-uglify'); 
const git = require('gulp-git');
const runSequence = require('run-sequence').use(gulp);
const gutil = require('gulp-util');





console.log(gutil.env.env);

gulp.task('compile', () => {
  let stream =  gulp.src([
            '!node_modules/**/*.js',
            '!test/**/*.js',
            '!build/**/*',
            '!README.md',
            '!docs/**/*',
            '!log/**/*',
            // '!dist/**/*',
            '!gulpfile.js', 
            './**/*.js'])
            .pipe(babel()) // compile new ones
           // .pipe(concat('bundle.js'))
            .pipe(uglify())
            .pipe(gulp.dest('./build')) // write them

    return stream
});

gulp.task('default', ()=>{
  runSequence('clean','compile','package','docs', 'certs');
  // todo enable checkin into the azure branch

  //'checkout_azure'  , 'azure_prep','commit_azure','push_azure', 'check_out_master','modules'

});

gulp.task('clean', () => {
    return del(['./build/**/*']);
});

//move the package
gulp.task('package', () => {
  let stream =  gulp.src([
            'package.json', 
            'package-lock.json'])
            .pipe(gulp.dest('./build')) // write them

    return stream
});

//move the documentation
gulp.task('docs', () => {
  let stream =  gulp.src([
            './docs/**/*'])
            .pipe(gulp.dest('./build/docs')) // write them

    return stream
});

gulp.task('certs', () => {
  let stream =  gulp.src([
            './certs/**/*.pem'])
            .pipe(gulp.dest('./build/certs')) // write them

    return stream
});

gulp.task('modules', () => {
  let stream =  gulp.src([
            './node_modules/**/*.js'])
            .pipe(gulp.dest('./build/node_modules')) // write them

    return stream
});

gulp.task('checkout_azure', function(){
  console.log('check azure out')
  return git.checkout('azure_dist', function (err) {
    if (err) throw err;
  });
});

gulp.task('push_azure', function(){
  git.push('origin', 'azure_dist', function (err) {
    if (err) throw err;
  });
});


gulp.task('check_out_master', function(){
  console.log('check out master')
  return git.checkout('master', function (err) {
    if (err) throw err;
  });
});

gulp.task('azure_prep', function(err){
  console.log('files changed')
   return del(['!.gitignore','!README.md', '!./build','!./node_modules','!./certs','./**'])
    .then(paths => {
          // copy files to root and commit.     
      gulp.src('./build/**/*')
      .pipe(gulp.dest("./"))
    });
}); 

/*
gulp.task('bump', ()=>{
  return gulp.src(['./package.json'])
  .pipe(gulp.bump({type:'minor'}))
  .pipe(gulp.dest('./'));
});*/

gulp.task('commit_azure',  ()=> {
   return gulp.src(['./*', '!./build/**/*'])
    .pipe(git.commit(['azure build version new version' , '']))
})

gulp.task('push_azure', function(){
  git.push('origin', function (err) {
    if (err) throw err;
  });
});