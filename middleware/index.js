import moment             from 'moment';
import {db}               from '../models';
import userController     from '../controller/user';
import logger             from '../util/logger';
import {validateToken}    from '../util/token';
import {cleanQuery}       from '../util/helperFunctions';
import * as errorHandler  from '../util/errorHandler';

let authorizeToken = (req, res, next) => {

  let token = req.query.token || req.headers['x-access-token'];
  let refresh = req.headers['x-access-refresh-token'];

  if (token) {

    logger.info('validateToken');
    validateToken(token, refresh)
    .then(usr => {

      if (!usr) {
        return next({
          code: 'UNAUTHORIZED',
          message: 'Invalid token'
        });
      } 
      else {
        
        if (req.query.noTrack  && req.query.noTrack == "true") {
          return db.models.user.findOne({
            where: {username: usr.upn}
          })
          .then(user => {

            if (!user) {
              return res.status(403).send('UNAUTHORIZED: Invalid user requested'); 
            }
            // Append token and refresh token 
            // Cleans query params values to avoid empty. 
            req.query = cleanQuery(req.query); 
            res.set('x-access-refresh-token', user.refreshToken); 
            res.set('x-access-token', user.token); 
            return next('route');

          })
          .catch(next);

        } 
        else 
        {

          return db.models.user.update({
            lastSeen: Date.now()
          }, 
          {
            where: {username: usr.upn}
          })
          .then(result => {
            return db.models.user.findOne({
              where: {username: usr.upn}
            }); 
          })
          .then(user => {

            if (!user) {
              return res.status(403).send('UNAUTHORIZED: Invalid user requested'); 
            } else {
            // Append token and refresh token 
            // Cleans query params values to avoid empty. 
            req.query = cleanQuery(req.query);

            res.set('x-access-refresh-token', user.refreshToken); 
            res.set('x-access-token', user.token);

            // Appends logged in user to the request as currentUser. 
            req.currentUser = JSON.parse(JSON.stringify(user));
            return next('route');
            
          }
        })
        .catch(next); 

        } 
      }
    })
    .catch(error => {
      return res.status(403).send('UNAUTHORIZED: Invalid token')
    });
  } else {
    return res.status(403).send('UNAUTHORIZED: Permission denied'); 
  };
};

// Validates type of error and returns accordingly.
let checkErrors = (err, req, res, next) =>{
  let e = errorHandler.getErrorType(err);
  if (e.code && e.code === 'UNAUTHORIZED') {
    return res.status(401);
  } else {
    return res.status(400);
  }
  return res.json(e);
 // next();
}

export {authorizeToken,checkErrors}