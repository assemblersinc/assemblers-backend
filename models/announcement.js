import * as ROLES from '../util/roleType'; 

export default (sequelize, DataTypes) =>{
  const announcement = sequelize.define('announcement', {
    announcementId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    text: {
      type: DataTypes.STRING(500),
      allowNull: false
    },
    tag:{
      type: DataTypes.STRING(500),
      allowNull: false
    },
    userId: {
        type: DataTypes.INTEGER(11),
        allowNull: false
    },
    url: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'announcement', 
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  
  announcement.associate = (models) => {
    announcement.belongsTo(models.user, {as:'author', otherKey:'userId', foreignKey:'userId'}); 
  }

  announcement.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN, 
  }
  return announcement; 
};
