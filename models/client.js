import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const client = sequelize.define('client', {
      clientId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      code: {
        type: DataTypes.STRING(5),
        allowNull: false,
        unique: true
      },
      storeId: {
        type: DataTypes.STRING(15),
        allowNull: false
      },
      name: {
        type: DataTypes.STRING(45),
        allowNull: true
      },
      phoneNumber: {
        type: DataTypes.STRING(15),
        allowNull: true
      },
      email: {
        type: DataTypes.STRING(45),
        allowNull: true
      },
      contact: {
        type: DataTypes.STRING(45),
        allowNull: true
      },
      active: {
        type: DataTypes.BOOLEAN,
        allowNull: true
      },
      createdAt: {
          type: DataTypes.DATE,
          allowNull: true
      },
      updatedAt: {
          type: DataTypes.DATE,
          allowNull: true
      }
    }, {
      tableName: 'client', 
      hooks: {
        beforeFind: (model, options) => {
          console.log('beforeFind');
        },
        beforeCreate: (model, options) => {
          console.log('beforeCreate'); 
        },
        beforeDestroy: (model, options) => {
          console.log('beforeDestroy'); 
        },
        beforeUpdate: (model, options) => {
          console.log('beforeUpdate'); 
        }
        ,
        beforeUpsert: (model, options) => {
          console.log('beforeUpsert'); 
        }
        ,
        beforeBulkCreate: (model, options) => {
          console.log('beforeBulkCreate'); 
        }
        ,
        beforeBulkDestroy: (model, options) => {
          console.log('beforeBulkDestroy'); 
        }
        ,
        beforeBulkUpdate: (model, options) => {
          console.log('beforeBulkUpdate'); 
        }
      }
    });

    client.associate = (models) =>{
          client.hasMany(models.clientattributes, {as:'clientattributes', foreignKey: 'clientId'});
          //never do this. never alias the same as the model.
          client.hasMany(models.location, {as:'location', foreignKey: 'clientId'});

	        //Also, big mistake to not just use 'id' for all the primary keys

	        //client.hasMany(models.location, {foreignKey: 'clientId'});//fixed

    }

    client.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
    }

    return client; 
}


