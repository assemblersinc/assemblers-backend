import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const clientproductsku = sequelize.define('clientproductsku', {
    productId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'productId'
      }
    },
    upc: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'upc'
      }
    },
    sku: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    clientId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'client',
        key: 'clientId'
      }
    },
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'clientproductsku',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  clientproductsku.associate = (models) =>{  
  }

  clientproductsku.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }

  return clientproductsku
};
