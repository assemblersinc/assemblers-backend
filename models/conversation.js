import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) =>{
   const conversation = sequelize.define('conversation', {
    conversationId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    senderId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    receiverId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'location',
        key: 'locationId'
      }
    },
    read: {
      type: DataTypes.BOOLEAN,
      defaultValue:false, 
      allowNull: false,
    },
    message: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    priority: {
      type: DataTypes.BOOLEAN,
      defaultValue:false,
      allowNull: false
    },
    deleted: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'conversation',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  conversation.associate = (models) =>{
    conversation.belongsTo(models.user, {as:'sender' , otherKey:'userId', foreignKey:'senderId' });
    conversation.belongsTo(models.user, {as:'receiver' , otherKey:'userId', foreignKey:'receiverId' });
    conversation.belongsTo(models.location, {as:'location' , otherKey:'locationId', foreignKey:'locationId' });
  }

  conversation.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }
 
  return conversation; 
};
