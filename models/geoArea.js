import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const geoarea = sequelize.define('geoarea', {
    areaId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    regionId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    managerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    managerSecondId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    managerThirdId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    opsManagerId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'geoarea',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  geoarea.associate = (models) =>{
      geoarea.belongsTo(models.user, {as:'manager' , otherKey:'userId', foreignKey:'managerId' });
      geoarea.belongsTo(models.user, {as:'secondManager' , otherKey:'userId', foreignKey:'managerSecondId' });
      geoarea.belongsTo(models.user, {as:'thirdManager' , otherKey:'userId', foreignKey:'managerThirdId' });
      geoarea.belongsTo(models.user, {as:'opsManager',otherKey:'userId', foreignKey:'opsManagerId'});
      geoarea.belongsTo(models.region, {as:'region',otherKey:'regionId', foreignKey:'regionId'});
      geoarea.hasMany(models.user, {as:'users',otherKey:'areaId', foreignKey:'areaId'});
      geoarea.hasMany(models.location, {as:'locations',otherKey:'areaId', foreignKey:'areaId'});
  }

  geoarea.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }
 
  return geoarea; 
};
