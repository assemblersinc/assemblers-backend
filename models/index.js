import fs         from 'fs';
import path       from 'path';
import Sequelize  from 'sequelize';
import lodash     from 'lodash'; 
import util       from 'util';
import logger     from '../util/logger';
import {config}   from '../config';

let sequelize = new Sequelize(
  config.databases.apiDb.database,
  config.databases.apiDb.user,
  config.databases.apiDb.password,
  config.databases.apiDb.options
);

let optimum = new Sequelize(config.databases.optimumdb.database
  ,config.databases.optimumdb.user,config.databases.optimumdb.password, {
        dialect: 'mssql',
        port: 1433, 
        host: config.databases.optimumdb.host, 
    });

let db = null;

db = {
  sequelize,
  Sequelize, 
  optimum,
  models:[]
};

/** Sync models **/
fs.readdirSync(__dirname)
.filter(file => {
  return (file.indexOf('.') !== 0) && (file !== 'index.js');
})
.forEach(file => {
  const modelDir = path.join(__dirname, file);
  const model = sequelize.import(modelDir);
  db.models[model.name]= model;
});

Object.keys(db.models).forEach(key => {
  if ('associate' in db.models[key]) {
    db.models[key].associate(db.models);
  }
});

export {db};