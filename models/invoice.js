import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const invoice = sequelize.define('invoice', {
    invoiceId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    invoiceNumber: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey:true, 
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    requestId: {
      type: DataTypes.BIGINT,
      allowNull: true
    },
    signatureImageUrl: {
      type: DataTypes.STRING(1234),
      allowNull: true
    },
    invoiceTypeId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey:true, 
      defaultValue: 1
    },
    clientId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey:true, 
      references: {
        model: 'client',
        key: 'clientId'
      }
    },
    workNumber: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    vendorNumber: {
      type: DataTypes.STRING(5),
      allowNull: true
    },
    invoiceGeneratedTime: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
    },
    invoiceCompletedTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    keyRecTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    poNumber: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    keyRecNumber: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    total: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: '0'
    },
    stamp: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  },
  {
    tableName: 'invoice',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  invoice.associate = (models) => {
    invoice.belongsTo(models.user, {as: 'user', foreignKey: 'userId'});
    invoice.belongsTo(models.client, {as: 'client', foreignKey: 'clientId'});
	  invoice.belongsTo(models.location, {foreignKey: 'locationId'});
    invoice.belongsTo(models.invoicetype, {as: 'invoicetype', foreignKey: 'invoiceTypeId'});
    invoice.belongsTo(models.request, {as: 'request', foreignKey: 'requestId'});
    invoice.hasMany(models.invoiceproduct, {as: 'invoiceproduct', foreignKey: 'invoiceId'});

    ////////* for area managers  */////
    invoice.hasMany(models.invoiceproduct, {as: 'repairs', foreignKey: 'invoiceId'});
    invoice.hasMany(models.invoiceproduct, {as: 'products', foreignKey: 'invoiceId'});

  }

  invoice.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.GENERAL, 
      update: ROLES.GENERAL
  }

  return invoice; 
};
