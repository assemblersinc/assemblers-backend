import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const invoiceProduct = sequelize.define('invoiceproduct', {
    invoiceId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'invoice',
        key: 'invoiceId'
      }
    },
    productId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    itemId:{
      type: DataTypes.STRING(45),
      allowNull: true,
      defaultValue: null
    },
    sku:{
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: null
    },
    requestId:{
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      allowNull: false,
      references: {
        model: 'request',
        key: 'requestId'
      }
    }, 
    requestItemId:{
      type: DataTypes.INTEGER(11),
      allowNull: false
    }, 
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    qty: {
      type: DataTypes.INTEGER(45),
      allowNull: false,
      defaultValue: 0
    },
    total: {
      type: DataTypes.INTEGER(45),
      allowNull: false,
      defaultValue: 0
    },
    pricePerUnit: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0.00
    },
    ai_code: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    isExpenseItem: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    serialNumber: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    entryType:{
      type: DataTypes.STRING(10),
      allowNull: false, 
      defaultValue: 'P'
    }, 
    createdAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updatedAt: {
      type: DataTypes.DATE,
      allowNull: true
    }
  },
  {
    tableName: 'invoiceproduct',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  invoiceProduct.associate = (models) => {
    invoiceProduct.belongsTo(models.invoice, {as: 'invoiceproduct', foreignKey: 'invoiceId'});
    invoiceProduct.belongsTo(models.request, {as: 'request', foreignKey: 'requestId'});
  }

  invoiceProduct.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.GENERAL, 
      update: ROLES.GENERAL
  }

  return invoiceProduct; 
};