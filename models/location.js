import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const location = sequelize.define('location', {
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    clientId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'client',
        key: 'clientId'
      }
    },
    Location: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    phoneNumber: {
      type: DataTypes.STRING(20),
      allowNull: false
    },
    storeNumber: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    areaId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    address: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    city: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    state: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    zip: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    district: {
      type: DataTypes.STRING(10),
      allowNull: false
    },
    lat: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    lon: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    timezone: {
      type: DataTypes.STRING(5),
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'location',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  location.associate =(models) => {
    location.belongsTo(models.client, {as:'client', foreignKey: 'clientId'});
    location.belongsTo(models.geoarea, {as:'geoarea', foreignKey: 'areaId'});
    location.hasOne(models.storeassemblycount, {as:'storeassemblycounts', foreignKey: 'locationId'});
    location.hasMany(models.invoice, {as:'invoices', foreignKey: 'locationId'});
    location.hasMany(models.request, {as:'requests', foreignKey: 'locationId'});
    location.hasMany(models.storerepairticket, {as:'repairs', foreignKey: 'locationId'});
  };

  location.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  };

  return location; 
};
