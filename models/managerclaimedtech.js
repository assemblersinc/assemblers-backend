import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) =>{
  const managerclaimedtech = sequelize.define('managerclaimedtech', {
    managerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    techId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    chatName: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'managerclaimedtech',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  
  managerclaimedtech.associate = (models) => {
      managerclaimedtech.belongsTo(models.user, {as:'manager' , otherKey:'userId', foreignKey:'managerId' });
      managerclaimedtech.belongsTo(models.user, {as:'tech' , otherKey:'userId', foreignKey:'techId' });

     /*      user.hasMany(models.managerclaimedtech);
     user.hasMany(models.managerclaimedtech, {as:'clockedin', foreignKey: 'managerId', otherKey:'userId'});
     user.hasMany(models.managerclaimedtech, {as:'clockedout', foreignKey: 'managerId', otherKey:'userId'});*/
  }

  managerclaimedtech.permissions  = {
      create: 'admin', 
      select: ROLES.GENERAL, 
      delete: 'admin', 
      update: 'admin'
  }
  return managerclaimedtech; 
};
