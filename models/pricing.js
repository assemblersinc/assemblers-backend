import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const pricing = sequelize.define('pricing', {
    priceId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    productId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'productId'
      }
    },
    clientId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'client',
        key: 'clientId'
      }
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      primaryKey: false,
      references: {
        model: 'location',
        key: 'locationId'
      }
    },
    price: {
      type: "DOUBLE",
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    expenseItem: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    ai_code: {
      type: DataTypes.STRING(45),
      allowNull: true
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'pricing',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  pricing.associate = (models) => {
    // pricing.belongsTo(models.requestproduct, {foreignKey: 'priceId'});
  }

  pricing.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }
  return pricing; 
};
