import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const pricingrequest = sequelize.define('pricingrequest', {
    priceRequestId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    sku: {
      type: DataTypes.STRING(200),
      allowNull: true,
      primaryKey: true,
      unique:true
    },
    upc: {
      type: DataTypes.STRING(100),
      allowNull: true,
      primaryKey: false,
      unique:false
    },
    description: {
      type: DataTypes.STRING(500),
      allowNull: true,
      primaryKey: false
    },
    clientId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'client',
        key: 'clientId'
      }
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      primaryKey: false
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'pricingrequest',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  pricingrequest.associate = (models) => {
      pricingrequest.belongsTo(models.location, {as:'location', foreignKey: 'locationId'});
      pricingrequest.belongsTo(models.client, {as:'client', foreignKey: 'clientId'});
      pricingrequest.belongsTo(models.user, {as:'user', foreignKey: 'userId'});
  }

  pricingrequest.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }
  return pricingrequest; 
};
