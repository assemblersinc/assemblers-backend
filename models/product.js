import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const product = sequelize.define('product', {
    productId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    model: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    upc: {
      type: DataTypes.STRING(45),
      allowNull: false,
      primaryKey: true
    },
    categoryId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
  tableName: 'product', 
  hooks: {
    beforeFind: (model, options) => {
      console.log('beforeFind');
    },
    beforeCreate: (model, options) => {
      console.log('beforeCreate'); 
    },
    beforeDestroy: (model, options) => {
      console.log('beforeDestroy'); 
    },
    beforeUpdate: (model, options) => {
      console.log('beforeUpdate'); 
    }
    ,
    beforeUpsert: (model, options) => {
      console.log('beforeUpsert'); 
    }
    ,
    beforeBulkCreate: (model, options) => {
      console.log('beforeBulkCreate'); 
    }
    ,
    beforeBulkDestroy: (model, options) => {
      console.log('beforeBulkDestroy'); 
    }
    ,
    beforeBulkUpdate: (model, options) => {
      console.log('beforeBulkUpdate'); 
    }
  }
});

  product.associate = (models) => {
    product.belongsTo(models.productcategory, {as:'productcategory', foreignKey: 'categoryId'});
    product.hasOne(models.pricing, {as:'pricing',through: models.pricing, foreignKey: 'productId'});   
    product.hasOne(models.clientproductsku, {as:'sku',through: 'sku',otherKey: 'productId',  foreignKey: 'productId'});   
  }

  product.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }

  return product;
};
