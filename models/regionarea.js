import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const region = sequelize.define('region', {
    regionId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    managerId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    managerSecondId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    division: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: null
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'region',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  region.associate = (models) =>{
      region.belongsTo(models.user, {as:'manager' , otherKey:'userId', foreignKey:'managerId' });
      region.belongsTo(models.user, {as:'secondManager' , otherKey:'userId', foreignKey:'managerSecondId' });
      region.hasMany(models.geoarea, {as:'geoareas', foreignKey:'regionId' });
  }

  region.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }
 
  return region; 
};
