import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const repaircost = sequelize.define('repair', {
    repairId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true, 
      autoIncrement: true
    },
    price: {
      type: "DOUBLE",
      allowNull: true
    },
    timeLevel: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    minutes: {
      type: "DOUBLE",
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    }, 
    ai_code: {
      type: DataTypes.STRING(45),
      allowNull: false
    }, 
    repairCategoryId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'repair',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  repaircost.associate =(models) => {
    repaircost.belongsTo(models.repaircategory, {as:'repairCategory' ,otherKey:'repairCategoryId', foreignKey:'repairCategoryId' }) ;
  }

  repaircost.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.GENERAL, 
      update: ROLES.GENERAL
  }

  return repaircost;
};
