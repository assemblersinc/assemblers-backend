import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const request = sequelize.define('request', {
    requestId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true, 
      references: {
        model: 'location',
        key: 'locationId'
      }
    },
    requestedById: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    comments: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    photos:{
      type:DataTypes.JSON, 
      allowNull:true
    }, 
    requestTypeId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'requesttype',
        key: 'requestTypeId'
      }
    },
    scheduleDate: {
        type: DataTypes.DATE,
        allowNull: true, 
        defaultValue: DataTypes.NOW
    },
    createTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    endTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    storeWalkCompleted:{
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    completed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'request',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  request.associate = (models) => {
    request.belongsTo(models.requesttype, {as:'requesttype', foreignKey: 'requestTypeId'});
    request.hasMany(models.requestproduct, {as:'requestproduct', foreignKey: 'requestId'});
    request.hasMany(models.storeservice, {as:'requestservice', foreignKey: 'requestId'});
    request.hasMany(models.storewalk, {as: 'storewalk', foreignKey: 'requestId'});
    request.hasOne(models.requestcreatedby, {as: 'requestcreatedby', foreignKey: 'requestId'});
    request.belongsTo(models.location, {as: 'location', foreignKey: 'locationId'});
  };

  request.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.GENERAL, 
      update: ROLES.GENERAL
  };
  return request; 
};
