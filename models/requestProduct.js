import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const requestProduct =  sequelize.define('requestproduct', {
    requestProductId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    requestId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'request',
        key: 'requestId'
      }
    },
    priceId: {
      type:DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'pricing',
        key: 'priceId'
      }
    },
    productId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'product',
        key: 'productId'
      }
    },
     userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    serialNumber: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    qtyTotal: {
      type: DataTypes.DOUBLE,
      allowNull: true
    },
    qtyAssembled: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0
    },
    fromStoreWalk: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    statusId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      defaultValue: 1
    },
    completedTime: {
      type: DataTypes.DATE,
      allowNull: true,
    },
    scanTime: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    completed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    isUserGeoFenced:{
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue:null
    },
    userGeoLong:{
      type: DataTypes.FLOAT,
      allowNull: true
    },
    userGeoLat:{
      type: DataTypes.FLOAT,
      allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'requestproduct',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  
  requestProduct.associate = (models) => {
      requestProduct.belongsTo(models.user, {as:'user', foreignKey: 'userId'});
      requestProduct.belongsTo(models.product, {as:'product', foreignKey: 'productId'});
      requestProduct.belongsTo(models.pricing, {foreignKey: 'priceId'});
      requestProduct.hasOne(models.invoiceproduct, {foreignKey: 'requestItemId', as: 'invoiceproductrequest'});
  }

  requestProduct.permissions  = {
    create: ROLES.GENERAL, 
    select: ROLES.GENERAL, 
    delete: ROLES.GENERAL, 
    update: ROLES.GENERAL
  }
  return requestProduct; 
};
