import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) =>{
  const role = sequelize.define('role', {
    roleId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: false
    },
    name: {
      type: DataTypes.STRING(15),
      allowNull: false
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: true
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'role',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  
  role.associate = (models) => {
  
  }

  role.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }
  return role; 
};
