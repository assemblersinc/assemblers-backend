import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const scan = sequelize.define('scan', {
    scanId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    requestId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'request',
        key: 'requestId'
      }
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    scannedTime: {
      type: DataTypes.DATE,
      allowNull: false
    },
    serialNumber: {
      type: DataTypes.STRING(45),
      allowNull: false
    },
    productId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'product',
        key: 'productId'
      }
    },
    completedTime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    qty: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    images: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    statusId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'status',
        key: 'statusId'
      }
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'scan',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  scan.associate = (models) => {
    
  }

  scan.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.GENERAL, 
      update: ROLES.GENERAL
  }
  return scan;
};
