import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const service = sequelize.define('service', {
    serviceId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true, 
      autoIncrement: true
    },
    price: {
      type: "DOUBLE",
      allowNull: true
    },
    name: {
      type: DataTypes.STRING(100),
      allowNull: false
    }, 
    ai_code: {
      type: DataTypes.STRING(45),
      allowNull: false
    }, 
    serviceCategoryId: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'service',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  service.associate =(models) => {
    service.belongsTo(models.servicecategory, {as:'serviceCategory' ,otherKey:'serviceCategoryId', foreignKey:'serviceCategoryId' }) ;
  }

  service.permissions  = {
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }

  return service;
};
