import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const session = sequelize.define('sessions', {
    deviceId: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'sessions',
    classMethods:
    {
      associate: (models) => {
  
      }
    }
  });
  session.associate = (models) => {
    
  }

  session.permissions  = {

  }
  return session;
};
