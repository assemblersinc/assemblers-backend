import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const storeassemblycounts = sequelize.define('storeassemblycount', {
      storeAssemblyId: {
        type: DataTypes.INTEGER(11),
        allowNull: false,
        primaryKey: true,
        autoIncrement: true
      },
      locationId: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      countedDate: {
        type: DataTypes.DATE,
        allowNull: false
      },
      bikeOnRackCount: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      bikeHolesCount: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      bikeStagedCount: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      bikeBoxedCount: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      bikeRepairsCount:{
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      wheelBarrowCount: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      grillCount: {
        type: DataTypes.INTEGER(11),
        allowNull: false
      },
      bikeCount: {
        type: DataTypes.INTEGER(11),
        allowNull: true
      },
      createdAt: {
          type: DataTypes.DATE,
          allowNull: true
      },
      updatedAt: {
          type: DataTypes.DATE,
          allowNull: true
      }
    }, {
      tableName: 'storeassemblycount', 
      hooks: {
        beforeFind: (model, options) => {
          console.log('beforeFind');
        },
        beforeCreate: (model, options) => {
          console.log('beforeCreate'); 
        },
        beforeDestroy: (model, options) => {
          console.log('beforeDestroy'); 
        },
        beforeUpdate: (model, options) => {
          console.log('beforeUpdate'); 
        }
        ,
        beforeUpsert: (model, options) => {
          console.log('beforeUpsert'); 
        }
        ,
        beforeBulkCreate: (model, options) => {
          console.log('beforeBulkCreate'); 
        }
        ,
        beforeBulkDestroy: (model, options) => {
          console.log('beforeBulkDestroy'); 
        }
        ,
        beforeBulkUpdate: (model, options) => {
          console.log('beforeBulkUpdate'); 
        }
      }
    });

    storeassemblycounts.associate = (models) =>{
      storeassemblycounts.belongsTo(models.location, {as:'location', foreignKey: 'locationId'});
    }

    storeassemblycounts.permissions  = {
      create: ROLES.GENERAL,  
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
    }

    return storeassemblycounts; 
}


