import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const storerepair = sequelize.define('storerepair', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true, 
      autoIncrement:true
    },
    storeRepairId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: false
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'location',
        key: 'locationId'
      }
    },
    imageData: {
      type: DataTypes.JSON,
      allowNull: true
    },
    qrcode: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    productId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    repairId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'repaircost',
        key: 'repairCostId'
      }
    },
    completed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'storerepair',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  storerepair.associate= (models) => {
    storerepair.belongsTo(models.user, {as:'user' ,otherKey:'userId', foreignKey:'userId' }) ;
    storerepair.belongsTo(models.location, {as:'location' ,otherKey:'locationId', foreignKey:'locationId' }) ;
    storerepair.belongsTo(models.repair, {as:'repair' ,otherKey:'repairId', foreignKey:'repairId' }) ;
  }

  storerepair.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.GENERAL, 
      update: ROLES.GENERAL
  }


  
  return storerepair; 
};
