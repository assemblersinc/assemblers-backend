import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const storerepairticket = sequelize.define('storerepairticket', {
    storeRepairId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'location',
        key: 'locationId'
      }
    },
    imageData: {
      type: DataTypes.JSON,
      allowNull: true
    },
    qrcode: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    productName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    productSku: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    productId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    completed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'storerepairticket',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  storerepairticket.associate= (models) => {
    storerepairticket.hasMany(models.storerepair, {as: 'repairs', otherKey:'storeRepairId', foreignKey:'storeRepairId' })
    storerepairticket.belongsTo(models.user, {as:'user' ,otherKey:'userId', foreignKey:'userId' }) ;
    storerepairticket.belongsTo(models.location, {as:'location' ,otherKey:'locationId', foreignKey:'locationId' }) ;
    storerepairticket.belongsTo(models.product, {as:'product' ,otherKey:'productId', foreignKey:'productId' }) ;
  }

  storerepairticket.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.GENERAL, 
      update: ROLES.GENERAL
  }


  
  return storerepairticket; 
};
