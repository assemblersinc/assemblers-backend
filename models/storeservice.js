import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const storeservice = sequelize.define('storeservice', {
    storeServiceId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    requestId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'request',
        key: 'requestId'
      }
    },
    serviceId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'service',
        key: 'serviceId'
      }
    },
    qtyTotal: {
      type: DataTypes.INTEGER(11),
      allowNull: false, 
      defaultValue: 0
    },
    qtyServiced: {
      type: DataTypes.INTEGER(11),
      allowNull: false, 
      defaultValue: 0
    },
    imageData: {
      type: DataTypes.JSON,
      allowNull: true, 
      defaultValue: {}
    },
    completed: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    isUserGeoFenced:{
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue:null
    },
    userGeoLong:{
      type: DataTypes.FLOAT,
      allowNull: true
    },
    userGeoLat:{
      type: DataTypes.FLOAT,
      allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'storeservice',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  storeservice.associate= (models) => {
    storeservice.belongsTo(models.user, {as:'user' ,otherKey:'userId', foreignKey:'userId' }) ;
    storeservice.belongsTo(models.request, {as:'request' ,otherKey:'requestId', foreignKey:'requestId' }) ;
    storeservice.belongsTo(models.service, {as:'service' ,otherKey:'serviceId', foreignKey:'serviceId' }) ;

    // for invoice
    storeservice.hasOne(models.invoiceproduct, {foreignKey: 'requestItemId', as: 'invoiceservicerequest'});
  }

  storeservice.permissions  = {
      create:ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }


  
  return storeservice; 
};
