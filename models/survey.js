import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const survey = sequelize.define('survey', {
    surveyId: {
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'user',
        key: 'userId'
      },
    },
    enteredByName: {
      type: DataTypes.STRING(200),
      allowNull: true
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'location',
        key: 'locationId'
      },
    },
    questionId: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    invoiceId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'invoice',
        key: 'invoiceId'
      },
    },
    response: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'survey', 
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  
  survey.associate = (models) => {
    survey.belongsTo(models.surveyquestion, {as: 'question', foreignKey:'surveyId'}); 
    models.user.hasOne(survey, {as:'survey', foreignKey:'userId'}); 
    models.location.hasOne(survey, {as:'location', foreignKey:'locationId'}); 
    models.invoice.hasOne(survey, {as:'invoice', foreignKey:'invoiceId'}); 
  }

  survey.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.ADMIN
  }
  return survey; 
};
