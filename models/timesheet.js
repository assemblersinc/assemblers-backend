import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  let timesheet = sequelize.define('timesheet', {
    timeId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    userId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'user',
        key: 'userId'
      }
    },
    timeIn: {
      type: DataTypes.DATE,
      allowNull: true
    },
    timeOut: {
      type: DataTypes.DATE,
      allowNull: true
    },
    mealIn: {
      type: DataTypes.DATE,
      allowNull: true
    },
    mealOut: {
      type: DataTypes.DATE,
      allowNull: true
    },
    hasProblem: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: false
    },
    comment: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    locationId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'location',
        key: 'locationId'
      }
    }, 
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'timesheet',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });
  
  timesheet.associate= (models) => {
    timesheet.belongsTo(models.user, {as:'user' ,otherKey:'userId', foreignKey:'userId' }) 
    timesheet.belongsTo(models.location, {as:'location' ,otherKey:'locationId', foreignKey:'locationId' }) 
  }

  /**
  * model permissions
  */
  timesheet.permissions  = {
      create: ROLES.GENERAL, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.GENERAL
  }
  return timesheet; 
};
