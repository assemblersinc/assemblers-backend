import * as ROLES from '../util/roleType'; 
export default (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    userId: {
      type: DataTypes.INTEGER(11),
      primaryKey: true,
      autoIncrement: true
    },
    areaId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      references: {
        model: 'geoarea',
        key: 'areaId'
      }
    },
    firstName: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    lastName: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    username: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    token: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    refreshToken: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    tokenExpiresOn: {
      type: DataTypes.DATE,
      allowNull: true
    },
    notificationToken:{
      type: DataTypes.TEXT,
      allowNull: true
    }, 
    userDeviceOS:{
      type: DataTypes.STRING(10),
      allowNull: true
    }, 
    email: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING(15),
      allowNull: true
    },
    managerId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    hiredate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    timeSheetId: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    },
    roleId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'role',
        key: 'roleId'
      }
    },
    techId: {
      type: DataTypes.STRING(25),
      allowNull: true
    },
    clockStatus: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    mealStatus: {
      type: DataTypes.STRING(10),
      allowNull: true
    },
    active: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false
    },
    lastSeen: {
        type: DataTypes.DATE,
        allowNull: true
    },
    createdAt: {
        type: DataTypes.DATE,
        allowNull: true
    },
    updatedAt: {
        type: DataTypes.DATE,
        allowNull: true
    }
  }, {
    tableName: 'user',
    hooks: {
      beforeFind: (model, options) => {
        console.log('beforeFind');
      },
      beforeCreate: (model, options) => {
        console.log('beforeCreate'); 
      },
      beforeDestroy: (model, options) => {
        console.log('beforeDestroy'); 
      },
      beforeUpdate: (model, options) => {
        console.log('beforeUpdate'); 
      }
      ,
      beforeUpsert: (model, options) => {
        console.log('beforeUpsert'); 
      }
      ,
      beforeBulkCreate: (model, options) => {
        console.log('beforeBulkCreate'); 
      }
      ,
      beforeBulkDestroy: (model, options) => {
        console.log('beforeBulkDestroy'); 
      }
      ,
      beforeBulkUpdate: (model, options) => {
        console.log('beforeBulkUpdate'); 
      }
    }
  });

  user.associate = (models) => {

    models.request.belongsTo(user, {as: 'request', foreignKey: 'requestedById'});
    user.belongsTo(models.geoarea, {as:'geoarea', foreignKey: 'areaId'});
    user.belongsTo(models.role, {as:'role', through:'roleId', foreignKey: 'roleId'});
    user.belongsTo(models.timesheet, {as:'timesheet', foreignKey: 'timeSheetId'});

    user.hasMany(models.invoice, {as: 'invoices', foreignKey: 'userId'});

    user.hasOne(models.managerclaimedtech, {as: 'claimedby',foreignKey: 'managerId'});

    // for AM claimed techs. 
    user.hasMany(models.managerclaimedtech, {as:'claimtechs', foreignKey: 'managerId', otherKey:'userId'});

     //unclaimed AM by area.
    user.hasMany(models.user, {as:'unclaimtechs', sourceKey:'areaId', foreignKey: 'areaId'});

    user.hasMany(models.managerclaimedtech, {as:'clockedin', foreignKey: 'managerId', otherKey:'userId'});
    user.hasMany(models.managerclaimedtech, {as:'clockedout', foreignKey: 'managerId', otherKey:'userId'});

  };

  user.permissions  = {
    
      create: ROLES.ADMIN, 
      select: ROLES.GENERAL, 
      delete: ROLES.ADMIN, 
      update: ROLES.GENERAL
  }

  return user; 
};
