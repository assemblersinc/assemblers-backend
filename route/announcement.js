import controller   from '../controller/announcement';
import {Router}     from 'express';

const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/user/:userId')
  .get((...args) => controller.findByUserId(...args));

router.route('/:announcementId')
  .get((...args) => controller.findById(...args))
  .put((...args) => controller.update(...args))
  .delete((...args) => controller.remove(...args));

export default router;