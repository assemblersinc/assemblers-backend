import controller     from '../controller/areamanager';
import {Router}       from 'express';
 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));


router.route('/:managerId/mytechs')
  .get((...args) => controller.findAllMyTechs(...args))

/*
route to handle manager claiming tech. 
*/

router.route('/:managerId/unclaimtech/')
  .get((...args) => controller.findAllUnClaimedTech(...args))

router.route('/:managerId/claimtech/')
  .get((...args) => controller.findAllClaimedTechs(...args))
  .post((...args) => controller.createClaimTech(...args));
  
router.route('/:managerId/claimtech/:techId')
  .get((...args) => controller.findOneClaimTech(...args))
  .delete((...args) => controller.deleteTechClaim(...args))
  .put((...args) => controller.updateTechClaim(...args));

router.route('/invoice/:clientId')
  .get((...args) => controller.findInvoicePerClient(...args)); 

router.route('/storerequest/:clientId')
  .get((...args) => controller.findStoreRequestsPerClient(...args)); 

router.route('/storerepair/:clientId')
  .get((...args) => controller.findStoreRepairsPerClient(...args)); 





export default router;