import controller   from '../controller/auth';
import {Router}     from 'express'; 

const router = new Router();

router.route('/login')
  .post((...args) => controller.login(...args));

router.route('/logout')
  .post((...args) => controller.logout(...args));

export default router;