import controller   from '../controller/azuregraph';
import {Router}     from 'express';

const router = new Router();

router.route('/user/:username')
  .get((...args) => controller.getUser(...args))

router.route('/user')
  .post((...args) => controller.createAzureUser(...args));

router.route('/user/both')
  .post((...args) => controller.createAzureAndAssemblersUser(...args));
  
router.route('/invalidatetoken')
   .post((req, res, next) => controller.invalidatedTokens(...args));

export default router;