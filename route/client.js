import controller from '../controller/client';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/:clientId')
  .get((...args) => controller.findOne(...args)); 
  
router.route('/:clientId/locations')
  .get((...args) => controller.findClientLocationsWithClientId(...args));

router.route('/:clientId/locations/:locationId')
  .get((...args) => controller.findClientLocationsWithClientAndLocationId(...args))
  .delete((...args) => controller.remove(...args));

router.route('/:clientName/nogps')
  .get((...args) => controller.findClientByName(...args));

  router.route('/:clientName/')
  .get((...args) => controller.findClientByName(...args));

export default router;