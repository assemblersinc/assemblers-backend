import controller from '../controller/conversation';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));


router.route('/sender/:senderId/receiver/:receiverId')
  .post((...args) => controller.findMyMessages(...args))
  .put((...args) => controller.updateMyMessage(...args))
  .delete((...args) => controller.removeMyMessage(...args)); 

  router.route('/receiver/:receiverId')
  .get((...args) => controller.findAllConversationLastMessage(...args))
  
router.route('/searchuser/:term')
  .get((...args) => controller.findUserByName(...args))

router.route('/:converstationId')
  .get((...args) => controller.findOneByConversationId(...args))
  .put((...args) => controller.update(...args))
  .delete((...args) => controller.remove(...args)); 
  


export default router;