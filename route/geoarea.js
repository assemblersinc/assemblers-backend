import controller from '../controller/geoarea';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/:areaId')
  .get((...args) => controller.findOne(...args))
  .put((...args) => controller.update(...args))
  .delete((...args) => controller.destroy(...args))


  
export default router;