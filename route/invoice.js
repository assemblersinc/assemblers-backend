import {Router}       from 'express';
import controller     from '../controller/invoice';
import {signatures}   from '../util/storage'; 

const router = new Router();

// Invoice Table
router.route('/')
  .get((...args) => controller.findAllInvoice(...args))
  .post((...args) => controller.createInvoice(...args))
  .put((...args) => controller.updateMultipleInvoices(...args));

router.route('/completed')
  .get((...args) => controller.findAllInvoiceCompleted(...args));

// TODO: In Phase 2, rename this endpoint to match other types of blob upload
router.route('/submit')
  .post((...args) => controller.saveSurveyWithInvoiceSignature(...args));

router.route('/upload/stamp')
  .post((...args) => controller.uploadStampImagesAndUpdateInvoices(...args));

router.route('/current')
  .get((...args) => controller.findAllInvoiceCurrent(...args));

router.route('/history')
  .get((...args) => controller.findInvoiceHistory(...args));

// Invoice Type Table
router.route('/type')
  .get((...args) => controller.findAllInvoiceType(...args))
  .post((...args) => controller.createInvoiceType(...args));

router.route('/type/:invoiceTypeId')
  .put((...args) => controller.updateInvoiceType(...args))
  .get((...args) => controller.findOneInvoiceType(...args))
  .delete((...args) => controller.removeInvoiceType(...args));

// Invoice Product Table
router.route('/product')
  .get((...args) => controller.findAllInvoiceProduct(...args))
  .post((...args) => controller.createInvoiceProduct(...args));

router.route('/product/remove')
  .post((...args) => controller.removeInvoiceProductFromInvoice(...args));

router.route('/product/:invoiceProductId')
  .put((...args) => controller.updateInvoiceProduct(...args))
  .get((...args) => controller.findOneInvoiceProduct(...args))
  .delete((...args) => controller.removeInvoiceProduct(...args));

// Invoice Table
router.route('/:invoiceId')
  .put((...args) => controller.updateInvoice(...args))
  .get((...args) => controller.findOneInvoice(...args))
  .delete((...args) => controller.removeInvoice(...args));

router.route('/:invoiceId/signature')
  .get((...args) => controller.getSignature(...args))
  .post((...args) => controller.applySignature(...args));

router.route('/:invoiceId/total')
  .post((...args) => controller.applySignature(...args));

router.route('/:invoiceId/survey/')
  .get((...args) => controller.surveyResponse(...args));

export default router;