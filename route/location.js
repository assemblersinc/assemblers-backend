import controller from '../controller/location';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/searchlocation')
  .get((...args) => controller.searchLocationByStateCityZip(...args))

router.route('/storenumber/:storeNumber/client/:clientId')
  .get((...args) => controller.findOneByStoreNumber(...args));
  
router.route('/:locationId')
  .get((...args) => controller.findOne(...args)); 

router.route('/zip')
  .post((...args) => controller.findAllByZipCode(...args));

router.route('/findnearby')
  .post((...args) => controller.findStoresNearUserGps(...args));

export default router;