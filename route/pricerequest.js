import controller from '../controller/pricerequest';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/:sku')
  .get((...args) => controller.findBySku(...args));

router.route('/:requestPriceId')
  .delete((...args) => controller.remove(...args));

export default router;