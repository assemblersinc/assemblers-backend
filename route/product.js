import controller from '../controller/product';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

// Specs search tearm (sku, upc, name)
router.route('/search/:term/client/:clientId')
  .get((...args) => controller.search(...args))


router.route('/:productId/client/:clientId')
  .get((...args) => controller.findOne(...args));

router.route('/:productId/:clientId')
  .put((...args) => controller.update(...args))
  .get((...args) => controller.findOne(...args))
  .delete((...args) => controller.remove(...args));
  
export default router;