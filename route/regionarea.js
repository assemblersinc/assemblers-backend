
import controller from '../controller/regionarea';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/:regionId')
  .get((...args) => controller.findOne(...args))
  .put((...args) => controller.update(...args))
  .delete((...args) => controller.destroy(...args))


  
export default router;