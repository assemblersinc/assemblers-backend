import controller from '../controller/repair';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/category/')
  .get((...args) => controller.findAllCategories(...args))
  .post((...args) => controller.createCategory(...args));

router.route('/search/:term')
  .get((...args) => controller.search(...args));

// Specs search tearm (sku, upc, name)
router.route('/category/search/:term')
  .get((...args) => controller.searchCategory(...args))

 router.route('/category/:repairCategoryId')
  .put((...args) => controller.updateCategory(...args))
  .get((...args) => controller.findOneCategory(...args))
  .delete((...args) => controller.removeCategory(...args));

router.route('/:repairId')
  .put((...args) => controller.update(...args))
  .get((...args) => controller.findOne(...args))
  .delete((...args) => controller.remove(...args));

  
export default router;