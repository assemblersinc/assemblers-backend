import controller     from '../controller/reporting';
import {Router}       from 'express';

const router          = new Router();

router.route('/')
.get((...args) => controller.dashboard(...args));




export default router;