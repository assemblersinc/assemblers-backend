import controller from '../controller/request';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAllRequest(...args))
  .post((...args) => controller.createStoreRequest(...args));

// Store Request Product endpoints
// Product assembly request and handles Service items as well. 
router.route('/product/')
  .get((...args) => controller.findAllRequestProduct(...args))
  .post((...args) => controller.createRequestProduct(...args));

  /**
   * [deprecated] for App after v1.1 release use 
   * /product/location/:locationId/worklist for App. 
   * Otherwise, use this to get all product/service requested by location. 
   */
router.route('/product/location/:locationId')
  .get((...args) => controller.findAllRequestProductsByLocation(...args))

router.route('/product/location/:locationId/worklist')
  .get((...args) => controller.findAllRequestProductsByLocationWithPictures(...args))


 // updates the product request 
router.route('/product/:requestProductId')
  .get((...args) => controller.findOneRequestProduct(...args))
  .put((...args) => controller.updateRequestProduct(...args));

// updates the store service request 
router.route('/service/:storeServiceId')
  .get((...args) => controller.findOneRequesService(...args))
  .put((...args) => controller.updateRequestService(...args));

router.route('/product/:requestProductId/complete')
  .put((...args) => controller.completeRequestProductForInvoice(...args));

router.route('/product/:productItemId/complete/ondemand')
  .put((...args) => controller.onDemandRequestProductForInvoice(...args));

// Storewalk endpoints
router.route('/storewalk/')
  .get((...args) => controller.findAllStoreWalk(...args))
  .post((...args) => controller.createStoreWalk(...args));
  
router.route('/storewalk/start')
  .post((...args) => controller.startStoreWalk(...args));

router.route('/storewalk/complete')
  .post((...args) => controller.completeStoreWalk(...args));

// Storewalk endpoints
router.route('/storewalk/:storeWalkId')
  .get((...args) => controller.findOneStoreWalk(...args))
  .put((...args) => controller.updateStoreWalk(...args))
  .delete((...args) => controller.removeStoreWalk(...args));

// BOPIS: "Bought Online, Pickup In Store" endpoints
router.route('/bopis/')
  .get((...args) => controller.findAllBopis(...args));

// Adds request made from the store 
// it requires body.request & body.requestedby. 

router.route('store/request')
  .get((...args) => controller.createRequestFromStore(...args)); 

// Store Request endpoints
router.route('/:requestId')
  .get((...args) => controller.findOneRequest(...args))
  .delete((...args) => controller.removeRequest(...args));

router.route('/:requestId/user/:userId')
  .put((...args) => controller.updateRequest(...args));

export default router;