import controller from '../controller/role';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/:roleId')
  .get((...args) => controller.findById(...args))
  .delete((...args) => controller.remove(...args));

router.route('/name/:name')
  .get((...args) => controller.findByName(...args));

export default router;