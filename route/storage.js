import {Router}       from 'express'; 
import controller     from '../controller/storage';
import {repairs, signatures, stamps, storewalks}   from '../util/storage'; 

const router = new Router();

router.route('/signature')
  .post(signatures.single("image"),(...args) => controller.uploadSignatureImage(...args))
  .delete((...args) => controller.deleteSignatureImage(...args));

router.route('/stamp')
  .post(stamps.single("image"),(...args) => controller.uploadStampImage(...args))
  .delete((...args) => controller.deleteStampImage(...args));

router.route('/repair')
  .post(repairs.single("image"),(...args) => controller.uploadRepairImage(...args))
  .delete((...args) => controller.deleteRepairImage(...args));

router.route('/storewalk')
  .post(storewalks.single("image"),(...args) => controller.uploadStorewalkImage(...args))
  .delete((...args) => controller.deleteStorewalkImage(...args));

export default router;