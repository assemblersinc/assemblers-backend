import controller from '../controller/storeAssemblyCount';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

 router.route('/location/:locationId')
  .get((...args) => controller.findByLocation(...args));

router.route('/:storeAssemblyId')
  .get((...args) => controller.findOne(...args))
  .delete((...args) => controller.remove(...args));


export default router;