import controller from '../controller/storeRepair';
import {Router} from 'express'; 
const router = new Router();


router.route('/qrcode/:code')
  .get((...args) => controller.findAllByQRcode(...args));

router.route('/repair/')
  .post((...args) => controller.addRepair(...args));


router.route('/repair/complete')
  .post((...args) => controller.completeRepairAndMoveToInvoice(...args));

router.route('/repair/:storeItemRepairId')
  .get((...args) => controller.findOneRepair(...args))
  .put((...args) => controller.updateRepair(...args))
  .delete((...args) => controller.removeRepair(...args));

router.route('/qrcode/:code')
  .get((...args) => controller.findAllByQRcode(...args));

router.route('/search/:term')
  .get((...args) => controller.search(...args));

router.route('/:locationId/:storeRepairTicketId')
  .put((...args) => controller.updateTicket(...args))
  .get((...args) => controller.findOneTicket(...args))
  .delete((...args) => controller.removeTicket(...args));

router.route('/:locationId')
  .get((...args) => controller.findAllPerLocation(...args))
  //.post((...args) => controller.create(...args));

router.route('/')
  .post((...args) => controller.createTicket(...args));

  
export default router;