import controller from '../controller/storeService';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args))
  .post((...args) => controller.create(...args));

router.route('/:storeServiceId')
  .put((...args) => controller.update(...args))
  .get((...args) => controller.findOne(...args))
  .delete((...args) => controller.remove(...args));

  
export default router;