import controller from '../controller/survey';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
    .get((...args) => controller.findAllSurvey(...args))
    .post((...args) => controller.create(...args));

router.route('/complete')
  .post((...args) => controller.saveCompletedSurveyQuestions(...args));

router.route('/id/:surveyId')
  .get((...args) => controller.findOneSurvey(...args))
  .put((...args) => controller.updateSurvey(...args))
  .delete((...args) => controller.removeSurvey(...args));

router.route('/question/')
  .get((...args) => controller.findAllSurveyQuestion(...args))
  .post((...args) => controller.create(...args));

router.route('/question/:questionId')
    .get((...args) => controller.findOne(...args))
    .put((...args) => controller.update(...args))
    .delete((...args) => controller.destroy(...args));

export default router;