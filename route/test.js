import controller from '../controller/test';
import {Router} from 'express'; 
const router = new Router();

// test send email 
router.route('/sendEmail')
  .post((...args) => controller.sendEmail(...args));

export default router;