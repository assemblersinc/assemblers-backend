import controller from '../controller/timesheet';
import {Router} from 'express';
const router = new Router();


router.route('/user/:userId/')
  .get((...args) => controller.findRecentTimesheetsForUser(...args));

router.route('/:timeId/user/:userId/')
  .get((...args) => controller.findOneTimesheetForUser(...args))
  .put((...args) => controller.updateTravelToAnotherStore(...args))
  .delete((...args) => controller.remove(...args));
  

router.route('/clockout')
  .post((...args) => controller.clockout(...args));

router.route('/clockin')
  .post((...args) => controller.clockin(...args));

router.route('/mealin')
  .post((...args) => controller.mealin(...args));

router.route('/mealout')
  .post((...args) => controller.mealout(...args));

router.route('/fixtime')
  .put((...args) => controller.fixTime(...args))

export default router;