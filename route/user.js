import controller from '../controller/user';
import {Router} from 'express'; 
const router = new Router();

router.route('/')
  .get((...args) => controller.findAll(...args));

router.route('/:userId')
  .get((...args) => controller.findOneById(...args));

router.route('/:userId/location/')
 .get((...args) => controller.findAllUserLocations(...args));

router.route('/area/:areaId')
  .get((...args) => controller.findAllByArea(...args));

router.route('/username/:username')
  .get((...args) => controller.findOneByUsername(...args));

router.route('/terminate')
  .post((...args) => controller.terminateUser(...args));

router.route('/registerpush')
	.post((...args) => controller.registerPush(...args));

export default router;