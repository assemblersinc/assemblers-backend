import {Router}            from 'express';
import authentication      from './route/auth';
import client              from './route/client';
import invoice             from './route/invoice';
import location            from './route/location';
import product             from './route/product';
import request             from './route/request';
import role                from './route/role';
import timesheet           from './route/timesheet';
import user                from './route/user';
import geoarea             from './route/geoarea';
import survey              from './route/survey';
import repair              from './route/repair';
import storeRepair         from './route/storeRepair';
import service             from './route/service';
import storeService        from './route/storeService';
import storage             from './route/storage';
import areamanager         from './route/areamanager';
import azureapi            from './route/azuregraphapi';
import announcement        from './route/announcement';
import regionarea          from './route/regionarea';
import conversation        from './route/conversation';
import storeAssemblyCount  from './route/storeAssemblyCount';
import pricerequest        from './route/pricerequest';
import reporting           from './route/reporting';
import test                from './route/test';

var express = require('express');
const privateRouter = new Router();
const publicRouter = new Router(); 

privateRouter.use('/user', user);
privateRouter.use('/client', client);
privateRouter.use('/invoice', invoice);
privateRouter.use('/location', location);
privateRouter.use('/product', product);
privateRouter.use('/request', request);
privateRouter.use('/role', role);
privateRouter.use('/timesheet', timesheet);
privateRouter.use('/geoarea', geoarea);
privateRouter.use('/survey', survey);
privateRouter.use('/storerepair', storeRepair);
privateRouter.use('/repair', repair);
privateRouter.use('/storeservice', storeService);
privateRouter.use('/service', service);
privateRouter.use('/areamanager', areamanager);
privateRouter.use('/azureapi', azureapi);
privateRouter.use('/announcement',announcement);
privateRouter.use('/region', regionarea); 
privateRouter.use('/storeassemblycount', storeAssemblyCount);
privateRouter.use('/storage', storage);
privateRouter.use('/region', regionarea); 
privateRouter.use('/conversation', conversation);
privateRouter.use('/pricerequest', pricerequest);
privateRouter.use('/test', test);

publicRouter.use('/auth', authentication);
publicRouter.use('/reporting', reporting);

// Endpoint for connection testing
publicRouter.route('/').get((req, res) => { res.send('Welcome to the Assemblers Inc API! ' + process.env.NODE_ENV ) });

let privateRoutes = privateRouter;
let publicRoutes = publicRouter; 

export default {publicRoutes, privateRoutes}

