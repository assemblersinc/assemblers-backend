// =======================================
// Entry point of the node express server. 
// =======================================
import express          from 'express';
import helmet           from 'helmet';
import bodyParser       from 'body-parser';
import {config}         from './config';
import * as middleware  from './middleware';
import {db}             from './models';
import routes           from './routes';
import logger           from './util/logger';

let app  = express();

app.use(helmet());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json()); 

app.use(config.api.path, routes.publicRoutes);
app.use(config.api.path + '/docs', express.static(__dirname + '/docs'));

app.use(middleware.checkErrors);
app.use(config.api.path, middleware.authorizeToken, routes.privateRoutes);

var makeServer = () => {
  app.listen(config.server.port, () => {
    logger.notice(`App (${process.env.NODE_ENV }) listening on port: ${config.server.port}`);
  });
}

if (process.env.NODE_ENV === 'development') {
    process.env.DEBUG='*';
    logger.notice('Development environment selected. Database syncing.');
    // Uncomment the line below if you need to sync the db. WARNING: It will truncate tables!
    //db.sequelize.sync().then(makeServer);
    makeServer(); 
} else {
    logger.notice('Production environment selected. Database sync skipped.');
    makeServer();
}

export default app;
