//Require the dev-dependencies
import user from '../../models/user';
import chai from 'chai';
import chaiHttp from'chai-http';
import server from'../../index';
import {config} from '../../config/config';
import http from 'http';
import assert from 'assert';
import '../lib/index.js';

const should = chai.should();

chai.use(chaiHttp); 

describe('Users', () => {
    /*
    * Test the /GET route
    */
    describe('/GET User', () => {
        it('it should GET all the users', (done) => {
            chai.request(server)
                .get(config.api.path + '/user')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.message.should.be.eql('All Users');
                done();
                });
        });
    });

  /*
  * Test the /POST route
  */
  describe('/POST User', () => {
      it('it should not POST a user without pages field', (done) => {
        let user = {
            firstName: "John",
            lastName: "Smith",
            userName :"jsmith", 
            role : "Tech", 
            techId : "101010",  
        }
        chai.request(server)
            .post(config.api.path + '/user')
            .send(user)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object')
            done();
            });
      });
  });
  
   describe('/UPDATE User', () => {
       
   });


   describe('/DELETE User', () => {
       
   });

});
