import logger from'winston';
import util  from 'util';

var Errors = [
  'UNAUTHORIZED', // Go back to login
  'LOCKED', // Lockout of account
  'INVALID_REQUEST', // Bad parameters, objects they dont own, etc
  'INVALID_TOKEN',
  'EXPIRED_TOKEN',
  'INVALID_AUTH',
  'INVALID_USER',
  'INVALID_QUERY',
  'NOT_UNIQUE',
  'REPEATED',
  'ACCOMPLISHED'
];

function PBSuccess(message) {
  this.success = true;
  if (message) {
    this.message = message;
  }
}

function PBError (code, message){
  if (!code) {
    throw new Error('Invalid PB Error');
  }
  this.code = code;
  if (!message) {message = code;}
  this.message = message;

  if (code === 'UNKNOWN') {
    if (code.stack) {logger.debug(code.stack);}
    logger.debug('Unknown error: ', code, message);
  } else {
    logger.error(code);
    this.PBError = true;
  }
}
util.inherits(PBError, Error);

function getErrorType(error, message)  {
  if (Array.isArray(error) && error.length === 2) {
    error = {
      code: error[0],
      message: error[1]
    };
  }
  if (typeof error === 'object') {
    if (error.code) {
      return new PBError(error.code, error.message);
    }
    if (error.name === 'SequelizeUniqueConstraintError') {
      message = error.errors.map((error) => error.message).join();
      return new PBError('NOT_UNIQUE', message);
    }
    if (error.message && Errors.indexOf(error.message) !== -1) {
      return new PBError(error.message);
    }
    logger.error(message +  ' ' + error); 
    return new PBError('UNKNOWN', 'Unknown error');
  }
  if (error && message) { return new PBError(error, message); }
  if (error && Errors.indexOf(error) !== -1) { return new PBError(error); }
  return new PBError('UNKNOWN', error);
};

export {PBSuccess as Success, PBError as Error, getErrorType}
