import winston      from 'winston';
import dailyRotate  from 'winston-daily-rotate-file';
import fs           from 'fs';
import _            from 'lodash';

const tsFormat = () => (new Date()).toLocaleTimeString();
let logDir = 'log';

winston.emitErrs = true;

// Create log directory if it doesn't exist
if (!fs.existsSync(logDir)) { fs.mkdirSync(logDir) }

// Logging levels and colors assigned to Winston syslog defaults
let levels = _.clone(winston.config.syslog.levels);
let colors = _.clone(winston.config.syslog.colors);
// ... Except for logger.notice()
colors.notice = 'cyan';

const logger = new winston.Logger({
  levels: levels,
  colors: colors,
  handleExceptions: false,
  exceptionHandlers: [
    new winston.transports.File({filename: '${logDir}/exceptions.log'})
  ],
  transports: [

    // For console logging
    new winston.transports.Console({
      // prettyPrint = granular description of line-by-line error stack trace
      prettyPrint: false,
      colorize: true,
      timestamp: true,
      handleExceptions: true,
      // if prettyPrint = false, this will keep stack trace readable
      humanReadableUnhandledException: true
    }),

    // For file logging
    new dailyRotate({
      filename: `${logDir}/-results.log`,
      timestamp: tsFormat,
      datePattern: 'yyyy-MM-dd',
      prepend: true,
      colorize: false,
    }),
  ],

  // if 'true', Winston will exit upon fatal error / uncaughtException
  exitOnError: false

});

export default logger;