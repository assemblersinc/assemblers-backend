'use strict';

import azure from 'azure'; 
import {config} from '../config' ; 

var notificationHubService = azure.createNotificationHubService(config.azurenotification.hubName,
	config.azurenotification.connectionString, true);

const register  = (type, identifier, tag) => {
	// Tag will most likely be the userid
	return new Promise((resolve, reject) => {

		var handler = function (e, r) {
			if (e) {
				return reject(e);
			} else {
				return resolve(r);
			}
		};

		switch (type) {
			case 'ios':
				notificationHubService.apns.createTemplateRegistration(
					identifier,
					tag,
					{
						'aps': {
							'alert': '$(message)',
							'badge': '#(count)',
							'sound': 'default', 
							'payload': '$(payload)'
						},
					},
					handler
				);
				break;

			case 'android':
				notificationHubService.gcm.createTemplateRegistration(
					identifier,
					tag,
					{
						'data': {
							'message': '$(message)', 
							'payload':'$(payload)'
						}
					},
					handler
				);
				break;

			default:
				return reject('no platform type?');
				break;

		}

	});
};


const sendPush = (type, tag, message, payload) => {
	return new Promise((resolve, reject) => {

		var handler = function (e, r) {
			if (e) {
				return reject(e);
			} else {
				return resolve(r);
			}
		};

		notificationHubService.send(tag, {message: message,payload:JSON.stringify(payload)}, handler);
	});

};

export {register, sendPush};