import bCrypt from 'bcrypt-nodejs';

let createPassword = (password) => {
  let credentials = {
    salt: 'test1',
    hash: 'test2'
  };
  credentials.salt = bCrypt.genSaltSync(10);
  credentials.hash = bCrypt.hashSync(password, credentials.salt, null);
  return Promise.resolve(credentials);
};

let alidatePassword = (hash, password)=>{
  var pass;
  try {
    pass = bCrypt.compareSync(password, hash);
  }
  catch(err) {
    return Promise.reject();
  }
  if (pass) {
    return Promise.resolve();
  } else {
    return Promise.reject();
  }
};

export {createPassword, alidatePassword};