import logger from '../logger';
import {config} from '../../config';
import Handlebars from 'handlebars';
import {parseString} from 'xml2js';
import {db} from '../../models';
import rp  from  'request-promise-native';


const kue = require('kue');

var queue = kue.createQueue({
	prefix: 'invoiceExact_' + process.env.NODE_ENV,
	redis: config.azureRedis
});

//protect from crash/reboot
process.once('SIGTERM', function (sig) {
	queue.shutdown(5000, function (err) {
		console.log('Kue shutdown: ', err || '');
		process.exit(0);
	});
});

queue.watchStuckJobs(60000);

// If dashboard desired. call from app entry point.
// module.exports.initKue = (app) => {
// 	var kueUiExpress = require('kue-ui-express');
// 	kueUiExpress(app, '/paykue/', '/qapi');
// 	app.use('/qapi', kue.app);
// };

// Put jobs in queue.
module.exports.addJob = (invoice) => {
	// console.dir(invoice);

	if (!invoice) {
		return Promise.reject('No invoice?');
	}

	var job = queue.create('invoiceExact', {
		invoice: invoice
	}).priority('high').attempts(1).backoff({delay: 8 * 1000, type: 'fixed'}).removeOnComplete(true).save();
	// TODO: Does this create an event listener? Do we need to remove it afterward?
	job.on('complete', function () {
		logger.info("Finished Job.");
	});
	// TODO: Does this create an event listener? Do we need to remove it afterward?
	job.on('failed', function (errorMessage) {
		logger.error("Job Failure Invoice: %s, Error: %s", job.data.invoice.invoiceId, errorMessage);
	});
	// TODO: Does this create an event listener? Do we need to remove it afterward?
	job.on('failed attempt', function (errorMessage, attempt) {
		logger.error("Job Attempt Failure: %s InvoiceId: %s, Error: %s", attempt, job.data.invoice.invoiceId, errorMessage);
	});

	return Promise.resolve();
};



// Process jobs in queue
queue.process('invoiceExact', 2, function (job, done) {
	new Promise((resolve, reject) => { //this is used as an async version of try/catch so we never lose data
		logger.info("Starting JOB: %s", JSON.stringify(job));

		Handlebars.registerHelper('lpad4', function (arg) {
			return arg.padStart(4, "0");
		});

		var template = Handlebars.compile(config.databases.exactdb.xml);

		var xml = template({invoice: job.data.invoice});
		console.log(xml);

		return rp.post({
			uri: config.databases.exactdb.host + config.databases.exactdb.endpoint,
			headers: config.databases.exactdb.headers,
			timeout: 15000,
			body: xml
		})
		.then( (body) => {
			//get orderID from body xml
			console.log(body);

			parseString(body, function (err, resObj) {

				if (resObj.Validations && resObj.Validations.length) {
					return reject('Post to exactDB failed validation');
				}

				if (resObj.importXmlResponse && resObj.importXmlResponse.importXmlResult) {
					parseString(resObj.importXmlResponse.importXmlResult, function (err, orderObj) {
						if (err || !orderObj || !orderObj.Order) {
							console.log(orderObj);
							return reject('Unable to parse response' + err);
						}

						//update order
						var newNumber = orderObj.Order.trim();
						if (newNumber && newNumber!= '0'){
						return db.models.invoice.update({invoiceNumber: newNumber}, {where: {invoiceId: job.data.invoice.invoiceId}})
							.then(() => {
								logger.debug('Invoice %s updated with response %s', job.data.invoice.invoiceId, newNumber);
								resolve();
							});
						}
					});
				}

			});
		})
		.catch(reject);
	})
	.then(() => {
		console.log('DONE');
		done();
	})
	.catch((e) => {
		logger.error('tg' + e);
		return done(e);
	});
});


export default module.exports;


