export const ROLE_TYPE = {
  'TECH' : 4,
  'AREA_MANAGER' : 2,
  'ADMIN' : 1, 
  'EMPLOYEE' : 3, 
  'CONTRACTOR' : 24,
  'OPS_MANAGER' : 5, 
  'CONTRACTOR_TECH': 25, 
  'STORE': 99
}

/* 
  These arrays are used in /middleware/security/accessCheck.js
  to verify the user requesting an HTTP has appropriate 
  user role authorization
*/
export const ADMIN = [
  ROLE_TYPE.ADMIN,
  ROLE_TYPE.AREA_MANAGER,
  ROLE_TYPE.OPS_MANAGER,
]

export const AREA_MANAGER = [
  ROLE_TYPE.AREA_MANAGER,
  ROLE_TYPE.OPS_MANAGER,
]

export const GENERAL = [
  ROLE_TYPE.TECH,
  ROLE_TYPE.AREA_MANAGER,
  ROLE_TYPE.ADMIN,
  ROLE_TYPE.EMPLOYEE,
  ROLE_TYPE.CONTRACTOR,
  ROLE_TYPE.OPS_MANAGER,
  ROLE_TYPE.CONTRACTOR_TECH,
  ROLE_TYPE.STORE
]

export const STORE = [
  ROLE_TYPE.STORE,
  ROLE_TYPE.STORES
]

export const CONTRACTOR = [
  ROLE_TYPE.CONTRACTOR,
  ROLE_TYPE.CONTRACTOR_TECH
]