/*
   Compares the roleId of the incoming HTTP request's user account with the roleIds granted
   access by the /models/ hooks. Certain sequelize methods are restricted to certain roleIds.
   To disable, edit config.checkAuthLevelSwitch
 */
import logger     from '../../util/logger';
import {config}     from '../../config';

export function checkAuthLevel(role, roles, res) {
  if (config.checkAuthLevelSwitch) {
    console.log('checking auth level')
    if (role in roles) {
      return true
    } else {
      return false
    }
  } else {
    return true
  }
}