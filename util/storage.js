import multer               from 'multer'; 
import MulterAzureStorage   from 'multer-azure-storage';
import {config}             from '../config';

const repairs = multer({
  storage: new MulterAzureStorage({
    azureStorageConnectionString: config.azurestorage.azureStorageConnectionString,
    azureStorageAccessKey: config.azurestorage.azureStorageAccessKey,
    azureStorageAccount: config.azurestorage.azureStorageAccount,
    containerName: config.azurestorage.containerNameRepairs,
    containerSecurity: config.azurestorage.containerSecurity
  })
});

const signatures = multer({
  storage: new MulterAzureStorage({
    azureStorageConnectionString: config.azurestorage.azureStorageConnectionString,
    azureStorageAccessKey: config.azurestorage.azureStorageAccessKey,
    azureStorageAccount: config.azurestorage.azureStorageAccount,
    containerName: config.azurestorage.containerNameSignatures,
    containerSecurity: config.azurestorage.containerSecurity
  })
});

const stamps = multer({
  storage: new MulterAzureStorage({
    azureStorageConnectionString: config.azurestorage.azureStorageConnectionString,
    azureStorageAccessKey: config.azurestorage.azureStorageAccessKey,
    azureStorageAccount: config.azurestorage.azureStorageAccount,
    containerName: config.azurestorage.containerNameStamps,
    containerSecurity: config.azurestorage.containerSecurity
  })
});

const storewalks = multer({
  storage: new MulterAzureStorage({
    azureStorageConnectionString: config.azurestorage.azureStorageConnectionString,
    azureStorageAccessKey: config.azurestorage.azureStorageAccessKey,
    azureStorageAccount: config.azurestorage.azureStorageAccount,
    containerName: config.azurestorage.containerNameStorewalks,
    containerSecurity: config.azurestorage.containerSecurity
  })
});

export {repairs, signatures, stamps, storewalks}
