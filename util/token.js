import _              from 'lodash'; 
import aad            from 'azure-ad-jwt';
import adal           from 'adal-node';
import jwt            from 'jsonwebtoken';
import moment         from 'moment';
import logger         from '../util/logger';
import models         from '../models';
import {config}       from '../config';
import authController from '../controller/auth.js';

let AuthenticationContext = adal.AuthenticationContext;

let createToken  = (user, deviceId, transaction)=> {

  var opts = {};
  if (transaction) {
    opts.transaction = transaction;
  }
  const timestamp = moment().utc().format();
  return models.session.upsert({
    userId: user.id,
    deviceId: deviceId,
    timestamp: timestamp
  },
  // Include sequelize transaction ID if there is one
  opts
  )
  .then(() => {
    const payload = {
      pan: user.id,
      noonce: deviceId + timestamp,
      subject: config.jwt.subject,
      issuer: config.jwt.issuer
    };
    return jwt.sign(payload, config.jwt.secret, {
      expiresIn: config.jwt.expiresIn
    });
  });
};  

let validateToken = (jwtToken, refToken) => {
	return new Promise((resolve, reject) => { 
    validateAzureToken(jwtToken)
    .then(usr => { 
      logger.info('Token validated'); 
      return resolve(usr);
    })
    .catch(err => {
      
        // If refresh token is not provided return invalid. 
        if(!refToken){
          return reject({
                code: 'UNAUTHORIZED',
                message: 'Invalid token for user.' 
          });
        }
        // Attempt to get new token
        refreshToken(refToken)
        .then((result)=> {

          /**
           * updates user table 
           * TODO://Find a better way to pass this to the client. 
           */

          if (!result) {
            logger.warning("Token refresh null return"); 
            return reject({
                code: 'UNAUTHORIZED',
                message: 'Invalid refresh token for user.' 
              });
          } else {
            logger.info("Revalidating token..."); 
            // We validate the token again to get the user info. 
            return validateAzureToken(result.accessToken, result)
            .then((user) => {
              // adds new tokens to user 
              user.accessToken = user.accessToken; 
              user.refreshToken = user.refreshToken; 
              user.tokenExpiresOn = user.expiresOn; 

              return resolve(user);
            })
            .catch((err) => {
              logger.warning("validateAzureToken failure:", err); 
              return reject({
                code: 'UNAUTHORIZED',
                message: 'Invalid token for user.' 
              });
            });;
          }
        })
        .catch(err => {
          logger.warning("Refreshing token failure:", err); 
          return reject({
            code: 'UNAUTHORIZED',
            message: 'Invalid refresh token for user.' 
          });
        });
    });
  });
};

let refreshToken = (refreshToken) =>{
     
  var authorityUrl = config.azuread.authorityHostUrl;
  var context = new AuthenticationContext(authorityUrl);
   
  var parameters = {
    clientId : config.azuread.clientId,
    clientSecret: config.azuread.clientSecret, 
  };

  return new Promise((resolve, reject) => {
    context.acquireTokenWithRefreshToken(
      refreshToken, 
      parameters.clientId, 
      parameters.clientSecret,
      null,
      ((err, tokenResponse)=> {
          if (err) {
            logger.warning(err); 
            // console.log('well refreshing didn\'t work: ' + err.stack);
            return reject({
              code: 'UNAUTHORIZED',
              message: 'Invalid refresh token for user.' });
          } else {
            return resolve(tokenResponse); 
          }
        })
      );
  });
}

let validateAzureToken = (token, options) => {
  return new Promise((resolve, reject) => {
		aad.verify(token, { audience: 'spn:' + config.azuread.audience}, function(err, result) {
			if (result) {
        _.assign(result, options);
				return resolve(result);
			} else {
        logger.warning("Invalid token from azure"); 
        return reject({
            code: 'UNAUTHORIZED',
            message: 'Invalid token user.'
        });
			}
		});
	});

}

export {createToken,validateToken}